import React, { useState, useEffect, Fragment } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  ScrollView,
  SafeAreaView,
  FlatList
} from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";
import { useNavigation } from "@react-navigation/native";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { Colors } from "../utils/theme";
import { ModeLoader } from "../utils/theme/mode-loader";
import styling from "../styles";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import { HomePageHeader } from "../components/page-headers";
import NewLocation from "../components/newLocation";

type newLocations = {
  name: { type: String; required: true };
  _id: { type: String; required: true };
  tag: { type: String; required: true };
  coordinates: { type: Array<number>; required: true };
  numReviews: { type: Number };
  avgRating: { type: Number };
  reviews: { type: Array<string> };
  image: { type: String };
  upvotes: { type: Number };
  isVerified: { type: Boolean };
  description: { type: String };
  upvoted: { type: boolean };
};

export default function NewLocationsHome() {
  const theme = ModeLoader();
  const location = locations(theme);
  const styles = styling(theme);
  const [renderUpvote, setRenderUpvote] = useState(true);
  const [unverifiedLocations, setUnverifiedLocations] = useState<
    newLocations[]
  >([]);
  const [imageUploaded, setImageUploaded] = useState(false);



  const lightOrDark = () => {
    return theme == "light";
  };

  async function getLocations() {
    try {
      const response = await fetch(
        "http://132.249.242.205:5050/locations?" +
          new URLSearchParams({
            isVerified: "false",
          }),
        {
          method: "GET",
          headers: {
            "Content-Type": "application/json",
            "x-access-token": await AsyncStorage.getItem("token"),
          },
        }
      );
      const data = await response.json();
      setUnverifiedLocations(data);
      const hasImages = unverifiedLocations.map(item => item.image);
      console.log(hasImages)
    } catch (e) {
      console.error("Error fetching locations: ", e);
    }
  }
  const handleInitialImageState = () => {
    const hasImages = unverifiedLocations.some(item => item.image);
    setImageUploaded(hasImages);
  };
  

  async function pressUpvote(item: newLocations) {
    try {
      const response = await fetch("http://132.249.242.205:5050/vote", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          "x-access-token": await AsyncStorage.getItem("token"),
        },
        body: JSON.stringify({
          locationId: item._id,
        }),
      });
      getLocations();
    } catch (e) {
      console.error("Error" + e);
    }
    console.log(item.name, item._id, item.upvotes);
  }
  useEffect(() => {
    getLocations();
  }, []);

  const navigation = useNavigation();

  //just need to add image field from previous locations
  return (
    <Fragment>
      <SafeAreaView
        style={{
          flex: 0,
          backgroundColor: Colors[theme]?.secondaryBackground,
          height: "3%",
        }}
      />
      <SafeAreaView style={styles.container}>
        <HomePageHeader title="New Locations" theme={theme} />
        <FlatList
          data={unverifiedLocations}
          renderItem={({item}) => {
            return <NewLocation item={item} key={item._id} onUpvote={()=>getLocations()}/>
          }}
        >
        </FlatList>
        <View style={location.addButtonContainer}>
          <TouchableOpacity
            style={location.addButton}
            onPress={() => {
              navigation.navigate("NewLocationsMap");
            }}
          >
            <MaterialCommunityIcons
              name="plus-box"
              size={60}
              color={Colors[theme]?.mainButton}
            />
          </TouchableOpacity>
        </View>
      </SafeAreaView>
    </Fragment>
  );
}

const locations = (theme) =>
  StyleSheet.create({
    item: {
      marginLeft: 20,
      marginRight: 20,
      flexDirection: "column",
      borderRadius: 20,
      marginTop: 24,
      padding: 30,
      backgroundColor: Colors[theme]?.card,
      fontSize: 24,
    },

    row: {
      flexDirection: "row",
      justifyContent: "space-between",
      alignItems: "center",
      paddingBottom: 8,
      top: -10,
    },
    title: {
      paddingRight: 70,
      color: "white",
      fontSize: 16,
      lineHeight: 16,
      letterSpacing: 1,
    },
    tag: {
      color: "white",
      fontSize: 14,
      lineHeight: 14,
      letterSpacing: 1,
    },

    addButton: {
      borderRadius: 20,
    },

    addButtonContainer: {
      position: "absolute",
      marginLeft: 320,
      zIndex: 1,
      bottom: "15%",
    },
  });
