import request from 'supertest';
import jwt from 'jsonwebtoken';
import bcrypt from 'bcrypt';
import app from '../src/index'; 
import User from '../src/models/user.model';
import Location from '../src/models/location.model'; 
import Review from '../src/models/review.model'; 

const createTestUser = async () => {
    const salt = await bcrypt.genSalt(Number(process.env.SALT));
    const hashPassword = await bcrypt.hash('testpassword', salt);
    return User.create({
      username: 'Test User',
      email: `testuser_${Date.now()}@example.com`,
      password: hashPassword,
    });
  };

const generateAuthToken = (user) => {
    return jwt.sign(
        {
          name: user.name,
          email: user.email,
        },
        process.env.JWT_SECRET
      );
};

describe('User Settings API', () => {
    let testUser;
    let authToken;

    beforeAll(async () => {
        testUser = await createTestUser();
        authToken = generateAuthToken(testUser);
    });

    afterAll(async () => {
        await User.deleteOne({ email: testUser.email });
    });

    describe('GET /settings', () => {
        it('should return user details for a valid token', async () => {
          const response = await request(app)
            .get('/settings')
            .set('x-access-token', authToken)
            .expect(200);
    
          expect(response.body.email).toBe(testUser.email);
          expect(response.body.profilePicture).toBe(testUser.profilePicture);
          expect(response.body.username).toBe(testUser.username);
        });
    
        it('should return 401 for an invalid token', async () => {
          await request(app).get('/settings').set('x-access-token', 'invalid-token').expect(401);
        });
    });

    describe('PUT /settings', () => {
        it('should update user details for a valid token', async () => {
          const newName = 'Updated User';
          const newUsername = 'updateduser123';
    
          const response = await request(app)
            .put('/settings')
            .send({
              name: newName,
              username: newUsername,
              oldPassword: 'testpassword',
              password: 'newpassword',
            })
            .set('x-access-token', authToken)
            .expect(200);
    
          expect(response.body.message).toBe('User updated successfully');
    
          const updatedUser = await User.findById(testUser._id);
          expect(updatedUser.username).toBe(newUsername);
    
          
          expect(await bcrypt.compare('newpassword', updatedUser.password)).toBeTruthy();
        });

        it('should return 400 for an invalid email update', async () => {
            try {
                await User.deleteOne({ email: 'existinguser@example.com' });
            } catch (error) {
                console.log(error);
            }
            await User.create({
                username: 'Existing User',
                email: 'existinguser@example.com',
                password: 'existinguserpassword',
            });

            const response = await request(app)
              .put('/settings')
              .send({
                email: "existinguser@example.com", 
              })
              .set('x-access-token', authToken)
              .expect(400);
            
      
            expect(response.body.message).toBe('Email already exists');
          });
    });

    describe('GET /settings/favorites', () => {
        it('should return favorite locations for a valid token', async () => {
          
          const location = await Location.create({
            name: 'Test Location',
            tag: 'test',
            coordinates: [0, 0],
            numReviews: 0,
            avgRating: 0,
            reviews: [],
            image: '',
            upvotes: 0,
            isVerified: false,
          });
    
          testUser.favoriteLocations.push(location._id);
          await testUser.save();
    
          const response = await request(app)
            .get('/settings/favorites')
            .set('x-access-token', authToken)
            .expect(200);
    
          expect(response.body.message).toBe('Favorites retrieved successfully');
          expect(response.body.favorites).toHaveLength(1);
          expect(response.body.favorites[0].name).toBe(location.name);
          expect(response.body.favorites[0].coordinates).toEqual(location.coordinates);
          expect(response.body.favorites[0]._id).toBe(location._id.toString());
        });

        it('should return 401 for an invalid token', async () => {
            await request(app).get('/settings/favorites').set('x-access-token', 'invalid-token').expect(401);
        });
    });
});