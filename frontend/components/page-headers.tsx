import React, { Fragment } from "react";
import { View, Image } from "react-native";
import { Text } from "react-native-paper";
import { Button } from "@rneui/themed";
import { Entypo } from "@expo/vector-icons";
import { useNavigation } from "@react-navigation/native";
import { Colors } from "../utils/theme";
import { ModeLoader } from "../utils/theme/mode-loader";

//Page headers for review & other pages
export const MainPageHeader = ({ title }: { title: string }) => {
  const theme = ModeLoader();

  const navigation = useNavigation();

  return (
    <Fragment>
      <View
        style={{
          width: "100%",
          height: 60,
          justifyContent: "flex-start",
          alignItems: "center",
          borderBottomLeftRadius: 15,
          borderBottomRightRadius: 15,
          backgroundColor: Colors[theme]?.secondaryBackground,
        }}
      >
        <Text
          style={{
            color: Colors[theme]?.secondaryText,
            fontFamily: "Poppins-Regular",
            fontSize: 30,
            fontStyle: "normal",
            fontWeight: "400",
            letterSpacing: 1.2,
          }}
        >
          {title}
        </Text>
      </View>
      <View
        style={{
          position: "relative",
        }}
      >
        <Button
          onPress={() => {
            navigation.goBack();
          }}
          buttonStyle={{ backgroundColor: "transparent", marginLeft: 10 }}
          containerStyle={{
            backgroundColor: "transparent",
            position: "absolute",
            top: -55,
          }}
          size="sm"
          icon={
            <Entypo
              name="chevron-left"
              size={25}
              color={Colors[theme]?.primaryText}
            />
          }
        />
      </View>
    </Fragment>
  );
};

//Page headers for review & other pages
export const AccountPageHeader = ({
  title,
  page,
  theme,
}: {
  title: string;
  page: string;
  theme: string;
}) => {
  const navigation = useNavigation();
  return (
    <Fragment>
      <View
        style={{
          width: "100%",
          height: 60,
          justifyContent: "flex-start",
          alignItems: "center",
          borderBottomLeftRadius: 15,
          borderBottomRightRadius: 15,
          backgroundColor: Colors[theme]?.secondaryBackground,
        }}
      >
        <Text
          style={{
            color: Colors[theme]?.secondaryText,
            fontFamily: "Poppins-Regular",
            fontSize: 30,
            fontStyle: "normal",
            fontWeight: "400",
            letterSpacing: 1.2,
          }}
        >
          {title}
        </Text>
      </View>
      <View
        style={{
          position: "relative",
        }}
      >
        <Button
          onPress={() => {
            navigation.navigate(page);
          }}
          buttonStyle={{ backgroundColor: "transparent", marginLeft: 27 }}
          containerStyle={{
            backgroundColor: "transparent",
            position: "absolute",
            top: -54,
          }}
          size="sm"
          icon={
            <Entypo
              name="chevron-left"
              size={25}
              color={Colors[theme]?.primaryText}
            />
          }
        />
      </View>
    </Fragment>
  );
};

//Page header without back button
export const HomePageHeader = ({
  title,
  theme,
}: {
  title: string;
  theme: string;
}) => {
  return (
    <Fragment>
      <View
        style={{
          width: "100%",
          height: 60,
          justifyContent: "flex-start",
          alignItems: "center",
          borderBottomLeftRadius: 15,
          borderBottomRightRadius: 15,
          backgroundColor: Colors[theme]?.secondaryBackground,
        }}
      >
        <Text
          style={{
            color: Colors[theme]?.secondaryText,
            fontFamily: "Poppins-Regular",
            fontSize: 30,
            fontStyle: "normal",
            fontWeight: "400",
            letterSpacing: 1.2,
          }}
        >
          {title}
        </Text>
      </View>
      <View
        style={{
          position: "relative",
        }}
      ></View>
    </Fragment>
  );
};

//Page header with back button and logo
export const logoPageHeader = (backPage: string) => {
  const theme = ModeLoader();

  const navigation = useNavigation();
  const page = () => navigation.navigate(backPage);

  return (
    <Fragment>
      <View
        style={{
          width: "100%",
          height: 40,
          justifyContent: "flex-start",
          alignItems: "center",
          backgroundColor: "transparent",
          position: "relative",
        }}
      >
        <Image
          style={{ width: 60, height: 60, position: "absolute", top: -10 }}
          source={require("../assets/AppLogo.png")}
        />
      </View>
      <View
        style={{
          position: "relative",
        }}
      >
        <Button
          onPress={page}
          buttonStyle={{ backgroundColor: "transparent", marginLeft: 10 }}
          containerStyle={{
            backgroundColor: "transparent",
            position: "absolute",
            top: -36,
          }}
          size="sm"
          icon={
            <Entypo
              name="chevron-left"
              size={27}
              color={Colors[theme]?.primaryText}
            />
          }
        />
      </View>
    </Fragment>
  );
};

//Page header with back button w/o logo
export const blankPageHeader = (backPage: string) => {
  const theme = ModeLoader();

  const navigation = useNavigation();
  const page = () => navigation.navigate(backPage);

  return (
    <Fragment>
      <View
        style={{
          width: "100%",
          height: 40,
          justifyContent: "flex-start",
          alignItems: "center",
          backgroundColor: "transparent",
          position: "relative",
        }}
      ></View>
      <View
        style={{
          position: "relative",
        }}
      >
        <Button
          onPress={page}
          buttonStyle={{ backgroundColor: "transparent", marginLeft: 10 }}
          containerStyle={{
            backgroundColor: "transparent",
            position: "absolute",
            top: -36,
          }}
          size="sm"
          icon={
            <Entypo
              name="chevron-left"
              size={27}
              color={Colors[theme]?.primaryText}
            />
          }
        />
      </View>
    </Fragment>
  );
};
