import {
  StyleSheet,
  Text,
  View,
  Image,
  ScrollView,
  SafeAreaView,
} from "react-native";
import styling from "../styles";
import { FlatList, TouchableOpacity } from "react-native-gesture-handler";
import React, { useEffect, useState, Fragment } from "react";
import { Rating } from "react-native-stock-star-rating"; //$ npm install react-native-stock-star-rating
import AsyncStorage from "@react-native-async-storage/async-storage";
import PastReview from "../components/PastReview";
import { ModeLoader } from "../utils/theme/mode-loader";
import { AccountPageHeader } from "../components/page-headers";
import { Colors } from "../utils/theme";

type Review = {
  _id: string;
  locationId: string;
  userId: string;
  stars: number;
  comment: string;
  date: Date;
  heading: string;
  photos: Array<string>;
  upvotes: number;
  username: string;
};

export default function ReviewPage() {
  const theme = ModeLoader();
  const styles = styling(theme);

  const [reviews, setReviews] = useState<Review[]>([]);

  async function getReviews() {
    try {
      const response = await fetch(
        "http://132.249.242.205:5050/settings/reviews",
        {
          method: "GET",
          headers: {
            "Content-Type": "application/json",
            "x-access-token": await AsyncStorage.getItem("token"),
          },
        }
      );
      const data = await response.json();
      setReviews(data.reviews);
    } catch (error) {
      console.error("Error fetching reviews:", error);
    }
  }

  useEffect(() => {
    getReviews();
  }, []);

  return (
    <Fragment>
      <SafeAreaView
        style={{
          flex: 0,
          backgroundColor: Colors[theme]?.secondaryBackground,
          height: "3%",
        }}
      />
      <SafeAreaView style={styles.container}>
        <AccountPageHeader title="Reviews" page="ProfilePage" theme={theme} />
        {/* <ScrollView>
          {reviews.reverse().map((item) => {
            return <PastReview item={item} key={item._id} />;
          })}
        </ScrollView> */}
        <FlatList 
          data={reviews.reverse()} 
          renderItem={({item})=> {
            return <PastReview item={item} key={item._id} />;
          }}>

        </FlatList>
      </SafeAreaView>
    </Fragment>
    // <View style={styles.container}>
    //   <View style={styles.header}>
    //     <SafeAreaView style={styles.headerContents}>
    //       <TouchableOpacity
    //         onPress={goToProfilePage}
    //         style={styles.headerBackButton}
    //       >
    //         <Image source={require("../assets/arrowLeft.png")} />
    //       </TouchableOpacity>
    //       <Text style={styles.pageTitle}>Reviews</Text>
    //     </SafeAreaView>
    //   </View>
    //   <ScrollView>
    //     {reviews.map((item) => {
    //       return <PastReview item={item} />;
    //     })}
    //   </ScrollView>
    // </View>
  );
}
