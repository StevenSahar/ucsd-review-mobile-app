import mongoose from "mongoose";

const reviewSchema = new mongoose.Schema(
  {
    locationId: { type: String, required: true },
    userId: { type: String, required: true },
    stars: { type: Number, required: true },
    comment: { type: String, required: true },
    date: { type: Date, required: true },
    heading: { type: String, required: true },
    photos: { type: Array, required: false },
    upvotes: { type: Number, required: true },
    username: { type: String, required: true },
  },
  {
    collection: "reviews",
  }
);

const Review = mongoose.model("Review", reviewSchema);
export default Review;
