import React from "react";
import { StyleSheet, TouchableOpacity, Text } from "react-native";
import { useNavigation } from "@react-navigation/core";
import { ModeLoader } from "../utils/theme/mode-loader";
import { Colors } from "../utils/theme";

function AINavigationButton() {
  const theme = ModeLoader();
  const styles = styling(theme);

  const navigation = useNavigation();

  return (
    <TouchableOpacity
      style={styles.smallButton}
      onPress={() => navigation.navigate("AIPlugin")}
    >
      <Text style={styles.buttonText}>Ask AI</Text>
    </TouchableOpacity>
  );
}

const styling = (theme: string) =>
  StyleSheet.create({
    smallButton: {
      zIndex: 1000,
      width: 82,
      height: 46,
      padding: 10,
      borderRadius: 20,
      alignItems: "center",
      justifyContent: "center",
      position: "absolute",
      right: 19,
      top: 54.5,
      backgroundColor: Colors[theme]?.button,
      paddingHorizontal: 12, // Adjust for the size you want
      elevation: 1, // Add shadow on Android
      // shadowColor: "black",
      // shadowOffset: { width: 0, height: 2 }, // Shadow position
      // shadowOpacity: 0.25,
      // shadowRadius: 3.84, // Shadow blur
    },
    buttonText: {
      // color: Colors[theme]?.buttonText,
      color: theme === "light" ? "white" : "black",
      fontSize: 17,
    },
  });

export default AINavigationButton;
