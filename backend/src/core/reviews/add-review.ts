import express from "express";
import Location from "../../models/location.model";
import User from "../../models/user.model";
import Review from "../../models/review.model";
import jwt from "jsonwebtoken";

const router = express.Router();

router.post("/", async (req, res) => {
    try {
        const token = req.headers["x-access-token"];
        const decoded = jwt.verify(token, process.env.JWT_SECRET);
        const user = await User.findById(decoded.id);
        const location = await Location.findById(req.body.locationId);

        const review = await Review.create({
            locationId: req.body.locationId,
            userId: decoded.id,
            stars: req.body.stars,
            comment: req.body.comment,
            date: req.body.date,
            heading: req.body.heading,
            photos: req.body.photos ? req.body.photos : [],
            upvotes: 0,
            username: user.username,
        });

        user.ownReviews.push(review._id);
        await user.save();
        let stars : any = Number(review.stars);

        location.reviews[stars - 1]++;
        location.avgRating = (location.avgRating * location.numReviews + stars) / (location.numReviews + 1);
        location.numReviews++;
        await location.save(); 

        res.status(200).send({ message: "Review added successfully"});
        return;
    } catch (error) {
        res.status(400).send({ message: error.message });
    }

});

export default router;