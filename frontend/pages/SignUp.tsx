import React, { useState, Fragment } from "react";
import {
  SafeAreaView,
  View,
  StyleSheet,
  TouchableOpacity,
  Image,
} from "react-native";
import { Text, TextInput } from "react-native-paper";
import { Button } from "@rneui/themed";
import { Formik } from "formik";
import { Entypo } from "@expo/vector-icons";
import { toggleButton } from "../components/toggle-password";
import { orSeparator } from "../components/or-separator";
import {
  validUsername,
  validEmailAddress,
  validPassword,
  confirmSamePassword,
} from "../utils/form-validator";
import { Colors } from "../utils/theme";
import { ModeLoader } from "../utils/theme/mode-loader";
import { logoPageHeader } from "../components/page-headers";
import GoogleAccount from "../components/google-account";

function Register() {
  const theme = ModeLoader();
  const styles = styling(theme);

  const [username, setUserName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");

  const [showPassword, setShowPassword] = useState(true);
  const [showConfirmPassword, setShowConfirmPassword] = useState(true);

  const [usernameError, setUsernameError] = useState(false);
  const [emailError, setEmailError] = useState(false);
  const [passwordError, setPasswordError] = useState(false);
  const [confirmPasswordError, setConfirmPasswordError] = useState(false);
  const [duplicateEmailError, setDuplicateEmailError] = useState(false);
  const [success, setSuccess] = useState(false);
  const [message, setMessage] = useState("");

  async function register() {
    const server = "132.249.242.205";
    const port = "5050";
    const route = `http://${server}:${port}/signup`;
    const response = await fetch(route, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        username: username,
        email: email,
        password: password,
      }),
    });

    const data = await response.json();
    console.log(data);

    if (data.message) {
      setSuccess(true);
      setMessage(
        "Your account has been successfully created. Please check your inbox and verify your account through the link sent."
      );

      //Get rid of the data once successfully registered
      setUserName("");
      setEmail("");
      setPassword("");
      setConfirmPassword("");
    } else {
      setDuplicateEmailError(true);
      setMessage(
        "The email provided is already associated with an existing account. Please use a different email address."
      );
    }
  }

  const formValidator = () => {
    //Reset all errors
    setUsernameError(false);
    setEmailError(false);
    setPasswordError(false);
    setConfirmPasswordError(false);
    setDuplicateEmailError(false);
    setSuccess(false);
    setMessage("");

    //Check whether valid username
    if (validUsername(username)) {
      setMessage(validUsername(username));
      setUsernameError(true);
      return false;
    }

    //Check whether valid email
    if (validEmailAddress(email)) {
      setMessage(validEmailAddress(email));
      setEmailError(true);
      return false;
    }

    //Check whether valid password
    if (validPassword(password)) {
      setMessage(validPassword(password));
      setPasswordError(true);
      return false;
    }

    //Check whether both password are the same
    if (confirmSamePassword(password, confirmPassword)) {
      setMessage(confirmSamePassword(password, confirmPassword));
      setConfirmPasswordError(true);
      return false;
    }

    return true;
  };

  const togglePassword = () => {
    setShowPassword(!showPassword);
  };

  const toggleConfirmPassword = () => {
    setShowConfirmPassword(!showConfirmPassword);
  };

  return (
    <Fragment>
      <SafeAreaView style={styles.topSafeAreaView} />
      <SafeAreaView style={styles.safeContainer}>
        {logoPageHeader("LandingPage")}
        <View style={styles.outerContainer}>
          <Formik
            initialValues={{}}
            onSubmit={() => {
              if (formValidator()) {
                register();
              }
            }}
          >
            {({ handleSubmit }) => (
              <View style={styles.container}>
                <Text style={styles.title}>Create Your Account</Text>
                <View
                  style={[
                    styles.inputContainer,
                    usernameError && styles.inputContainerError,
                  ]}
                >
                  <TextInput
                    style={styles.input}
                    value={username}
                    onChangeText={setUserName}
                    placeholder="username"
                    placeholderTextColor={Colors[theme]?.placeholderText}
                    textColor={Colors[theme]?.primaryText}
                    contentStyle={{ fontFamily: "Poppins-Regular" }}
                    selectionColor={Colors[theme]?.placeholderText}
                    underlineColor="transparent"
                    activeUnderlineColor="transparent"
                    textContentType="username"
                    keyboardType="default"
                  />
                </View>
                <View
                  style={[
                    styles.inputContainer,
                    emailError && styles.inputContainerError,
                  ]}
                >
                  <TextInput
                    style={styles.input}
                    value={email}
                    onChangeText={setEmail}
                    placeholder="email"
                    placeholderTextColor={Colors[theme]?.placeholderText}
                    textColor={Colors[theme]?.primaryText}
                    contentStyle={{ fontFamily: "Poppins-Regular" }}
                    selectionColor={Colors[theme]?.placeholderText}
                    underlineColor="transparent"
                    activeUnderlineColor="transparent"
                    textContentType="emailAddress"
                    keyboardType="email-address"
                    autoCapitalize="none"
                  />
                </View>
                <View
                  style={[
                    styles.inputContainer,
                    passwordError && styles.inputContainerError,
                  ]}
                >
                  <TextInput
                    style={styles.input}
                    value={password}
                    onChangeText={setPassword}
                    placeholder="password"
                    placeholderTextColor={Colors[theme]?.placeholderText}
                    textColor={Colors[theme]?.primaryText}
                    contentStyle={{ fontFamily: "Poppins-Regular" }}
                    selectionColor={Colors[theme]?.placeholderText}
                    underlineColor="transparent"
                    activeUnderlineColor="transparent"
                    textContentType="password"
                    passwordRules="minlength: 20; required: lower; required: upper; required: digit; required: [-().&@?'#$!%*,/&quot;+];"
                    keyboardType="default"
                    secureTextEntry={showPassword}
                  />
                  {toggleButton(showPassword, togglePassword)}
                </View>
                <View
                  style={[
                    styles.inputContainer,
                    confirmPasswordError && styles.inputContainerError,
                    { marginBottom: 5 },
                  ]}
                >
                  <TextInput
                    style={styles.input}
                    value={confirmPassword}
                    onChangeText={setConfirmPassword}
                    placeholder="confirm password"
                    placeholderTextColor={Colors[theme]?.placeholderText}
                    textColor={Colors[theme]?.primaryText}
                    contentStyle={{ fontFamily: "Poppins-Regular" }}
                    selectionColor={Colors[theme]?.placeholderText}
                    underlineColor="transparent"
                    activeUnderlineColor="transparent"
                    textContentType="password"
                    passwordRules="minlength: 20; required: lower; required: upper; required: digit; required: [-().&@?'#$!%*,/&quot;+];"
                    keyboardType="default"
                    secureTextEntry={showConfirmPassword}
                  />
                  {toggleButton(showConfirmPassword, toggleConfirmPassword)}
                </View>
                <View style={styles.messageContainer}>
                  <Text
                    style={[
                      styles.message,
                      (usernameError ||
                        emailError ||
                        passwordError ||
                        confirmPasswordError ||
                        duplicateEmailError) &&
                        styles.messageError,
                      success && styles.messageSuccess,
                    ]}
                  >
                    {message}
                  </Text>
                </View>
                <Button
                  onPress={() => handleSubmit()}
                  title="Continue"
                  icon={
                    <Entypo
                      name="chevron-right"
                      size={25}
                      color={Colors[theme]?.buttonText}
                    />
                  }
                  iconRight
                  buttonStyle={styles.continueButton}
                  titleStyle={styles.continueButtonText}
                />
              </View>
            )}
          </Formik>
          {orSeparator()}
          {/* {GoogleAccount(false)} */}
        </View>
      </SafeAreaView>
    </Fragment>
  );
}

const styling = (theme) =>
  StyleSheet.create({
    topSafeAreaView: {
      flex: 0,
      backgroundColor: Colors[theme]?.primaryBackground,
    },
    safeContainer: {
      flex: 1,
      backgroundColor: Colors[theme]?.primaryBackground,
    },
    outerContainer: {
      flex: 1,
      alignItems: "center",
    },
    container: {
      alignItems: "center",
      justifyContent: "flex-start",
    },
    title: {
      color: Colors[theme]?.primaryText,
      fontFamily: "Poppins-Regular",
      fontSize: 20,
      fontStyle: "normal",
      fontWeight: "400",
      letterSpacing: 1,
      marginTop: 15,
      marginBottom: 47,
    },
    inputContainer: {
      width: 290,
      height: 50,
      flexDirection: "row",
      alignItems: "center",
      borderTopLeftRadius: 23,
      borderBottomLeftRadius: 23,
      borderBottomRightRadius: 23,
      borderTopRightRadius: 23,
      borderWidth: 0.5,
      borderColor: Colors[theme]?.border,
      backgroundColor: Colors[theme]?.inputContainer,
      opacity: 1,
      margin: 10,
      padding: 0,
    },
    input: {
      width: "87%",
      height: 50,
      borderTopLeftRadius: 23,
      borderBottomLeftRadius: 23,
      borderBottomRightRadius: 0,
      borderTopRightRadius: 0,
      backgroundColor: "transparent",
      color: Colors[theme]?.placeholderText,
      fontFamily: "Poppins-Regular",
      fontSize: 12,
      fontStyle: "normal",
      fontWeight: "400",
      letterSpacing: 0.6,
      paddingLeft: 5,
    },
    continueButton: {
      width: 290,
      height: 50,
      alignItems: "center",
      justifyContent: "flex-end",
      borderTopLeftRadius: 23,
      borderBottomLeftRadius: 23,
      borderBottomRightRadius: 23,
      borderTopRightRadius: 23,
      backgroundColor: Colors[theme]?.button,
      marginTop: 5,
    },
    continueButtonText: {
      color: Colors[theme]?.buttonText,
      fontFamily: "Poppins-Regular",
      fontSize: 12,
      fontStyle: "normal",
      fontWeight: "400",
      letterSpacing: 0.6,
      textAlign: "left",
      paddingLeft: 14,
      marginRight: 167,
    },
    inputContainerError: {
      backgroundColor: "rgba(231, 70, 70, 0.24)",
      borderWidth: 1,
      borderColor: "#E74646",
    },
    messageContainer: {
      width: 270,
      height: 57,
      justifyContent: "center",
    },
    message: {
      width: "100%",
      color: Colors[theme]?.primaryText,
      fontFamily: "Poppins-Regular",
      fontSize: 11,
      fontStyle: "normal",
      fontWeight: "400",
      letterSpacing: 0.6,
      textAlign: "left",
      padding: 5,
    },
    messageError: {
      color: "#E74646",
    },
    messageSuccess: {
      color: "#198754",
    },
  });

export default Register;
