import mongoose from "mongoose";

const imageSchema = new mongoose.Schema(
  {
    imageData: Buffer,
    contentType: String,
  },
  {
    collection: "Images",
  }
);

const Images = mongoose.model("Image", imageSchema);

export default Images;