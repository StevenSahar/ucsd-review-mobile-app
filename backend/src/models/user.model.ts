import mongoose from "mongoose";

const UserSchema = new mongoose.Schema(
  {
    email: { type: String, required: true, unique: true },
    password: { type: String, required: true },
    username: { type: String, required: true },
    favoriteLocations: { type: Array, required: false },
    ownReviews: { type: Array, required: false },
    likedReviews: { type: Array, required: false },
    upvotedLocations: { type: Array, required: false },
    profilePicture: { type: String, required: false },
    verified: { type: Boolean, default: false },
  },
  {
    collection: "users",
  }
);

export default mongoose.model("User", UserSchema);
