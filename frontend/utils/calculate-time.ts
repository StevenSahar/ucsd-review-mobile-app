//Calculate the time different between current date with posted date

export const calculateTime = (postedDate: Date) => {
  const HOUR = 60;
  const DAY = HOUR * 24;
  const WEEK = DAY * 7;
  const MONTH = WEEK * 4.34524;
  const YEAR = MONTH * 12;

  const currentDate = new Date().getTime();
  const postDate = postedDate.getTime();

  const timeDifference = Math.round((currentDate - postDate) / (1000 * 60)); //In Minutes

  if (timeDifference < HOUR) {
    const time = Math.round(timeDifference);
    return `${time} minute${time <= 0 ? "" : "s"} ago`;
  } else if (timeDifference < DAY) {
    const time = Math.round(timeDifference / HOUR);
    return `${time} hour${time <= 0 ? "" : "s"} ago`;
  } else if (timeDifference < WEEK) {
    const time = Math.round(timeDifference / DAY);
    return `${time} day${time <= 0 ? "" : "s"} ago`;
  } else if (timeDifference < MONTH) {
    const time = Math.round(timeDifference / WEEK);
    return `${time} week${time <= 0 ? "" : "s"} ago`;
  } else if (timeDifference < YEAR) {
    const time = Math.round(timeDifference / MONTH);
    return `${time} month${time <= 0 ? "" : "s"} ago`;
  } else {
    const time = Math.round(timeDifference / YEAR);
    return `${time} year${time <= 0 ? "" : "s"} ago`;
  }
};
