## Get All Locations
Return an array with all locations in the database.

- **URL**: `/locations`
- **Method**: `GET`
- **Request Body**: None
- **Request Headers**: `x-access-token` (string, required): The authentication token of the user.
### Responses
- Success Response:
    - Status Code: `200`
    - Response Body: 
        ```json
            [
                {
                    "_id": "64becc3743626f3c61839fcd",
                    "name": "Canyon Vista",
                    "tag": "Dining Hall",
                    "coordinates": [
                    32.883956337387865,
                    -117.23326874292854
                    ]
                },
                {
                    "_id": "64becc9943626f3c61839fcf",
                    "name": "Foodworx",
                    "tag": "Dining Hall",
                    "coordinates": [
                    32.87878784655529,
                    -117.23039113454314
                    ]
                }
            ]
        ```
- Error Response:
    - Status Code: `500`
    - Response Body:
        ```json
            {
                "message": "<error message>"
            }
        ```

## Leave a Review
Adds a review to the specified location. Updates the average review score of the location. 

- **URL**: `/add-review`
- **Method**: `POST`
- **Request Body**:
    - `locationId` (string, required): The id of the location to add the review to.
    - `comment` (string, required): The review text to add to the location.
    - `stars` (number, required): The number of stars the user left for the location.
    - `date` ([Date](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date), required): The date the review was added.
    - `heading` (string, required): The heading of the review.
    - `photos` (array, optional): An array of base64 encoded photos to add to the review.
- **Request Headers**: `x-access-token` (string, required): The authentication token of the user.

### Responses
- Success Response:
    - Status Code: `200`
    - Response Body: `{ "message": "Review Added Successfully" }`
- Error Response:
    - Status Code: `400`
    - Response Body: `{ "message": <error message> }`

## Get Location Reviews
Gets all reviews for the specified location.

- **URL**: `/location-reviews`
- **Method**: `GET`
- **Request Query**:
    - `locationId` (string, required): The id of the location to get reviews for.
    - `sortOption` (string, optional): The sort option to use when getting reviews. Can be `newest`, `mostUpvoted`, `highest`, or `lowest`. Defaults to `newest`.
- **Request Headers**: `x-access-token` (string, required): The authentication token of the user.

### Responses
- Success Response:
    - Status Code: `200`
    - Response Body: 
        ```json
            [
                {
                    "locationId": "64becc3743626f3c61839fcd",
                    "userId": "64becc3743626f3c61839fcd",
                    "stars": 5,
                    "comment": "This place is great!",
                    "date": "2023-11-11T00:00:00.000Z",
                    "heading": "Great Place!",
                    "photos": [],
                    "upvotes": 0,
                    "username": "johndoe"
                }
            ]
        ```
- Error Response:
    - Status Code: `400`
    - Response Body: `{ "error": <error message> }`
                    

## Vote on a location
Votes on a location: handles both upvotes and downvotes depending on the user's previous vote.

- **URL**: `/vote`
- **Method**: `POST`
- **Request Body**:
    - `locationId` (string, required): The id of the location to vote on.
- **Request Headers**: `x-access-token` (string, required): The authentication token of the user.

### Responses
- Success Response:
    - Status Code: `200`
    - Response Body: `{ "message": "Vote recorded successfully" }`
- Error Response:
    - Status Code: `400`
    - Response Body: `{ "message": Invalid token }`

## Add a location
Adds a location to the database.

- **URL**: `/add-location`
- **Method**: `POST`
- **Request Body**:
    - `name` (string, required): The name of the location.
    - `tag` (string, required): The tag of the location.
    - `coordinates` (array, required): The coordinates of the location.
    - `image` (string, optional): The base64 encoded image of the location.
    - `description` (string, required): The description of the location.
- **Request Headers**: `x-access-token` (string, required): The authentication token of the user.

### Responses
- Success Response:
    - Status Code: `200`
    - Response Body: `{ "message": "Location added successfully" }`
- Error Response:
    - Status Code: `400`
    - Response Body: `{ "message": <error message> }`