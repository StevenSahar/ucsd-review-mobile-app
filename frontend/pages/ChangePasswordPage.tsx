import React, { useState, Fragment } from "react";
import {
  StyleSheet,
  Text,
  View,
  SafeAreaView,
  Image,
  TextInput,
} from "react-native";
import { Button } from "@rneui/themed";
import { Formik } from "formik";
import globalStyles from "../styles";
import { Entypo } from "@expo/vector-icons";
import { TouchableOpacity } from "react-native-gesture-handler";
import { useNavigation } from "@react-navigation/core";
import { ModeLoader } from "../utils/theme/mode-loader";
import { Colors } from "../utils/theme";
import { toggleButton } from "../components/toggle-password";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { validPassword, confirmSamePassword } from "../utils/form-validator";
import { AccountPageHeader } from "../components/page-headers";

const ChangePasswordPage = () => {
  const theme = ModeLoader();
  const styles = styling(theme);
  const global = globalStyles(theme);

  const navigation = useNavigation();
  const goToSettingsPage = () => {
    navigation.navigate("SettingsPage");
  };

  const [email, setEmail] = useState("");
  const [name, setName] = useState("");

  const getUserInfo = async () => {
    const server = "132.249.242.205";
    const port = "5050";
    const route = `http://${server}:${port}/settings/`;
    const response = await fetch(route, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        "x-access-token": await AsyncStorage.getItem("token"),
      },
    });

    const data = await response.json();

    if (response?.ok) {
      setEmail(data.email);
      setName(data.name);
    }
  };

  const [oldPassword, setOldPassword] = useState("");
  const [newPassword, setNewPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");

  const [showOldPassword, setShowOldPassword] = useState(true);
  const [showNewPassword, setShowNewPassword] = useState(true);
  const [showConfirmPassword, setShowConfirmPassword] = useState(true);

  const [passwordError, setPasswordError] = useState(false);
  const [newPasswordError, setNewPasswordError] = useState(false);
  const [confirmError, setConfirmError] = useState(false);
  const [success, setSuccess] = useState(false);
  const [message, setMessage] = useState("");

  const toggleShowOldPassword = () => {
    setShowOldPassword(!showOldPassword);
  };
  const toggleShowNewPassword = () => {
    setShowNewPassword(!showNewPassword);
  };
  const toggleShowConfirmPassword = () => {
    setShowConfirmPassword(!showConfirmPassword);
  };

  async function changePassword() {
    const server = "132.249.242.205";
    const port = "5050";
    const route = `http://${server}:${port}/settings`;
    const response = await fetch(route, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        "x-access-token": await AsyncStorage.getItem("token"),
      },
      body: JSON.stringify({
        name: name,
        email: email,
        oldPassword: oldPassword,
        password: newPassword,
      }),
    });

    if (response?.ok) {
      setSuccess(true);
      setMessage("successfully changed password");

      setOldPassword("");
      setNewPassword("");
      setConfirmPassword("");
    } else {
      setPasswordError(true);
      setMessage("incorrect Password");
    }
  }

  const formValidator = () => {
    setPasswordError(false);
    setNewPasswordError(false);
    setConfirmError(false);
    setSuccess(false);
    setMessage("");

    if (validPassword(newPassword)) {
      setMessage(validPassword(newPassword));
      setNewPasswordError(true);
      return false;
    }
    if (confirmSamePassword(newPassword, confirmPassword)) {
      setMessage(confirmSamePassword(newPassword, confirmPassword));
      setConfirmError(true);
      return false;
    }

    return true;
  };

  return (
    <Fragment>
      <SafeAreaView
        style={{
          flex: 0,
          backgroundColor: Colors[theme]?.secondaryBackground,
          height: "3%",
        }}
      />
      <SafeAreaView style={styles.container}>
        <AccountPageHeader
          title="New Password"
          page="SettingsPage"
          theme={theme}
        />
        <View style={styles.outerContainer}>
          <Formik
            initialValues={{}}
            onSubmit={() => {
              if (formValidator()) {
                getUserInfo();
                changePassword();
              }
            }}
          >
            {({ handleSubmit }) => (
              <View style={styles.container}>
                <View
                  style={[
                    styles.inputContainer,
                    passwordError && styles.inputContainerError,
                  ]}
                >
                  <TextInput
                    style={styles.input}
                    value={oldPassword}
                    onChangeText={setOldPassword}
                    placeholder="old password"
                    placeholderTextColor={Colors[theme]?.placeholderText}
                    selectionColor={Colors[theme]?.placeholderText}
                    autoCapitalize="none"
                    autoComplete="off"
                    textContentType="password"
                    keyboardType="default"
                    secureTextEntry={showOldPassword}
                  />
                  {toggleButton(showOldPassword, toggleShowOldPassword)}
                </View>
                <View
                  style={[
                    styles.inputContainer,
                    newPasswordError && styles.inputContainerError,
                  ]}
                >
                  <TextInput
                    style={styles.input}
                    value={newPassword}
                    onChangeText={setNewPassword}
                    placeholder="new password"
                    placeholderTextColor={Colors[theme]?.placeholderText}
                    selectionColor={Colors[theme]?.placeholderText}
                    autoCapitalize="none"
                    autoComplete="off"
                    textContentType="password"
                    passwordRules="minlength: 20; required: lower; required: upper; required: digit; required: [-().&@?'#$!%*,/&quot;+];"
                    keyboardType="default"
                    secureTextEntry={showNewPassword}
                  />
                  {toggleButton(showNewPassword, toggleShowNewPassword)}
                </View>
                <View
                  style={[
                    styles.inputContainer,
                    confirmError && styles.inputContainerError,
                  ]}
                >
                  <TextInput
                    style={styles.input}
                    value={confirmPassword}
                    onChangeText={setConfirmPassword}
                    placeholder="confirm password"
                    placeholderTextColor={Colors[theme]?.placeholderText}
                    selectionColor={Colors[theme]?.placeholderText}
                    autoCapitalize="none"
                    autoComplete="off"
                    textContentType="password"
                    passwordRules="minlength: 20; required: lower; required: upper; required: digit; required: [-().&@?'#$!%*,/&quot;+];"
                    keyboardType="default"
                    secureTextEntry={showConfirmPassword}
                  />
                  {toggleButton(showConfirmPassword, toggleShowConfirmPassword)}
                </View>
                <View style={styles.messageContainer}>
                  <Text
                    style={[
                      styles.message,
                      (passwordError || newPasswordError || confirmError) &&
                        styles.messageError,
                      success && styles.messageSuccess,
                    ]}
                  >
                    {message}
                  </Text>
                </View>
                <Button
                  onPress={() => {
                    handleSubmit();
                    console.log("submitted");
                  }}
                  title="Submit"
                  icon={
                    <Entypo
                      name="chevron-right"
                      size={25}
                      color={theme === "light" ? "#000000" : "#181927"}
                    />
                  }
                  iconRight
                  buttonStyle={styles.submit}
                  titleStyle={styles.buttonText}
                />
              </View>
            )}
          </Formik>
        </View>
      </SafeAreaView>
    </Fragment>
  );
};

const styling = (theme) =>
  StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: Colors[theme]?.primaryBackground,
    },
    smallerBox: {
      backgroundColor: Colors[theme]?.secondaryBackground,
      height: 150,
      borderRadius: 10,
    },
    pageTitle: {
      color: "white",
      fontSize: 30,
      fontStyle: "normal",
      fontWeight: "400",
      lineHeight: 30,
      letterSpacing: 1.5,
      textAlign: "center",
    },
    outerContainer: {
      flex: 1,
      alignItems: "center",
      paddingTop: 30,
    },
    input: {
      width: "87%",
      height: 50,
      backgroundColor: "transparent",
      color: Colors[theme]?.primaryText,
      fontFamily: "Poppins-Regular",
      fontSize: 12,
      fontStyle: "normal",
      fontWeight: "400",
      letterSpacing: 0.6,
      paddingLeft: 20,
    },
    inputContainer: {
      width: 290,
      height: 50,
      flexDirection: "row",
      alignItems: "center",
      borderRadius: 23,
      borderWidth: 0.5,
      borderColor: Colors[theme]?.border,
      backgroundColor: Colors[theme]?.inputContainer,
      opacity: 1,
      margin: 10,
      padding: 0,
    },
    buttonText: {
      fontFamily: "Poppins-Regular",
      fontStyle: "normal",
      color: Colors[theme]?.buttonText,
      fontSize: 12,
      fontWeight: "400",
      letterSpacing: 0.6,
      textAlign: "left",
      paddingLeft: 14,
      marginRight: 168,
    },
    submit: {
      fontFamily: "Poppins-Regular",
      fontStyle: "normal",
      borderRadius: 23,
      borderWidth: 0.5,
      borderColor: Colors[theme]?.border,
      height: 50,
      width: 290,
      alignItems: "center",
      justifyContent: "center",
      backgroundColor: theme === "light" ? "#E74B4B" : "#CBFF4D",
      margin: 10,
    },
    inputContainerError: {
      backgroundColor: "rgba(231, 70, 70, 0.24)",
      borderWidth: 1,
      borderColor: "#E74646",
    },
    messageContainer: {
      width: 290,
      height: 60,
      justifyContent: "center",
      margin: 10,
    },
    message: {
      width: "100%",
      color: Colors[theme]?.primaryText,
      fontFamily: "Poppins-Regular",
      fontSize: 11,
      fontStyle: "normal",
      fontWeight: "400",
      letterSpacing: 0.6,
      textAlign: "left",
      padding: 5,
    },
    messageError: {
      color: "#E74646",
    },
    messageSuccess: {
      color: "#198754",
    },
  });

export default ChangePasswordPage;
