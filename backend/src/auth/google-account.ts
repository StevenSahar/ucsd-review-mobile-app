import express from "express";
import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";
import User from "../models/user.model";

const router = express.Router();

router.post("/", async (req, res) => {
  try {
    const { email, name } = req.body;

    let user = await User.findOne({ email: email });

    if (!user) {
      var randomstring = Math.random().toString(36).slice(-8); //random string of length 8

      //Encrypts Password
      const salt = await bcrypt.genSalt(Number(process.env.SALT));
      const hashPassword = await bcrypt.hash(randomstring, salt);

      //Save user information to database
      user = await new User({
        email: email,
        password: hashPassword,
        username: name,
        verified: true,
      }).save();
    }

    const token = jwt.sign(
      {
        username: user.username,
        email: user.email,
      },
      process.env.JWT_SECRET
    );

    return res.status(200).send({ token: token });
  } catch (error) {
    return res.status(400).send({ error: "Invalid email or password" });
  }
});

export default router;
