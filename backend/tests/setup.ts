import User from "../src/models/user.model";

beforeAll(async() => {
    process.env.NODE_ENV = "test";
});

afterAll(() => {
    process.emit("SIGINT");
});