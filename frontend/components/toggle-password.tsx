import React from "react";
import { GestureResponderEvent, TouchableOpacity } from "react-native";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import { ModeLoader } from "../utils/theme/mode-loader";

export const toggleButton = (
  state: boolean,
  showFunction: ((event: GestureResponderEvent) => void) | undefined
) => {
  const theme = ModeLoader();

  return (
    <TouchableOpacity onPress={showFunction}>
      {state ? (
        <MaterialCommunityIcons
          name="eye-off"
          size={20}
          color={theme === "light" ? "#323232" : "#E8E9EB"}
        />
      ) : (
        <MaterialCommunityIcons
          name="eye"
          size={20}
          color={theme === "light" ? "#E74B4B" : "#CBFF4D"}
        />
      )}
    </TouchableOpacity>
  );
};
