import * as Font from "expo-font";

export const loadFontsAsync = async () => {
  try {
    await Font.loadAsync({
      "Poppins-Regular": require("../assets/fonts/Poppins-Regular.ttf"),
      "Poppins-Medium": require("../assets/fonts/Poppins-Medium.ttf"),
      "Poppins-SemiBold": require("../assets/fonts/Poppins-SemiBold.ttf"),
    });
  } catch (error) {
    console.warn("Error loading fonts:", error);
  }
};
