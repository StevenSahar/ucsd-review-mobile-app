# UCSD Roast and Review

## Description

This is a react native application that allows users to review and rate various aspects of the UCSD campus. Examples include the dining halls, the lecture halls, study spots, and the dorms. Users can also view other users' reviews and ratings.

Unlike Yelp and Google Maps, this application is specifically for UCSD students and is not meant to be used for commercial purposes. This application is meant to be a fun way for students to share their opinions about the campus and to help other students make decisions about where to go on campus. The granularity of the locations is also much smaller than Yelp and Google Maps, which allows for more specific reviews and ratings.

<img src="demo.png" width="300" />
<img src="demodark.png" width="300" />

### Product Demo

Check out a demo of our product [here](https://youtu.be/zpivyxn-Cm8)

<iframe src="https://www.youtube.com/embed/zpivyxn-Cm8" title="Final Product Demo" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

## Installation

### Physical Android Device

- make sure apps from unknown sources are allowed to be installed. Settings > Apps > Special Access > Install Unknown apps
- go to [this link](https://expo.dev/artifacts/eas/oVvwSJWKAXxEj4ARSDg59R.apk) on your android device to download the application
- press on the downloaded file and install the app
- happy testing!

### iOS Simulator

- MacOS is a **requirement**
- clone the repo using `git clone`
- `cd frontend`
- `npm install`
- Change to your own [OpenAI free API key](https://openai.com/blog/openai-api) if needed.
- follow [this link](https://docs.expo.dev/workflow/ios-simulator/) to set up the iOS Simulator
- `npx expo start`
- press `i` to open the application
- happy testing!

### Android Emulator

- follow [this link](https://docs.expo.dev/workflow/android-studio-emulator/) to install the Android Emulator
- clone the repo using `git clone`
- `cd frontend`
- `npm install`
- Change to your own [OpenAI free API key](https://openai.com/blog/openai-api) if needed.
- start your Android Emulator from the command line using `emulator -avd <emulator name>`
- `npx expo start`
- press `a` to open the application on the Android Emulator
- happy testing!

## License

Copyright belong to all developers.
Cloud supported by San Diego Supercomputing Center.

## Project status

Stop maintainence and possibly Cloud support after September 2023. Do not overuse Ask AI function due to OpenAI API quota limits.

## How We Built It

We used React Native to build the front end of the application. We used the `react-native-maps` library to display the map and pins. For the backend, we used MongoDB to store the data and Express to create the API. The runtime environment is Node.js. We used JWT for authentication and bcrypt for password hashing. Additionally, we connected to OpenAI API to create a AI chat page. During the planning phase, we used Figma to create mockups of the application. To send emails to users, we use Nodemailer.

## Things We Learned

The learnings of our team can be split into two categories: technical and non-technical. For the technical learnings, we learned how to use React Native to build a mobile application. We also learned how to use MongoDB and Express to create backend APIs. More specifically, we learned best practices for frontend and backend. Our learnings also include developing a CI/CD pipeline that is robust and maintainable. For the non-technical learnings, we learned how to work together as a team and how to communicate effectively. We also learned how to plan out a project and how to divide up the work. Additionally, we learned how to use Figma to create mockups of the application. We came to value the importance of collaboration, and pair programming as a means to achieve that.

## Challenges We Faced

Our journey was not free of obstacles; both our frontend and backend developers faced multiple challenges. A few of the key challenges we faced were working with `react-native-maps` to acheive our desired result, designing an attractive and intuitive UI, implementing an image storing system that would scale, rendering information quickly, and integrating the ChatGPT feature.

## Accomplishments That We're Proud Of

In all, we are proud of completing the project we set out to develop 8 weeks ago. We are extremely happy with the final product, and the pace at which we got work done. We are also pleased with the fact that we were able to implement all the features we set out to implement. On a different note, we are proud of the fact that we were able to work together as a team and communicate effectively. We are also proud of the fact that we were able to learn new technologies and apply them to our project.

## Contributors

Devs:

- [Evan Zhou](https://www.linkedin.com/in/ehzhou413/)
- [Raj Nawal](https://www.linkedin.com/in/rajnawal/)
- [Joshua Chen](https://www.linkedin.com/in/joshuachen4/)
- [Averi (Zhizhen) Yu](https://www.linkedin.com/mwlite/in/averi-zhizhen-yu-89b2861b9)
- [Steven Dominic Sahar](https://www.linkedin.com/in/stevensahar/)
- Bryan Lee

PM:

- [Gemma Luengo-Woods](https://www.linkedin.com/in/gemma-luengo-woods/)
