import React, { Fragment, useState } from "react";
import { SafeAreaView, View, StyleSheet } from "react-native";
import { Text, TextInput } from "react-native-paper";
import { Button } from "@rneui/themed";
import { Formik } from "formik";
import { Entypo } from "@expo/vector-icons";
import { Colors } from "../utils/theme";
import { ModeLoader } from "../utils/theme/mode-loader";
import { validEmailAddress } from "../utils/form-validator";
import { blankPageHeader } from "../components/page-headers";

function ForgetPassword() {
  //Load theme
  const theme = ModeLoader();
  const styles = styling(theme);

  //State declaration
  const [email, setEmail] = useState("");
  const [message, setMessage] = useState("");
  const [error, setError] = useState(false);

  //API
  async function sendEmail() {
    const server = "132.249.242.205";
    const port = "5050";
    const route = `http://${server}:${port}/forgot-password`;
    const response = await fetch(route, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        email: email,
      }),
    });

    const data = await response.json();
  }

  //Validate valid email address
  const verifyEmail = () => {
    if (validEmailAddress(email)) {
      setError(true);
      setMessage("Invalid Email Address.");
      return false;
    } else {
      setError(false);
      setMessage(
        "Success! If your email is in our database, we’ve sent an email to reset your password. "
      );
      return true;
    }
  };

  return (
    <Fragment>
      <SafeAreaView style={styles.topSafeAreaView} />
      <SafeAreaView style={styles.safeContainer}>
        {blankPageHeader("Login")}
        <Formik
          initialValues={{}}
          onSubmit={() => {
            sendEmail();
          }}
        >
          {({ handleSubmit }) => (
            <View style={styles.container}>
              <Text style={styles.title}>Forgot password</Text>
              <View
                style={[
                  styles.inputContainer,
                  error && styles.inputContainerError,
                ]}
              >
                <TextInput
                  style={styles.input}
                  value={email}
                  onChangeText={setEmail}
                  placeholder="email"
                  placeholderTextColor={Colors[theme]?.placeholderText}
                  textColor={Colors[theme]?.primaryText}
                  contentStyle={{ fontFamily: "Poppins-Regular" }}
                  selectionColor={Colors[theme]?.placeholderText}
                  underlineColor="transparent"
                  activeUnderlineColor="transparent"
                  textContentType="emailAddress"
                  keyboardType="email-address"
                />
              </View>
              <View style={styles.messageContainer}>
                <Text style={[styles.message, error && styles.messageError]}>
                  {message}
                  {message && !error && (
                    <Text
                      onPress={sendEmail}
                      style={[
                        styles.message,
                        { color: "#2889F7", backgroundColor: "transparent" },
                      ]}
                    >
                      Resend?
                    </Text>
                  )}
                </Text>
              </View>
              <Button
                onPress={() => {
                  if (verifyEmail()) {
                    handleSubmit();
                  }
                }}
                title="Submit"
                icon={
                  <Entypo
                    name="chevron-right"
                    size={25}
                    color={Colors[theme]?.buttonText}
                  />
                }
                iconRight
                buttonStyle={styles.submitButton}
                titleStyle={styles.submitButtonText}
              />
            </View>
          )}
        </Formik>
      </SafeAreaView>
    </Fragment>
  );
}

const styling = (theme) =>
  StyleSheet.create({
    topSafeAreaView: {
      flex: 0,
      backgroundColor: Colors[theme]?.primaryBackground,
    },
    safeContainer: {
      flex: 1,
      backgroundColor: Colors[theme]?.primaryBackground,
    },
    container: {
      flex: 1,
      alignItems: "center",
      justifyContent: "flex-start",
    },
    title: {
      color: Colors[theme]?.primaryText,
      fontFamily: "Poppins-Regular",
      fontSize: 20,
      fontStyle: "normal",
      fontWeight: "400",
      letterSpacing: 1,
      marginTop: 15,
      marginBottom: 45,
    },
    inputContainer: {
      width: 290,
      height: 50,
      flexDirection: "row",
      alignItems: "center",
      borderTopLeftRadius: 23,
      borderBottomLeftRadius: 23,
      borderBottomRightRadius: 23,
      borderTopRightRadius: 23,
      borderWidth: 0.5,
      borderColor: Colors[theme]?.border,
      backgroundColor: Colors[theme]?.inputContainer,
      opacity: 1,
      margin: 10,
      marginBottom: 20,
      padding: 0,
    },
    input: {
      width: "87%",
      height: 50,
      borderTopLeftRadius: 23,
      borderBottomLeftRadius: 23,
      borderBottomRightRadius: 0,
      borderTopRightRadius: 0,
      backgroundColor: "transparent",
      color: Colors[theme]?.placeholderText,
      fontFamily: "Poppins-Regular",
      fontSize: 12,
      fontStyle: "normal",
      fontWeight: "400",
      letterSpacing: 0.6,
      paddingLeft: 5,
    },
    submitButton: {
      width: 290,
      height: 50,
      alignItems: "center",
      justifyContent: "flex-end",
      borderTopLeftRadius: 23,
      borderBottomLeftRadius: 23,
      borderBottomRightRadius: 23,
      borderTopRightRadius: 23,
      backgroundColor: Colors[theme]?.button,
      marginTop: 20,
    },
    submitButtonText: {
      color: Colors[theme]?.buttonText,
      fontFamily: "Poppins-Regular",
      fontSize: 12,
      fontStyle: "normal",
      fontWeight: "400",
      letterSpacing: 0.6,
      textAlign: "left",
      paddingLeft: 14,
      marginRight: 180,
    },
    messageContainer: {
      width: 220,
      height: 55,
      backgroundColor: "transparent",
      alignItems: "center",
      justifyContent: "center",
    },
    message: {
      color: Colors[theme]?.primaryText,
      fontFamily: "Poppins-Regular",
      fontSize: 12,
      fontStyle: "normal",
      fontWeight: "400",
      letterSpacing: 0.6,
      lineHeight: 18,
    },
    inputContainerError: {
      backgroundColor: "rgba(231, 70, 70, 0.24)",
      borderWidth: 1,
      borderColor: "#E74646",
    },
    messageError: {
      color: "#E74646",
    },
  });

export default ForgetPassword;
