import express from "express";
import Location from "../../models/location.model";
import User from "../../models/user.model";
import jwt from "jsonwebtoken";

const router = express.Router();

router.post("/", async (req, res) => {
    try {
        const token = req.headers["x-access-token"];
        const decoded = jwt.verify(token, process.env.JWT_SECRET);
        const user = await User.findOne({ email: decoded.email });
        const locationId = req.body.locationId;
        const location = await Location.findById(locationId);

        if(user.upvotedLocations.includes(locationId)) {
            user.upvotedLocations = user.upvotedLocations.filter((location: string) => location !== locationId);
            location.upvotes = location.upvotes - 1;
        } else {
            user.upvotedLocations.push(locationId);
            location.upvotes++;
            if(location.upvotes >= 5) {
                setTimeout(async() => {
                    if (location.upvotes >= 5) {
                        location.isVerified = true;
                        await location.save();
                    }       
                }, 1000*3);
            }
        }
        await user.save();
        await location.save();

        res.status(200).send({ message: "Vote recorded successfully" });
    } catch (error) {
        res.status(400).send({ message: "Invalid token" });
    }
});

export default router;