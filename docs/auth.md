## User Registration
Registers a new user by creating a new account with the provided name, email, and password.

- **URL**: `/signup`
- **Method**: `POST`
- **Request Body**:
  - `name` (string, required): The name of the user.
  - `email` (string, required): The email of the user.
  - `password` (string, required): The password of the user.

### Responses
- Success Response:
    - Status Code: `200`
    - Response Body: `{ "status": "ok" }`
- Error Response if user already exists:
    - Status Code: `404`
    - Response Body: `{ "status": "error", "error": "Duplicate Email" }`

## User Login
Logs in a user and returns an authentication token if the provided email and password are valid.

- **URL**: `/login`
- **Method**: `POST`
- **Request Body**:
  - `email` (string, required): The email of the user.
  - `password` (string, required): The password of the user.

### Responses
- Success Response:
    - Status Code: `200`
    - response body:
        - ` {
            "token" : "<auth-token>"
        }`
- Error Response:
    - Status Code: `400`
    - response body:
        - ` {
            "message" : "Invalid email or password."
        }`

## Forgot Password
A request is sent to `/forgot-password` with the user's email. If the email is valid, a one-time password reset link is generated and sent to the user's email. The user can then use the link to reset their password. The link is valid for 15 minutes. TODO: Figure out if we are going to Deep Link to the app or just code a web page to handle the reset.

- **URL**: `/forgot-password`
- **Method**: `POST`
- **Request Body**:
  - `email` (string, required): The email of the user.

### Responses
- Success Response:
    - Status Code: `200`
    - response body:
        - ` {"message" : "Email sent to user's email."}`
- Error Response if user does not exist:
    - Status Code: `400`
    - response body:
        - ` { "message" : "Invalid email."}`
- Error Response if server error:
    - Status Code: `500`
    - response body:
        - ` { "message" : "Internal server error."}`

## Reset Password
A request is sent to `/reset-password/:id/:token` with the user's email, and new password. If the email and token are valid, the user's password is updated to the new password. The token is valid for 15 minutes.

- **URL**: `/reset-password/:id/:token`(the `id` and `token` are a part of the link sent in the email)
- **Method**: `POST`
- **Request Body**:
  - `email` (string, required): The email of the user.
  - `password` (string, required): The new password of the user.

### Responses
- Success Response:
    - Status Code: `200`
    - response body:
        - ` {"message" : "Password reset successfully."}`
- Error Response if user does not exist:
    - Status Code: `400`
    - response body:
        - ` { "message" : "User not found."}`
- Error Response if token is invalid:
    - Status Code: `400`
    - response body:
        - ` { "message" : "Invalid token."}`