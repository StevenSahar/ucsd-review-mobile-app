import express from "express";
import User from "../../models/user.model";
import jwt from "jsonwebtoken";

const router = express.Router();

router.get("/", async (req, res) => {
  try {
    const token = req.headers["x-access-token"];
    const decoded = jwt.verify(token, process.env.JWT_SECRET);

    const user = await User.findOne({ email: decoded.email });
    res.status(200).send({ likedReviews: user.likedReviews });
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
});

export default router;
