import React, { useEffect, useState, Fragment } from "react";
import { SafeAreaView, View, StyleSheet, Image } from "react-native";
import { Text } from "react-native-paper";
import { useRoute } from "@react-navigation/native";
import axios from "axios";
import { Colors } from "../utils/theme";
import { ModeLoader } from "../utils/theme/mode-loader";
import { blankPageHeader } from "../components/page-headers";

const EmailVerification = () => {
  //Load Dark/Light Mode
  const theme = ModeLoader();
  const styles = styling(theme);

  //Declaration
  const [validUrl, setValidUrl] = useState(true);
  const route = useRoute();

  //Backend API
  useEffect(() => {
    const verifyEmailUrl = async () => {
      try {
        const server = "132.249.242.205";
        const port = "5050";
        const url = `http://${server}:${port}/email-verification/${route.params.id}/verify/${route.params.token}`;
        const { data } = await axios.get(url);
        setValidUrl(true);
      } catch (error) {
        setValidUrl(false);
      }
    };

    verifyEmailUrl();
  }, [route]);

  return (
    <Fragment>
      <SafeAreaView style={styles.topSafeAreaView} />
      <SafeAreaView style={styles.safeContainer}>
        {blankPageHeader("LandingPage")}
        {validUrl ? (
          <View style={styles.container}>
            <View style={styles.content}>
              <Image
                source={require("../assets/GreenTicks.png")}
                style={styles.image}
              />
              <Text style={styles.title}>Email Verification</Text>
              <Text style={styles.description}>
                Your email has been verified. You can now log in with your
                account.
              </Text>
            </View>
          </View>
        ) : (
          <View style={styles.container}>
            <View style={styles.content}>
              <Image
                source={require("../assets/RedCross.png")}
                style={styles.image}
              />
              <Text style={styles.title}>404 Not Found</Text>
              <Text style={styles.description}>
                This link is invalid. Please ensure you have the correct URL.
              </Text>
            </View>
          </View>
        )}
      </SafeAreaView>
    </Fragment>
  );
};

const styling = (theme) =>
  StyleSheet.create({
    topSafeAreaView: {
      flex: 0,
      backgroundColor: Colors[theme]?.primaryBackground,
    },
    safeContainer: {
      flex: 1,
      backgroundColor: Colors[theme]?.primaryBackground,
    },
    container: {
      flex: 1,
      alignItems: "center",
      justifyContent: "center",
      position: "relative",
    },
    content: {
      alignItems: "center",
      justifyContent: "center",
      position: "absolute",
      top: 200,
    },
    title: {
      width: 290,
      color: Colors[theme]?.primaryText,
      fontFamily: "Poppins-Regular",
      fontSize: 25,
      fontWeight: "600",
      lineHeight: 28,
      letterSpacing: 1,
      textAlign: "center",
      margin: 13,
    },
    description: {
      width: 290,
      color: Colors[theme]?.primaryText,
      fontFamily: "Poppins-Regular",
      fontSize: 15,
      letterSpacing: 0.9,
      textAlign: "center",
      margin: 5,
    },
    image: {
      width: 80,
      height: 80,
      margin: 20,
    },
  });

export default EmailVerification;
