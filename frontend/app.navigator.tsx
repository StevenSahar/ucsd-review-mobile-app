import React, { useState } from "react";
import {
  CardStyleInterpolators,
  createStackNavigator,
} from "@react-navigation/stack";
import { NavigationContainer } from "@react-navigation/native";
import { Text } from "react-native-paper";
import * as Linking from "expo-linking";

import LandingPage from "./pages/LandingPage";
import Signup from "./pages/SignUp";
import Login from "./pages/Login";
import EmailVerification from "./pages/EmailVerification";
import ForgotPassword from "./pages/ForgotPassword";
import ResetPassword from "./pages/ResetPassword";
import NewReview from "./pages/NewReview";
import ModeSwitch from "./components/ModeSwitch";

import ChangePasswordPage from "./pages/ChangePasswordPage";
import FavoritesPage from "./pages/FavoritesPage";
import HomePage from "./pages/HomePage";
import AIPlugin from "./pages/AIPlugin";
import NewLocationsHome from "./pages/NewLocationsHome";
import NewLocationsMap from "./pages/NewLocationsMap";
import NewLocationsSubmit from "./pages/NewLocationsSubmit";
import ProfilePage from "./pages/ProfilePage";
import SettingsPage from "./pages/SettingsPage";
import ReviewPage from "./pages/ReviewPage";
import MenuBar from "./components/MenuBar";

const prefix = Linking.createURL("/");

const { Navigator, Screen, Group } = createStackNavigator();

const linking = {
  prefixes: [prefix],
  config: {
    screens: {
      LandingPage: "landing",
      Signup: "signup",
      Login: "login",
      EmailVerification: "users/:id/verify/:token",
      ForgotPassword: "forgot-password",
      ResetPassword: "reset-password/:id/:token",
      NewReview: "new-review/:locationId",
      ReviewsPopup: "review-popup",
      FavoritesPage: "favorites",
      HomePage: "home",
      AIPlugin: "AIPlugin",
      NewLocationsHome: "newlocations/home",
      NewLocationsMap: "newlocations/map",
      NewLocationsSubmit: "newlocations/submit",
      ProfilePage: "profile",
      SettingsPage: "settings",
      ReviewPage: "reviews",
    },
  },
};

const AppNavigator = () => {
  const [shouldDisplayMenuBar, setShouldDisplayMenuBar] = useState(false);

  const handleNavigationStateChange = (state) => {
    const currentRouteName = state.routes[state.index].name;
    //To-do: Add the pages that should include the menu bar
    const screensWithMenuBar = [
      "HomePage",
      "ProfilePage",
      "NewLocationsHome",
      "AIPlugin",
    ];

    setShouldDisplayMenuBar(screensWithMenuBar.includes(currentRouteName));
  };

  const config = {
    animation: "spring",
    config: {
      stiffness: 1000,
      damping: 50,
      mass: 3,
      overshootClamping: false,
      restDisplacementThreshold: 0.01,
      restSpeedThreshold: 0.01,
    },
  };
  return (
    <NavigationContainer
      linking={linking}
      fallback={<Text>Loading...</Text>}
      onStateChange={handleNavigationStateChange}
    >
      <Navigator
        screenOptions={{
          headerShown: false,
        }}
        initialRouteName="LandingPage" //To-do: Change to reflect whether account is logged in or not
      >
        <Group>
          <Screen name="LandingPage" component={LandingPage}></Screen>
          <Screen name="Signup" component={Signup}></Screen>
          <Screen name="Login" component={Login}></Screen>
          <Screen
            name="EmailVerification"
            component={EmailVerification}
          ></Screen>
          <Screen name="ForgotPassword" component={ForgotPassword}></Screen>
          <Screen name="ResetPassword" component={ResetPassword}></Screen>
        </Group>
        <Group>
          <Screen name="NewReview" component={NewReview}></Screen>
          <Screen name="ModeSwitch" component={ModeSwitch}></Screen>
        </Group>
        <Group>
          <Screen
            name="HomePage"
            component={HomePage}
            options={{ animationEnabled: false }}
          ></Screen>
          <Screen
            name="AIPlugin"
            component={AIPlugin}
            options={{ animationEnabled: false }}
          ></Screen>
        </Group>
        <Group>
          <Screen name="SettingsPage" component={SettingsPage}></Screen>
          <Screen
            name="ChangePasswordPage"
            component={ChangePasswordPage}
          ></Screen>
          <Screen
            name="ProfilePage"
            component={ProfilePage}
            options={{ animationEnabled: false }}
          ></Screen>
          <Screen name="ReviewsPage" component={ReviewPage}></Screen>
          <Screen name="FavoritesPage" component={FavoritesPage}></Screen>
        </Group>
        <Group>
          <Screen
            name="NewLocationSubmit"
            component={NewLocationsSubmit}
          ></Screen>
          <Screen
            name="NewLocationsHome"
            component={NewLocationsHome}
            options={{ animationEnabled: false }}
          ></Screen>
          <Screen name="NewLocationsMap" component={NewLocationsMap}></Screen>
        </Group>
      </Navigator>
      {shouldDisplayMenuBar && <MenuBar />}
    </NavigationContainer>
  );
};

export default AppNavigator;
