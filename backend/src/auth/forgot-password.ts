import express from "express";
import jwt from "jsonwebtoken";
import User from "../models/user.model";
import nodemailer from "nodemailer";
const router = express.Router();

router.post("/", async (req, res) => {
  const { email } = req.body;

  const user = await User.findOne({ email: email });
  if (!user) {
    console.log("User not found.");
    return res.status(400).send({ error: "User not found." });
  }

  const tempSecret = process.env.JWT_SECRET + user.email + user.password;
  const payload = {
    email: user.email,
    id: user._id,
  };
  const token = jwt.sign(payload, tempSecret, { expiresIn: "15m" });
  const link = process.env.SERVER_URL + `reset-password/${user._id}/${token}`;

  let transporter = nodemailer.createTransport({
    service: "gmail",
    auth: {
      user: process.env.EMAIL,
      pass: process.env.EMAIL_PASSWORD,
    },
  });

  let mailOptions = {
    from: process.env.EMAIL,
    to: user.email,
    subject: "Reset Password Link",
    html: `<p>Click <a href="${link}" style="color: blue">here</a> to reset your password. This link will expire in 15 minutes. If the link does not work, copy and paste the following link into your browser: ${link}</p>`,
  };

  transporter.sendMail(mailOptions, (error, info) => {
    if (error) {
      console.log(error);
      return res.status(500).send({ error: "Internal server error." });
    } else {
      console.log("Email sent: " + info.response);
      return res.status(200).send({ message: "Email sent to user's email." });
    }
  });
});

export default router;
