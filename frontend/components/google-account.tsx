import React, { useEffect } from "react";
import * as WebBrowser from "expo-web-browser";
import * as Google from "expo-auth-session/providers/google";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { StyleSheet, TouchableOpacity, Image } from "react-native";
import { Text } from "react-native-paper";
import { Colors } from "../utils/theme";
import { ModeLoader } from "../utils/theme/mode-loader";
import { useNavigation } from "@react-navigation/native";

WebBrowser.maybeCompleteAuthSession();

const GoogleAccount = (signin: boolean) => {
  //Page Navigation
  const navigation = useNavigation();
  const HomePage = () => navigation.navigate("HomePage");

  //Load theme
  const theme = ModeLoader();
  const styles = styling(theme);

  //To-do: Before building the App, get the ClientID for Android & iOS from Google Cloud by getting the "package" from app.json
  const [request, response, promptAsync] = Google.useAuthRequest({
    androidClientId: "",
    iosClientId: "",
    webClientId:
      "654909466511-3bc6poau1c3fojr2ef7ulfsmdhuc8q03.apps.googleusercontent.com",
  });

  useEffect(() => {
    handleEffect();
  }, [response]);

  async function handleEffect() {
    // Uncomment this to test functionality of Google Sign In/Up
    // await AsyncStorage.removeItem("token");
    const user = await getToken();
    if (!user) {
      if (response?.type === "success") {
        getUserInfo(response.authentication.accessToken);
      }
    } else {
      console.log("Account already logged in.");
    }
  }

  const getToken = async () => {
    const data = await AsyncStorage.getItem("token");
    if (!data) {
      return null;
    }
    return data;
  };

  const getUserInfo = async (token) => {
    if (!token) {
      return;
    }
    try {
      const GoogleResponse = await fetch(
        "https://www.googleapis.com/userinfo/v2/me",
        {
          headers: { Authorization: `Bearer ${token}` },
        }
      );

      const user = await GoogleResponse.json();

      const server = "132.249.242.205";
      const port = "5050";
      const route = `http://${server}:${port}/google-account`;
      const DatabaseResponse = await fetch(route, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          email: user.email,
          name: user.name,
        }),
      });

      const data = await DatabaseResponse.json();

      if (data.token) {
        await AsyncStorage.setItem("token", data.token);
        HomePage();
      }
    } catch (error) {
      console.log("Google login error.");
    }
  };

  return (
    <TouchableOpacity
      activeOpacity={0.3}
      disabled={!request}
      onPress={() => {
        promptAsync();
      }}
      style={styles.googleButton}
    >
      <Text style={styles.googleButtonText}>
        Sign {signin ? "In" : "Up"} with Google
      </Text>
      <Image
        style={{ width: 18, height: 18 }}
        source={require("../assets/GoogleIcon.png")}
      />
    </TouchableOpacity>
  );
};

const styling = (theme) =>
  StyleSheet.create({
    googleButton: {
      width: 290,
      height: 50,
      flexDirection: "row",
      alignItems: "center",
      justifyContent: "flex-start",
      borderTopLeftRadius: 23,
      borderBottomLeftRadius: 23,
      borderBottomRightRadius: 23,
      borderTopRightRadius: 23,
      backgroundColor: Colors[theme]?.googleButton,
      borderWidth: 0.5,
      borderColor: Colors[theme]?.border,
    },
    googleButtonText: {
      color: Colors[theme]?.buttonText,
      fontFamily: "Poppins-Regular",
      fontSize: 12,
      fontStyle: "normal",
      fontWeight: "400",
      letterSpacing: 0.6,
      textAlign: "left",
      paddingLeft: 22,
      marginRight: 106,
    },
  });

export default GoogleAccount;
