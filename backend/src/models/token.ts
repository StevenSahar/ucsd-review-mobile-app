import mongoose from "mongoose";

const tokenSchema = new mongoose.Schema(
  {
    userId: {
      type: mongoose.Schema.ObjectId,
      required: true,
      ref: "User",
      unique: true,
    },
    token: { type: String, required: true },
    createdAt: { type: Date, default: Date.now, expires: 3600 },
  },
  {
    collection: "tokens",
  }
);
//Created token will automatically be deleted from tokens collection in 3600s (1 hour)

export default mongoose.model("Token", tokenSchema);
