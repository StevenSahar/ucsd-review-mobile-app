import React, { useEffect, useState, Fragment } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  SafeAreaView,
  Button,
  SafeAreaViewBase,
  Alert,
} from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import { useNavigation } from "@react-navigation/native";
import MapView, { Callout, Marker, PROVIDER_GOOGLE } from "react-native-maps";
import { TouchableOpacity } from "react-native-gesture-handler";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { Colors } from "../utils/theme";
import { ModeLoader } from "../utils/theme/mode-loader";
import styling from "../styles";
import { Entypo } from "@expo/vector-icons";
import { LightMapStyle, DarkMapStyle } from "../styles";
import { AccountPageHeader } from "../components/page-headers";

const LATITUDE = 32.880201;
const LONGITUDE = -117.235796;
const LATITUDE_DELTA = 0.017;
const LONGITUDE_DELTA = 0.017;

const UCSDRegion = {
  latitude: LATITUDE,
  longitude: LONGITUDE,
  latitudeDelta: LATITUDE_DELTA,
  longitudeDelta: LONGITUDE_DELTA,
};

export default function NewLocationsMap() {
  const theme = ModeLoader();
  const newLocation = newLocations(theme);
  const styles = styling(theme);

  const lightOrDark = () => {
    return theme == "light";
  };

  const [defaultLocations, setDefaultLocations] = useState([]);
  const [marker, setMarker] = useState(false);
  const navigation = useNavigation();
  const [verifiedLocations, setVerifiedLocations] = useState<Location[]>([]);

  const handleMapPress = (event) => {
    const { latitude, longitude } = event.nativeEvent.coordinate;
    setMarker({ latitude, longitude });
  };

  const showDefaultLocations = () => {
    return verifiedLocations.map((item, id) => {
      return (
        <Marker
          key={id}
          coordinate={{
            latitude: item.coordinates[0],
            longitude: item.coordinates[1],
          }}
          title={item.name}
          image={getMarker(item.tag)}
        >
          <Callout tooltip={true} />
        </Marker>
      );
    });
  };

  async function getLocations() {
    try {
      const response = await fetch(
        "http://132.249.242.205:5050/locations?" +
          new URLSearchParams({
            isVerified: "true",
          }),
        {
          method: "GET",
          headers: {
            "Content-Type": "application/json",
            "x-access-token": await AsyncStorage.getItem("token"),
          },
        }
      );
      const data = await response.json();
      setVerifiedLocations(data);
    } catch (e) {
      console.error("Error fetching locations: ", e);
    }
  }

  useEffect(() => {
    getLocations();
  }, []);

  const getMarker = (tag: string) => {
    if (theme === "light") {
      if (tag === "Dining Hall") {
        return require("../assets/greenPin.png");
      } else if (tag === "Lecture Hall") {
        return require("../assets/redPin.png");
      } else if (tag === "Dorms") {
        return require("../assets/bluePin.png");
      } else if (tag === "Study Spot") {
        return require("../assets/yellowPin.png");
      }
    } else {
      if (tag === "Dining Hall") {
        return require("../assets/newNeonGreen.png");
      } else if (tag === "Lecture Hall") {
        return require("../assets/newNeonRed.png");
      } else if (tag === "Dorms") {
        return require("../assets/newNeonBlue.png");
      } else if (tag === "Study Spot") {
        return require("../assets/newNeonYellow.png");
      }
    }
  };

  return (
    <Fragment>
      <SafeAreaView
        style={{
          flex: 0,
          backgroundColor: Colors[theme]?.secondaryBackground,
          height: "3%",
        }}
      />
      <SafeAreaView style={styles.container}>
        <AccountPageHeader
          title="New Location"
          page="NewLocationsHome"
          theme={theme}
        />
        <View style={newLocation.next}>
          <TouchableOpacity
            style={newLocation.buttonContainer}
            onPress={() => {
              if (!marker) {
                Alert.alert(
                  "Please select an unmarked location on the map",
                  null,
                  [
                    {
                      text: "Ok",
                    },
                  ]
                );
              } else {
                navigation.navigate("NewLocationSubmit", {
                  markerLocation: marker,
                });
              }
            }}
          >
            <Text
              style={{
                color: Colors[theme]?.mainButtonText,
                paddingRight: 210,
                paddingLeft: 10,
                fontFamily: "Poppins-Regular",
              }}
            >
              Continue
            </Text>
            <Entypo
              name="chevron-right"
              size={25}
              color={Colors[theme]?.mainButtonText}
            />
          </TouchableOpacity>
        </View>
        <MapView
          style={newLocation.map}
          initialRegion={UCSDRegion}
          customMapStyle={lightOrDark() ? LightMapStyle : DarkMapStyle}
          provider={PROVIDER_GOOGLE}
          onPress={handleMapPress}
        >
          {marker && <Marker coordinate={marker} />}
          {showDefaultLocations()}
          <Callout tooltip={true}>
            <View
              style={{ width: 1, height: 1, backgroundColor: "transparent" }}
            />
          </Callout>
        </MapView>
      </SafeAreaView>
    </Fragment>
  );
}

const newLocations = (theme) =>
  StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: Colors[theme]?.primaryBackground,
    },
    buttonContainer: {
      flexDirection: "row",
      alignItems: "center",
      justifyContent: "flex-end",
      marginLeft: 20,
      flex: 1,
    },
    next: {
      flexDirection: "row",
      alignContent: "center",
      backgroundColor: Colors[theme]?.mainButton,
      alignItems: "center",
      marginLeft: 20,
      marginRight: 20,
      borderRadius: 50,
      top: "150%",
      height: 50,
      zIndex: 1,
    },
    map: {
      width: "100%",
      height: "100%",
      marginTop: -70,
      zIndex: -1,
    },
  });
