import request from 'supertest';
import app from '../src/index';
import Review from '../src/models/review.model';
import jwt from 'jsonwebtoken';

jest.mock('jsonwebtoken', () => ({
  verify: jest.fn().mockReturnValue({ id: 'mockUserId' }),
}));

describe('Get Location Reviews API', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should get reviews for a location with default sorting (newest)', async () => {
    const mockReviews = [
      {
        locationId: '64becc3743626f3c61839fcd',
        userId: '64becc3743626f3c61839fcd',
        stars: 5,
        comment: 'This place is great!',
        date: '2023-11-11T00:00:00.000Z',
        heading: 'Great Place!',
        photos: [],
        upvotes: 0,
        username: 'johndoe',
      },
    ];

    const findMock = jest.spyOn(Review, 'find').mockResolvedValue(mockReviews);

    const response = await request(app)
      .get('/location-reviews')
      .query({ locationId: '64becc3743626f3c61839fcd' })
      .set('x-access-token', 'mockToken');

    expect(response.status).toBe(200);
    expect(response.body.reviews).toEqual(mockReviews);
    expect(findMock).toHaveBeenCalledWith({ locationId: '64becc3743626f3c61839fcd' });
  });


  it('should handle error and return 500 status on server error', async () => {
    const findMock = jest.spyOn(Review, 'find').mockRejectedValue(new Error('Database error'));

    const response = await request(app)
      .get('/location-reviews')
      .query({ locationId: '64becc3743626f3c61839fcd' })
      .set('x-access-token', 'mockToken');

    expect(response.status).toBe(500);
    expect(response.body.error).toBe('Database error');
    expect(findMock).toHaveBeenCalledWith({ locationId: '64becc3743626f3c61839fcd' });
  });
});