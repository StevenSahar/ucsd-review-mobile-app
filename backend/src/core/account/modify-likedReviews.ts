import express from "express";
import User from "../../models/user.model";
import Review from "../../models/review.model";
import jwt from "jsonwebtoken";

const router = express.Router();

router.post("/", async (req, res) => {
  try {
    const token = req.headers["x-access-token"];
    const decoded = jwt.verify(token, process.env.JWT_SECRET);

    const { _id } = req.body;

    const review = await Review.findById(_id);
    const user = await User.findOne({ email: decoded.email });

    //Downvote
    if (user.likedReviews.includes(_id)) {
      const newArray = user.likedReviews.filter(function (value, index, arr) {
        return value !== _id;
      });
      user.likedReviews = newArray;
      await user.save();

      const temp: number = review.upvotes as number;
      const updatedUpvotes = temp - 1;
      review.upvotes = updatedUpvotes;
      review.save();

      return res.status(200).send({ message: "Review successfully downvoted" });
    }

    //Upvote
    const newArray = [...user.likedReviews, _id];
    user.likedReviews = newArray;
    await user.save();

    const temp: number = review.upvotes as number;
    const updatedUpvotes = temp + 1;
    review.upvotes = updatedUpvotes;
    review.save();

    return res.status(200).send({ message: "Review successfully upvoted" });
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
});

export default router;
