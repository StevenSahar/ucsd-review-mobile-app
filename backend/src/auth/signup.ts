import express from "express";
import bcrypt from "bcrypt";
import User from "../models/user.model";
import Token from "../models/token";
import sendEmail from "../utils/sendEmail";
import crypto from "crypto";

const router = express.Router();

router.post("/", async (req, res) => {
  try {
    //Checks for duplicate email
    let user = await User.findOne({ email: req.body.email });
    if (user)
      return res.status(409).send({
        error:
          "The email provided is already associated with an existing account. Please use a different email address.",
      });

    //Encrypts Password
    const salt = await bcrypt.genSalt(Number(process.env.SALT));
    const hashPassword = await bcrypt.hash(req.body.password, salt);

    //Save user information to database
    user = await new User({
      email: req.body.email,
      password: hashPassword,
      username: req.body.username,
    }).save();

    //Create a new token in the Token collection
    const token = await new Token({
      userId: user._id,
      token: crypto.randomBytes(32).toString("hex"),
    }).save();

    //Send Email Verification to User
    const url = `${process.env.SERVER_URL}/email-verification/${user.id}/verify/${token.token}`;
    const text = `<p>Click <a href="${url}" style="color: blue">here</a> to verify your account. This link will expire in 60 minutes. If the link does not work, copy and paste the following link into your browser: ${url}</p>`;
    await sendEmail(user.email, "Verify Email", text);

    res.status(200).send({
      message:
        "Your account has been successfully created. A verification email was sent to your email. Please check your inbox to verify your account.",
    });
  } catch (err) {
    res.status(400).send({ error: "Unexpected error." });
  }
});

export default router;
