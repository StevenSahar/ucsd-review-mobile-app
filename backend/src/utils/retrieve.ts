import Images from "../models/images.model";
const express = require("express");

const router = express.Router();

router.get("/:id", async (req, res) => {
  try {
    const imageId = req.params.id;

    const image = await Images.findById(imageId);
    if (!image) {
      return res.status(404).send("Image not found");
    }

    res.set("Content-Type", image.contentType);
    res.send(image.imageData);
  } catch (error) {
    res.status(400).send(error.message);
  }
});

export default router;
