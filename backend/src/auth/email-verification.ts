import express from "express";
import User from "../models/user.model";
import Token from "../models/token";

const router = express.Router();

router.get("/:id/verify/:token/", async (req, res) => {
  try {
    const user = await User.findOne({ _id: req.params.id });

    //User does not exist in database (users collection)
    if (!user) {
      return res.status(400).send({ error: "Invalid Link" });
    }

    const token = await Token.findOne({
      userId: user._id,
      token: req.params.token,
    });

    //Token does not exist in database (tokens Collection)
    if (!token) {
      return res.status(400).send({ error: "Invalid Link" });
    }

    //Update verified & delete created token
    await User.updateOne({ _id: user._id }, { $set: { verified: true } });
    await Token.deleteOne({ token: token.token });

    res.status(200).send({ message: "Email verified successfully" });
    console.log("Success");
  } catch (error) {
    res.status(500).send({ error: "Internal Server Error" });
    console.log("Fail");
  }
});

export default router;
