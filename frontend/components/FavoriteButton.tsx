import React, { useEffect, useState } from "react";
import { TouchableOpacity } from "react-native";
import { FontAwesome } from "@expo/vector-icons";
import AsyncStorage from "@react-native-async-storage/async-storage";

export const FavoriteButton = ({ locationId }: { locationId: string }) => {
  const [favorited, setFavorited] = useState(false);

  const toggleFavorited = () => {
    setFavorited(!favorited);
  };

  const handleInitialState = (state: any) => {
    setFavorited(state);
  };

  //API get-favoriteLocations : To-do: Change server
  const getFavoriteLocations = async () => {
    try {
      const server = "132.249.242.205";
      const port = "5050";
      const url = `http://${server}:${port}/get-favoriteLocations`;
      const response = await fetch(url, {
        method: "GET",
        headers: {
          "x-access-token": await AsyncStorage.getItem("token"),
        },
      });

      const data = await response.json();
      const data_favoriteLocations = data.favoriteLocations;

      handleInitialState(data_favoriteLocations.includes(locationId));
      return;
    } catch (error) {
      console.error("Error " + error);
    }
  };

  //API modify-favoriteLocations : To-do: Change server
  const modifyFavoriteLocations = async () => {
    try {
      const server = "132.249.242.205";
      const port = "5050";
      const url = `http://${server}:${port}/modify-favoriteLocations`;
      const response = await fetch(url, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          "x-access-token": await AsyncStorage.getItem("token"),
        },
        body: JSON.stringify({
          locationId: locationId,
        }),
      });
    } catch (error) {
      console.error("Error " + error);
    }
  };

  useEffect(() => {
    getFavoriteLocations();
  }, [locationId]);

  return (
    <TouchableOpacity
      onPress={() => {
        toggleFavorited();
        modifyFavoriteLocations();
      }}
    >
      {favorited ? (
        <FontAwesome name="heart" size={17} color="#FB2B2B" />
      ) : (
        <FontAwesome name="heart-o" size={17} color="#FB2B2B" />
      )}
    </TouchableOpacity>
  );
};
