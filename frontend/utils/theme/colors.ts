const commonColor = {
  placeholderText: "#B3B3B3", //Light Gray
  googleButton: "#FFF",
};

const light = {
  primaryBackground: "#FFF3E2", //Cream
  secondaryBackground: "#E74646", //Red
  button: "#ED7878", //(tertiary color) - Pink
  mainButton: "#E74646", //Red
  card: "#ED7878", //(tertiary color) - Pink

  primaryText: "#000000", //Black
  secondaryText: "#FFF3E2", //Off-White
  buttonText: "#262322", //Black
  mainButtonText: "#FFF3E2", //Off-White

  pin_1: "#568259", //Dark Green
  pin_2: "#B26E63", //Brown
  pin_3: "#2C4251", //Navy Blue
  pin_4: "#E9B44C", //Dark Yellow

  border: "#B3B3B3",
  inputContainer: "#FFF",
  barIcon: "#D9D9D9",
  starColor: "#FFDA59", //Yellow
  starBackgroundColor: "#262322", //Black
  text: "black",

  ...commonColor,
};

const dark = {
  primaryBackground: "#181927", //Navy Blue
  secondaryBackground: "#222222", //Dark Gray
  button: "#CBFF4D", //Neon
  mainButton: "#CBFF4D", //Neon
  card: "#222", //Dark Gray

  primaryText: "#E8E9EB", //Silver
  secondaryText: "#E8E9EB", //Silver
  buttonText: "#181927", //Dark Blue
  mainButtonText: "#181927", //Dark Blue

  pin_1: "#FF006E", //Bright Red
  pin_2: "#FF00FF", //Hot Pink
  pin_3: "#00FFFF", //Aqua
  pin_4: "#1EFFBC", //Green Turqoise

  border: "#E8E9EB",
  inputContainer: "#181927",
  barIcon: "#E8E9EB",

  starColor: "#CBFF4D", //Neon Green
  starBackgroundColor: "#E8E9EB", //Silver
  text: "white",

  ...commonColor,
};

export default { light, dark };
