import React, { Fragment } from "react";
import { View } from "react-native";
import { Text } from "react-native-paper";

export const orSeparator = () => {
  return (
    <View
      style={{
        flexDirection: "row",
        alignItems: "center",
        width: 290,
        margin: 30,
      }}
    >
      <View style={{ flex: 1, height: 1, backgroundColor: "#D9D9D9" }} />
      <View>
        <Text
          style={{
            width: 30,
            textAlign: "center",
            color: "#B3B3B3",
            // fontFamily: "Poppins",
            fontSize: 12,
            fontStyle: "normal",
            fontWeight: "400",
            letterSpacing: 0.6,
          }}
        >
          or
        </Text>
      </View>
      <View style={{ flex: 1, height: 1, backgroundColor: "#D9D9D9" }} />
    </View>
  );
};
