import express from "express";
import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";
import User from "../models/user.model";
import Token from "../models/token";
import sendEmail from "../utils/sendEmail";
import crypto from "crypto";
const router = express.Router();

router.post("/", async (req, res) => {
  // const { identification, password } = req.body;

  // const user = identification.includes("@")
  //   ? await User.findOne({
  //       email: identification,
  //     })
  //   : await User.findOne({
  //       username: identification,
  //     });

  const user = await User.findOne({
    email: req.body.email,
  });

  const isPasswordCorrect = user
    ? await bcrypt.compare(req.body.password, user.password)
    : false;

  if (!isPasswordCorrect) {
    return res.status(401).send({ error: "Invalid Email or Password" });
  }

  if (!user.verified) {
    let token = await Token.findOne({ userId: user._id });
    if (!token) {
      token = await new Token({
        userId: user._id,
        token: crypto.randomBytes(32).toString("hex"),
      }).save();
    }

    const url = `${process.env.SERVER_URL}/users/${user.id}/verify/${token.token}`;
    const text = `<p>Click <a href="${url}" style="color: blue">here</a> to verify your account. This link will expire in 60 minutes. If the link does not work, copy and paste the following link into your browser: ${url}</p>`;
    await sendEmail(user.email, "Verify Email", text);

    return res.status(400).send({
      message:
        "Account has not been verified. A verification link has been sent to your email.",
    });
  }

  if (isPasswordCorrect) {
    const token = jwt.sign(
      {
        username: user.username,
        email: user.email,
        id: user._id,
      },
      process.env.JWT_SECRET
    );
    return res.status(200).send({ token: token });
  } else {
    return res.status(400).send({ error: "Invalid email or password" });
  }
});

export default router;
