import express from "express";
import Review from "../../models/review.model";
import jwt from "jsonwebtoken";

const router = express.Router();

interface SortCriteria {
    stars?: number;
    date?: number;
    upvotes?: number;
}

router.get("/", async (req, res) => {
    try {
        const token = req.headers["x-access-token"];
        const decoded = jwt.verify(token, process.env.JWT_SECRET);
        const locationId = req.query.locationId;
        const sortOption = req.query.sortOption || "newest"; 

        const sortCriteria: SortCriteria = {};

        if (sortOption === "highest") {
            sortCriteria.stars = -1; 
        } else if (sortOption === "lowest") {
            sortCriteria.stars = 1; 
        } else if (sortOption === "newest") {
            sortCriteria.date = -1; 
        } else if (sortOption === "mostupvoted") {
            sortCriteria.upvotes = -1;
        }

        const reviews = await Review.find({ locationId: locationId }).sort(sortCriteria as any);
        
        res.status(200).json({reviews: reviews});
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
});

export default router;