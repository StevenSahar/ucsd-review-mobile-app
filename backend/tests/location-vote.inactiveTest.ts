import request from 'supertest';
import app from '../src/index';
import Location from '../src/models/location.model';
import User from '../src/models/user.model';
import jwt from 'jsonwebtoken';

jest.mock('jsonwebtoken', () => ({
  verify: jest.fn().mockReturnValue({ email: 'test@example.com' }),
}));

describe('Vote on a Location API', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  const mockUser = {
    _id: 'mockUserId',
    email: 'test@example.com',
    upvotedLocations: [],
  };

  it('should record an upvote on a location and verify it if upvotes >= 5', async () => {

    const mockLocation = {
      _id: 'mockLocationId',
      upvotes: 0,
      save: jest.fn(),
      isVerified : false
    };

    const findOneUserMock = jest.spyOn(User, 'findOne').mockResolvedValue(mockUser);
    const findByIdLocationMock = jest.spyOn(Location, 'findById').mockResolvedValue(mockLocation);

    const response = await request(app)
      .post('/vote')
      .send({ locationId: 'mockLocationId' })
      .set('x-access-token', 'mockToken');

    expect(response.status).toBe(200);
    expect(response.body.message).toBe('Vote recorded successfully');
    expect(findOneUserMock).toHaveBeenCalledWith({ email: 'test@example.com' });
    expect(findByIdLocationMock).toHaveBeenCalledWith('mockLocationId');
    expect(mockUser.upvotedLocations).toContain('mockLocationId');
    expect(mockLocation.upvotes).toBe(1);

    mockLocation.upvotes = 5;
    await new Promise(resolve => setTimeout(resolve, 3500));
    expect(mockLocation.save).toHaveBeenCalled();
    expect(mockLocation.isVerified).toBe(true);
  });

  it('should remove an upvote on a location if user has already upvoted', async () => {
    const mockUser = {
      _id: 'mockUserId',
      email: 'test@example.com',
      upvotedLocations: ['mockLocationId'],
    };

    const mockLocation = {
      _id: 'mockLocationId',
      upvotes: 1,
      save: jest.fn(),
    };

    const findOneUserMock = jest.spyOn(User, 'findOne').mockResolvedValue(mockUser);
    const findByIdLocationMock = jest.spyOn(Location, 'findById').mockResolvedValue(mockLocation);

    const response = await request(app)
      .post('/vote')
      .send({ locationId: 'mockLocationId' })
      .set('x-access-token', 'mockToken');

    expect(response.status).toBe(200);
    expect(response.body.message).toBe('Vote recorded successfully');
    expect(findOneUserMock).toHaveBeenCalledWith({ email: 'test@example.com' });
    expect(findByIdLocationMock).toHaveBeenCalledWith('mockLocationId');
    expect(mockUser.upvotedLocations).not.toContain('mockLocationId');
    expect(mockLocation.upvotes).toBe(0);
  });

  it('should handle error and return 400 status on invalid token', async () => {
    const findOneUserMock = jest.spyOn(User, 'findOne').mockRejectedValue(new Error('User not found'));

    const response = await request(app)
      .post('/vote')
      .send({ locationId: 'mockLocationId' })
      .set('x-access-token', 'invalidToken');

    expect(response.status).toBe(400);
    expect(response.body.message).toBe('Invalid token');
    expect(findOneUserMock).toHaveBeenCalledWith({ email: 'test@example.com' });
  });

  it('should handle error and return 400 status on server error', async () => {
    const findOneUserMock = jest.spyOn(User, 'findOne').mockResolvedValue(mockUser);
    const findByIdLocationMock = jest.spyOn(Location, 'findById').mockRejectedValue(new Error('Location not found'));

    const response = await request(app)
      .post('/vote')
      .send({ locationId: 'mockLocationId' })
      .set('x-access-token', 'mockToken');

    expect(response.status).toBe(400);
    expect(response.body.message).toBe('Location not found');
    expect(findOneUserMock).toHaveBeenCalledWith({ email: 'test@example.com' });
    expect(findByIdLocationMock).toHaveBeenCalledWith('mockLocationId');
  });
});