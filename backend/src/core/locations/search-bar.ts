import express from "express";
import location from "../../models/location.model";
import jwt from "jsonwebtoken";
const router = express.Router();
router.get("/", async (req, res) => {
  try {
    const token = req.headers["x-access-token"];
    const decoded = await jwt.verify(token, process.env.JWT_SECRET);
    const name = req.query.name;
    const regex = new RegExp(`${name}`, "i");
    const filteredLocation = await location
      .find({ name: regex })
      .sort({ name: 1 })
      .exec();
    res.status(200).send(filteredLocation); //return an array of location objects
  } catch (error) {
    res.status(400).send({ error: "Failed to retrieve location." });
  }
});
export default router;
