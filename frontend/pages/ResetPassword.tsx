import React, { Fragment, useState } from "react";
import { SafeAreaView, View, StyleSheet, Image } from "react-native";
import { Text, TextInput } from "react-native-paper";
import { Button } from "react-native-elements";
import { useRoute } from "@react-navigation/native";
import { Formik } from "formik";
import { Entypo } from "@expo/vector-icons";
import { toggleButton } from "../components/toggle-password";
import { validPassword, confirmSamePassword } from "../utils/form-validator";
import { Colors } from "../utils/theme";
import { ModeLoader } from "../utils/theme/mode-loader";
import { blankPageHeader } from "../components/page-headers";

const ResetPassword = () => {
  //Load theme
  const theme = ModeLoader();
  const styles = styling(theme);

  //State declarations
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");

  const [showPassword, setShowPassword] = useState(true);
  const [showConfirmPassword, setShowConfirmPassword] = useState(true);

  const [changedOnce, setChangedOnce] = useState(false);
  const [changeSuccessful, setChangeSuccessful] = useState(false);

  const [passwordError, setPasswordError] = useState(false);
  const [confirmPasswordError, setConfirmPasswordError] = useState(false);
  const [message, setMessage] = useState("");

  const route = useRoute();

  //API
  const resetPassword = async () => {
    const server = "132.249.242.205";
    const port = "5050";
    const url = `http://${server}:${port}/reset-password/${route.params.id}/${route.params.token}`;
    const response = await fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        password: password,
      }),
    });

    const data = await response.json();

    if (data.message) {
      setMessage("Password Reset");
      setPassword("");
      setConfirmPassword("");
      setChangeSuccessful(true);
    } else {
      setChangedOnce(true);
      setMessage("Invalid Token");
    }
  };

  const formValidator = () => {
    setPasswordError(false);
    setConfirmPasswordError(false);
    setMessage("");

    //Validate password
    if (validPassword(password)) {
      setMessage(validPassword(password));
      setPasswordError(true);
      return false;
    }

    //Validate confirm password
    if (confirmSamePassword(password, confirmPassword)) {
      setMessage(confirmSamePassword(password, confirmPassword));
      setConfirmPasswordError(true);
      return false;
    }

    return true;
  };

  const togglePassword = () => {
    setShowPassword(!showPassword);
  };

  const toggleConfirmPassword = () => {
    setShowConfirmPassword(!showConfirmPassword);
  };

  return (
    <Fragment>
      <SafeAreaView style={styles.topSafeAreaView} />
      <SafeAreaView style={styles.safeContainer}>
        {blankPageHeader("Login")}
        {!changedOnce ? (
          changeSuccessful ? (
            <View style={[styles.container, { justifyContent: "center" }]}>
              <View style={styles.content}>
                <Image
                  source={require("../assets/GreenTicks.png")}
                  style={styles.image}
                />
                <Text style={styles.responseTitle}>{message}</Text>
                <Text style={styles.responseDescription}>
                  Reset successful. You can now log in with your new password.
                </Text>
              </View>
            </View>
          ) : (
            <Formik
              initialValues={{}}
              onSubmit={() => {
                if (formValidator()) {
                  resetPassword();
                }
              }}
            >
              {({ handleSubmit }) => (
                <View style={styles.container}>
                  <Text style={styles.title}>Reset password</Text>
                  <View
                    style={[
                      styles.inputContainer,
                      passwordError && styles.inputContainerError,
                    ]}
                  >
                    <TextInput
                      style={styles.input}
                      value={password}
                      onChangeText={setPassword}
                      placeholder="new password"
                      placeholderTextColor={Colors[theme]?.placeholderText}
                      textColor={Colors[theme]?.primaryText}
                      contentStyle={{ fontFamily: "Poppins-Regular" }}
                      selectionColor={Colors[theme]?.placeholderText}
                      underlineColor="transparent"
                      activeUnderlineColor="transparent"
                      textContentType="password"
                      keyboardType="default"
                      secureTextEntry={showPassword}
                    />
                    {toggleButton(showPassword, togglePassword)}
                  </View>
                  <View
                    style={[
                      styles.inputContainer,
                      confirmPasswordError && styles.inputContainerError,
                      { marginBottom: 5 },
                    ]}
                  >
                    <TextInput
                      style={styles.input}
                      value={confirmPassword}
                      onChangeText={setConfirmPassword}
                      placeholder="confirm new password"
                      placeholderTextColor={Colors[theme]?.placeholderText}
                      textColor={Colors[theme]?.primaryText}
                      contentStyle={{ fontFamily: "Poppins-Regular" }}
                      selectionColor={Colors[theme]?.placeholderText}
                      underlineColor="transparent"
                      activeUnderlineColor="transparent"
                      textContentType="password"
                      keyboardType="default"
                      secureTextEntry={showConfirmPassword}
                    />
                    {toggleButton(showConfirmPassword, toggleConfirmPassword)}
                  </View>
                  <View style={styles.messageContainer}>
                    <Text
                      style={[
                        styles.message,
                        (passwordError || confirmPasswordError) &&
                          styles.messageError,
                      ]}
                    >
                      {message}
                    </Text>
                  </View>
                  <Button
                    onPress={() => handleSubmit()}
                    title="Reset"
                    icon={
                      <Entypo
                        name="chevron-right"
                        size={25}
                        color={Colors[theme]?.buttonText}
                      />
                    }
                    iconRight
                    buttonStyle={styles.resetButton}
                    titleStyle={styles.resetButtonText}
                  />
                </View>
              )}
            </Formik>
          )
        ) : (
          <View style={[styles.container, { justifyContent: "center" }]}>
            <View style={styles.content}>
              <Image
                source={require("../assets/RedCross.png")}
                style={styles.image}
              />
              <Text style={styles.responseTitle}>{message}</Text>
              <Text style={styles.responseDescription}>
                This link has expired. Please request a new password reset.
              </Text>
            </View>
          </View>
        )}
      </SafeAreaView>
    </Fragment>
  );
};

const styling = (theme) =>
  StyleSheet.create({
    topSafeAreaView: {
      flex: 0,
      backgroundColor: Colors[theme]?.primaryBackground,
    },
    safeContainer: {
      flex: 1,
      backgroundColor: Colors[theme]?.primaryBackground,
    },
    container: {
      flex: 1,
      alignItems: "center",
      justifyContent: "flex-start",
      position: "relative",
    },
    content: {
      alignItems: "center",
      justifyContent: "center",
      position: "absolute",
      top: 200,
    },
    title: {
      color: Colors[theme]?.primaryText,
      fontFamily: "Poppins-Regular",
      fontSize: 20,
      fontStyle: "normal",
      fontWeight: "400",
      letterSpacing: 1,
      marginTop: 15,
      marginBottom: 47,
    },
    inputContainer: {
      width: 290,
      height: 50,
      flexDirection: "row",
      alignItems: "center",
      borderTopLeftRadius: 23,
      borderBottomLeftRadius: 23,
      borderBottomRightRadius: 23,
      borderTopRightRadius: 23,
      borderWidth: 0.5,
      borderColor: Colors[theme]?.border,
      backgroundColor: Colors[theme]?.inputContainer,
      opacity: 1,
      margin: 10,
      padding: 0,
    },
    input: {
      width: "87%",
      height: 50,
      borderTopLeftRadius: 23,
      borderBottomLeftRadius: 23,
      borderBottomRightRadius: 0,
      borderTopRightRadius: 0,
      backgroundColor: "transparent",
      color: Colors[theme]?.placeholderText,
      fontFamily: "Poppins-Regular",
      fontSize: 12,
      fontStyle: "normal",
      fontWeight: "400",
      letterSpacing: 0.6,
      paddingLeft: 5,
    },
    resetButton: {
      width: 290,
      height: 50,
      alignItems: "center",
      justifyContent: "flex-end",
      borderTopLeftRadius: 23,
      borderBottomLeftRadius: 23,
      borderBottomRightRadius: 23,
      borderTopRightRadius: 23,
      backgroundColor: Colors[theme]?.button,
      marginTop: 5,
    },
    resetButtonText: {
      color: Colors[theme]?.buttonText,
      fontFamily: "Poppins-Regular",
      fontSize: 12,
      fontStyle: "normal",
      fontWeight: "400",
      letterSpacing: 0.6,
      textAlign: "left",
      paddingLeft: 14,
      marginRight: 191,
    },
    responseTitle: {
      color: Colors[theme]?.primaryText,
      fontFamily: "Poppins-Regular",
      fontSize: 25,
      fontWeight: "600",
      lineHeight: 28,
      letterSpacing: 1,
      textAlign: "center",
      margin: 13,
    },
    responseDescription: {
      color: Colors[theme]?.primaryText,
      fontFamily: "Poppins-Regular",
      fontSize: 15,
      letterSpacing: 0.9,
      textAlign: "center",
      margin: 5,
    },
    image: {
      width: 80,
      height: 80,
      margin: 20,
    },
    inputContainerError: {
      backgroundColor: "rgba(231, 70, 70, 0.24)",
      borderWidth: 1,
      borderColor: "#E74646",
    },
    messageContainer: {
      width: 270,
      height: 57,
      justifyContent: "center",
    },
    message: {
      width: "100%",
      color: Colors[theme]?.primaryText,
      fontFamily: "Poppins-Regular",
      fontSize: 11,
      fontStyle: "normal",
      fontWeight: "400",
      letterSpacing: 0.6,
      textAlign: "left",
      padding: 5,
    },
    messageError: {
      color: "#E74646",
    },
  });

export default ResetPassword;
