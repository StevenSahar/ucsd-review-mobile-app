import {
  StyleSheet,
  Text,
  View,
  Image,
  ScrollView,
  SafeAreaView,
} from "react-native";
import styling from "../styles";
import { TouchableOpacity } from "react-native-gesture-handler";
import React, { Fragment, useEffect, useState } from "react";
// import { Rating } from "react-native-stock-star-rating";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { Colors } from "../utils/theme";
import { ModeLoader } from "../utils/theme/mode-loader";
import { Rating } from "react-native-rating-element";
import { AccountPageHeader } from "../components/page-headers";
import { useNavigation } from "@react-navigation/native";
type Favorite = {
  _id: string;
  name: string;
  tag: string;
  coordinates: Array<number>;
  numReviews: number;
  avgRating: number;
  reviews: Array<string>;
  image: string;
  upvotes: number;
  isVerified: boolean;
};

export default function FavoritesPage() {
  const navigation = useNavigation();
  const theme = ModeLoader();
  const styles = styling(theme);
  const style = favorite(theme);
  const [favorites, setFavorites] = useState<Favorite[]>([]);

  async function getFavorites() {
    try {
      const response = await fetch(
        "http://132.249.242.205:5050/settings/favorites",
        {
          method: "GET",
          headers: {
            "Content-Type": "application/json",
            "x-access-token": await AsyncStorage.getItem("token"),
          },
        }
      );
      const data = await response.json();
      setFavorites(data.favorites);
    } catch (error) {
      console.error("Error getting Favorites: ", error);
    }
  }

  useEffect(() => {
    getFavorites();
  }, []);

  return (
    <Fragment>
      <SafeAreaView
        style={{
          flex: 0,
          backgroundColor: Colors[theme]?.secondaryBackground,
          height: "3%",
        }}
      />
      <SafeAreaView style={styles.container}>
        <AccountPageHeader title="Favorites" page="ProfilePage" theme={theme} />
        <ScrollView>
          {favorites.map((item) => {
            return (
              <TouchableOpacity
                onPress={() => {
                  navigation.navigate("HomePage", {
                    favLocation: item.coordinates,
                    favID: item._id,
                  });
                }}
              >
                <View key={item._id} style={style.item}>
                  <View style={style.row}>
                    <View style={{ flex: 1 }}>
                      <Text style={style.title}>{item.name}</Text>
                    </View>
                  </View>
                  <View style={style.row}>
                    <View style={{ flex: 1 }}>
                      <Text style={style.tag}>{item.tag}</Text>
                    </View>
                    <Rating
                      rated={item.avgRating}
                      totalCount={7}
                      size={14}
                      readonly={true}
                      ratingColor={Colors[theme]?.starColor}
                      ratingBackgroundColor={Colors[theme]?.starBackgroundColor}
                      icon="ios-star"
                    />
                  </View>
                </View>
              </TouchableOpacity>
            );
          })}
        </ScrollView>
      </SafeAreaView>
    </Fragment>
  );
}

const favorite = (theme) =>
  StyleSheet.create({
    information: {},
    item: {
      flexDirection: "column",
      alignItems: "flex-start",
      marginHorizontal: 40,
      borderRadius: 20,
      marginTop: 24,
      padding: 30,
      backgroundColor: Colors[theme]?.card,
      fontSize: 24,
    },
    row: {
      flexDirection: "row",
      justifyContent: "space-between",
      alignItems: "center",
      paddingBottom: 8,
      top: -10,
    },
    title: {
      color: "white",
      fontSize: 16,
      lineHeight: 16,
      letterSpacing: 1,
    },
    tag: {
      color: "white",
      fontSize: 14,
      lineHeight: 14,
      letterSpacing: 1,
    },
  });
