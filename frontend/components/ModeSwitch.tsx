import React, { useEffect, useState } from "react";
import { Switch } from "@rneui/themed";
import { get, save } from "../utils/theme/storage";
import { SafeAreaView } from "react-native-safe-area-context";

//  To-do: Copy and paste the contents of the following code into the page where the
//  toggle button will be placed.

const ModeSwitch = () => {
  const [darkMode, setDarkMode] = useState(false);
  const [theme, setTheme] = useState("");
  // const style = styling(theme); To-do: uncomment this code

  const changeTheme = async () => {
    const theme = await get("Theme");

    if (theme === "light") {
      await save("Theme", "dark");
      setTheme("dark");
    } else {
      await save("Theme", "light");
      setTheme("light");
    }
  };

  const checkTheme = async () => {
    try {
      const theme = await get("Theme");
      if (theme === "light") {
        setDarkMode(false);
        setTheme("light");
      } else {
        setDarkMode(true);
        setTheme("dark");
      }
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    checkTheme();
  });

  return (
    <SafeAreaView>
      <Switch
        value={darkMode}
        onValueChange={(value) => {
          setDarkMode(value);
          changeTheme();
        }}
        color="#E8E9EB" //color of button
      />
    </SafeAreaView>
  );
};

export default ModeSwitch;
