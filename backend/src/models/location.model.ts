import mongoose from "mongoose";

const LocationSchema = new mongoose.Schema(
  {
    name: { type: String, required: true },
    tag: { type: String, required: true },
    coordinates: { type: Array, required: true },
    numReviews: { type: Number },
    avgRating: { type: Number },
    reviews: { type: Array },
    image: { type: Array},
    upvotes: { type: Number },
    isVerified: { type: Boolean },
    description: { type: String },
  },
  {
    collection: "locations",
  }
);

export default mongoose.model("Location", LocationSchema);
