import { View, Text, StyleSheet } from "react-native";
import React, { useState } from "react";
import { CheckBox } from "react-native-elements";
import { Icon } from "@rneui/themed";
import { ModeLoader } from "../utils/theme/mode-loader";
import { Colors } from "../utils/theme";

type Category = {
  name: string;
  color: string;
  checked: boolean;
  onCheckboxClick: any;
};

function Category(props: Category) {
  const theme = ModeLoader();
  const styles = styling(theme);

  const [checked, setchecked] = useState(false);
  let iconName = props.name == "Favorites" ? "heart" : "map-pin";
  let iconColor = props.name == "Favorites" ? "red" : props.color;
  return (
    <View style={styles.main}>
      <Icon
        color={iconColor}
        name={iconName}
        size={25}
        containerStyle={{ width: 40 }}
        type="font-awesome"
      />
      <Text style={styles.text}>{props.name}</Text>
      <CheckBox
        size={24}
        checked={props.checked}
        onPress={props.onCheckboxClick}
        containerStyle={{ paddingBottom: 5, paddingTop: 5, paddingRight: 10 }}
        checkedColor={Colors[theme]?.button}
        checkedIcon="square"
        uncheckedIcon="square-o"
      />
    </View>
  );
}

const styling = (theme: string) =>
  StyleSheet.create({
    main: {
      backgroundColor: Colors[theme]?.primaryBackground,
      width: "100%",
      height: "14%",
      justifyContent: "center",
      alignItems: "center",
      display: "flex",
      flexDirection: "row",
      marginTop: 1,
      paddingLeft: 20,
    },

    text: {
      color: Colors[theme]?.primaryText,
      fontSize: 18,
      flex: 3,
      paddingLeft: 20,
    },
  });

export default Category;
