import React, { Fragment, useEffect, useState } from "react";
import {
  Keyboard,
  SafeAreaView,
  StyleSheet,
  Text,
  TextInput,
  TouchableWithoutFeedback,
  View,
  Image,
} from "react-native";
import styling from "../styles";
import { TouchableOpacity } from "react-native-gesture-handler";
import { useNavigation } from "@react-navigation/core";
import { Colors } from "../utils/theme";
import { Switch } from "@rneui/themed";
import { get, save } from "../utils/theme/storage";
import { Icon } from "@rneui/themed";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { validEmailAddress } from "../utils/form-validator";
import { AccountPageHeader } from "../components/page-headers";

export default function SettingsPage() {
  const [darkMode, setDarkMode] = useState(false);
  const [theme, setTheme] = useState("");

  const styles = styling(theme);
  const settings = setting(theme);

  const navigation = useNavigation();
  const goToProfilePage = () => {
    navigation.navigate("ProfilePage");
  };
  const goToChangePasswordPage = () => {
    navigation.navigate("ChangePasswordPage");
  };

  const changeTheme = async () => {
    const theme = await get("Theme");

    if (theme === "light") {
      await save("Theme", "dark");
      setTheme("dark");
    } else {
      await save("Theme", "light");
      setTheme("light");
    }
  };

  const checkTheme = async () => {
    try {
      const theme = await get("Theme");
      if (theme === "light") {
        setDarkMode(false);
        setTheme("light");
      } else {
        setDarkMode(true);
        setTheme("dark");
      }
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    if (!loaded) {
      getUserInfo();
      setLoaded(true);
    }
    checkTheme();
  });

  const [username, setUsername] = useState("");
  const [email, setEmail] = useState("");
  const [loaded, setLoaded] = useState(false);

  const [message, setMessage] = useState("");
  const [success, setSuccess] = useState(false);
  const [usernameError, setUsernameError] = useState(false);
  const [emailError, setEmailError] = useState(false);

  const getUserInfo = async () => {
    const server = "132.249.242.205";
    const port = "5050";
    const route = `http://${server}:${port}/settings/`;
    const response = await fetch(route, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        "x-access-token": await AsyncStorage.getItem("token"),
      },
    });

    const data = await response.json();

    if (response?.ok) {
      setUsername(data.username);
      setEmail(data.email);
    }
  };

  const formValidator = () => {
    setMessage("");
    setSuccess(false);
    setUsernameError(false);
    setEmailError(false);

    if (username.length < 3) {
      setUsernameError(true);
      setMessage("username must contain at least 3 characters");
      return false;
    }

    if (validEmailAddress(email)) {
      setMessage(validEmailAddress(email));
      setEmailError(true);
      return false;
    }

    return true;
  };

  const updateUserInfo = async () => {
    const server = "132.249.242.205";
    const port = "5050";
    const route = `http://${server}:${port}/settings/`;
    const response = await fetch(route, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        "x-access-token": await AsyncStorage.getItem("token"),
      },
      body: JSON.stringify({
        username: username,
        email: email,
      }),
    });

    if (response?.ok) {
      setSuccess(true);
      setMessage("successfully updated user info");
    }
  };

  return (
    <Fragment>
      <SafeAreaView
        style={{
          flex: 0,
          backgroundColor: Colors[theme]?.secondaryBackground,
          height: "3%",
        }}
      />
      <SafeAreaView style={styles.container}>
        <AccountPageHeader title="Settings" page="ProfilePage" theme={theme} />
        <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
          <View style={settings.buttonGroup}>
            <View style={settings.buttons}>
              <Text style={settings.buttonText}>Username</Text>
              <TextInput
                style={settings.arrow}
                value={username}
                onChangeText={setUsername}
                autoCapitalize="none"
                autoComplete="username"
                autoCorrect={false}
                onBlur={() => {
                  if (formValidator()) {
                    updateUserInfo();
                  }
                }}
              />
            </View>
            <View style={settings.buttons}>
              <Text style={settings.buttonText}>Email</Text>
              <Text style={settings.arrow}>{email}</Text>
              {/* <TextInput
              style={settings.arrow}
              value={email}
              onChangeText={setEmail}
              autoCapitalize="none"
              autoComplete="username"
              autoCorrect={false}
              onBlur={() => {
                if (formValidator()) {
                  updateUserInfo();
                }
              }}
            /> */}
            </View>
            <TouchableOpacity
              style={settings.buttons}
              onPress={goToChangePasswordPage}
            >
              <Text style={settings.buttonText}>Password</Text>
              <Icon
                name="chevron-right"
                type="entypo"
                color={Colors[theme]?.primaryText}
                style={{ marginRight: 20, marginLeft: 200 }}
                size={24}
              />
            </TouchableOpacity>
            <View style={[settings.buttons, { alignItems: "center" }]}>
              <Text style={[settings.buttonText, { flex: 1 }]}>Dark Mode</Text>
              <Switch
                trackColor={{ true: "#CBFF4D", false: "white" }}
                value={darkMode}
                onValueChange={(value) => {
                  setDarkMode(value);
                  changeTheme();
                  navigation.reset({
                    index: 0,
                    routes: [{ name: "SettingsPage" }],
                  });
                }}
                color="rgb(10, 132, 255)" //color of button
                style={{
                  marginRight: 24,
                  padding: 0,
                  transform: [{ scaleX: 0.9 }, { scaleY: 0.9 }],
                }}
              />
            </View>
            <View style={settings.messageContainer}>
              <Text
                style={[
                  settings.message,
                  (usernameError || emailError) && settings.messageError,
                  success && settings.messageSuccess,
                ]}
              >
                {message}
              </Text>
            </View>
          </View>
        </TouchableWithoutFeedback>
      </SafeAreaView>
    </Fragment>
  );
}

const setting = (theme) =>
  StyleSheet.create({
    buttonGroup: {
      flex: 1,
      padding: 10,
    },
    buttons: {
      width: "100%",
      overflow: "hidden",
      textAlign: "center",
      flexDirection: "row",
      paddingTop: 20,
    },
    buttonText: {
      width: "30%",
      flex: -1,
      color: Colors[theme]?.primaryText,
      fontFamily: "Poppins-Regular",
      marginLeft: 30,
      fontSize: 18,
      fontStyle: "normal",
      fontWeight: "400",
      lineHeight: 24,
      letterSpacing: 0.9,
    },
    arrow: {
      width: "70%",
      height: 30,
      color: Colors[theme]?.primaryText,
      fontFamily: "Poppins-Regular",
      flex: 1,
      textAlign: "right",
      marginRight: 23,
      fontSize: 18,
      fontStyle: "normal",
      fontWeight: "600",
      lineHeight: 24,
      letterSpacing: 0.9,
    },
    messageContainer: {
      width: 290,
      height: 60,
      justifyContent: "center",
      margin: 10,
    },
    message: {
      width: "100%",
      color: Colors[theme]?.primaryText,
      fontFamily: "Poppins-Regular",
      fontSize: 11,
      fontStyle: "normal",
      fontWeight: "400",
      letterSpacing: 0.6,
      textAlign: "left",
      padding: 5,
      margin: 15,
    },
    messageError: {
      color: "#E74646",
    },
    messageSuccess: {
      color: "#198754",
    },
  });
