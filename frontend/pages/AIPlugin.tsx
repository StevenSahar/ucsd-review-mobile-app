// reference: @bitheap-tech @youtube
import React, { useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  Button,
} from "react-native";
import "react-native-url-polyfill/auto";
import { ModeLoader } from "../utils/theme/mode-loader";
import { Colors } from "../utils/theme";

import { Configuration, OpenAIApi } from "openai";
import { API_KEY } from "../AIPlugin/config";

const configuration = new Configuration({
  // don't steal my key, pls. Belong to Averi Yu (zhy008@ucsd.edu)
  apiKey: API_KEY,
});

const openai = new OpenAIApi(configuration);

const AIPlugin = () => {
  const theme = ModeLoader();
  const styles = styling(theme);

  const [input, setInput] = useState("");
  const [output, setOutput] = useState("");

  const handleInput = async () => {
    try {
      const constPrompt = "Please enter your data: ";
      const userInput = constPrompt + input;
      const response = await openai.createCompletion({
        model: "text-davinci-003",
        prompt: `You: ${userInput}\nAI:`,
        temperature: 0,
        max_tokens: 60,
        top_p: 1.0,
        frequency_penalty: 0.5,
        presence_penalty: 0.0,
        stop: ["You:"],
      });
      setOutput(response.data.choices[0].text);
    } catch (error) {
      console.log(error);
    }

    setInput("");
  };

  return (
    <View
      style={[
        styles.container,
        { backgroundColor: theme === "light" ? "#faa19b" : "#181927" },
      ]}
    >
      <Text style={styles.title}> </Text>
      <Text style={styles.title}> A.I. UCSD Chat</Text>
      <View style={styles.chatContainer}>
        <View style={styles.inputButtonContainer}>
          <TextInput
            style={styles.input}
            placeholder="Ex. What is best UCSD dining hall?"
            onChangeText={(text) => setInput(text)}
            value={input}
            placeholderTextColor={Colors[theme]?.placeholderText}
            selectionColor={Colors[theme]?.placeholderText}
          />
          <TouchableOpacity style={styles.sendButton} onPress={handleInput}>
            <Text style={styles.sendButtonText}>Chat</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.outputContainer}>
          <Text style={styles.output}>{output}</Text>
        </View>
      </View>
      <Text style={styles.warnings}>
        {" "}
        Alert: free trial of 10 searches per user due to key belong to
        developers under no sponsorship under San Diego Supercomputing Center
        internship program.{" "}
      </Text>
    </View>
  );
};

const styling = (theme: string) =>
  StyleSheet.create({
    container: {
      flex: 3,
      alignItems: "center",
      justifyContent: "center",
    },
    title: {
      fontSize: 38,
      marginBottom: 20,
      fontFamily: "Poppins-Regular",
      color: Colors[theme]?.primaryText,
    },
    chatContainer: {
      width: "89%",
      height: "53.5%",
      borderWidth: 5,
      borderRadius: 15,
      overflow: "hidden",
      borderColor: theme === "light" ? "#9cf6f6" : "#CBFF4D",
      justifyContent: "space-between",
    },
    warnings: {
      fontSize: 12,
      marginBottom: 20,
      fontFamily: "Poppins-Regular",
      color: "gray",
    },
    inputContainer: {
      width: "90%",
      height: "50%",
      flexDirection: "column",
      alignItems: "center",
      borderRadius: 2,
    },
    input: {
      flex: 1,
      height: 40,
      borderRadius: 20,
      padding: 10,
      marginRight: 10,
      borderWidth: 0.5,
      borderColor: Colors[theme]?.border,
      backgroundColor: Colors[theme]?.inputContainer,
      color: Colors[theme]?.primaryText,
    },
    sendButton: {
      flexDirection: "row",
      backgroundColor: Colors[theme]?.mainButton,
      padding: 10,
      borderRadius: 20,
    },
    sendButtonText: {
      fontFamily: "Poppins-Regular",
      color: Colors[theme]?.mainButtonText,
      fontWeight: "bold",
      textAlign: "center",
    },
    outputContainer: {
      height: "96%",
      flexDirection: "column",
      padding: 13,
      backgroundColor: theme === "light" ? "#ffffff" : "#222222",
    },
    output: {
      fontSize: 19,
      color: Colors[theme]?.primaryText,
    },
    inputButtonContainer: {
      flexDirection: "row",
      alignItems: "center",
      padding: 10,
      backgroundColor: Colors[theme]?.primaryBackground,
    },
    addLocation: {
      position: "absolute",
      bottom: 20,
      right: 30,
    },
    pageTitle: {
      flexDirection: "row",
      alignItems: "center",
      justifyContent: "center",
      bottom: 325,
    },
  });

export default AIPlugin;
