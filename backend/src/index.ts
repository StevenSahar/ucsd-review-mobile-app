require("dotenv").config();

import express from "express";
import cors from "cors";
import mongoose from "mongoose";
import loginRouter from "./auth/login";
import signupRouter from "./auth/signup";
import forgotPasswordRouter from "./auth/forgot-password";
import resetPasswordRouter from "./auth/reset-password";
import emailVerificationRouter from "./auth/email-verification";
import googleAccountRouter from "./auth/google-account";
import addLocationRouter from "./core/locations/add-location";
import upvoteRouter from "./core/locations/location-vote";
import getLocationsRouter from "./core/locations/get-all-locations";
import searchBarRouter from "./core/locations/search-bar";
import addNewLocationRouter from "./core/locations/add-new-location";
import getLocationRouter from "./core/locations/get-location";
import addReviewRouter from "./core/reviews/add-review";
import getReviewRouter from "./core/reviews/get-reviews";
import userSettingsRouter from "./core/account/settings";
import getFavoriteLocationsRouter from "./core/account/get-favoriteLocations";
import getLikedReviewsRouter from "./core/account/get-likedReviews";
import modifyFavoriteLocationsRouter from "./core/account/modify-favoriteLocations";
import modifyLikedReviewsRouter from "./core/account/modify-likedReviews";
import uploadImageRouter from "./utils/upload";
import retrieveImageRouter from "./utils/retrieve";

const app = express();

const uri =
  process.env.NODE_ENV === "test"
    ? process.env.MONGODB_TEST_URI
    : process.env.MONGODB_URI;

mongoose.connect(uri);

const db = mongoose.connection;
db.on("error", (error) => console.error(error));
db.once("open", () => console.log("Connected to Database"));

app.use(cors());
app.use(express.json());

const port = process.env.NODE_ENV === "test" ? 5051 : process.env.PORT || 5050;

app.use("/login", loginRouter);
app.use("/signup", signupRouter);
app.use("/forgot-password", forgotPasswordRouter);
app.use("/reset-password", resetPasswordRouter);
app.use("/add-location", addLocationRouter);
app.use("/vote", upvoteRouter);
app.use("/settings", userSettingsRouter);
app.use("/locations", getLocationsRouter);
app.use("/email-verification", emailVerificationRouter);
app.use("/", searchBarRouter);
app.use("/add-new-location", addNewLocationRouter);
app.use("/google-account", googleAccountRouter);
app.use("/get-location", getLocationRouter);
app.use("/add-review", addReviewRouter);
app.use("/get-reviews", getReviewRouter);
app.use("/get-favoriteLocations", getFavoriteLocationsRouter);
app.use("/get-likedReviews", getLikedReviewsRouter);
app.use("/modify-favoriteLocations", modifyFavoriteLocationsRouter);
app.use("/modify-likedReviews", modifyLikedReviewsRouter);
app.use("/upload", uploadImageRouter);
app.use("/retrieve", retrieveImageRouter);

if (process.env.NODE_ENV !== "test") {
  app.listen(port, () => {
    console.log(`Server is running on port: ${port}`);
  });
}

process.on("SIGINT", async () => {
  await db.close();
  if (process.env.NODE_ENV !== "test") {
    console.log("Mongoose connection closed");
    process.exit(0);
  }
});

export default app;
