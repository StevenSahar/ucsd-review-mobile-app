import request from 'supertest';
import app from '../src/index'; 
import User from '../src/models/user.model';
import bcrypt from 'bcrypt';

describe('User Login API', () => {

  let loginTestUser: any;
  beforeEach(async () => {
    const salt = await bcrypt.genSalt(Number(process.env.SALT));
    const hashPassword = await bcrypt.hash('secretpassword', salt);

    loginTestUser = {
      username: 'John Doe',
      email: 'john.doe@example.com',
      password: hashPassword, 
      verified: true
    };
    await User.create(loginTestUser);
  });

  
  afterEach(async () => {
    await User.deleteOne({ email: loginTestUser.email });
  });

  it('should login a user and return status 200 with an authentication token', async () => {
    const loginData = {
      email: loginTestUser.email,
      password: 'secretpassword',
    };

    const response = await request(app).post('/login').send(loginData);

    expect(response.status).toBe(200);
    expect(response.body).toHaveProperty('token');

  });

  it('should return status 400 for invalid credentials', async () => {
    const loginData = {
      email: 'nonexistent@example.com', 
      password: 'wrongpassword', 
    };

    const response = await request(app).post('/login').send(loginData);

    expect(response.status).toBe(401);
    expect(response.body).toEqual({ error: 'Invalid Email or Password' });
  });
});
