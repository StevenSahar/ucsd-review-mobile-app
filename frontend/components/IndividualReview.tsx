//Display individual reviews in the reviews-popup.tsx page

import React, { useState, useEffect } from "react";
import { StyleSheet, View, TouchableOpacity, Image } from "react-native";
import { Text } from "react-native-paper";
import { Colors } from "../utils/theme";
import { ModeLoader } from "../utils/theme/mode-loader";
import { Rating } from "react-native-rating-element";
import { UpvoteButton } from "./UpvoteButton";
import { calculateTime } from "../utils/calculate-time";
import ImageView from "react-native-image-viewing";
import AsyncStorage from "@react-native-async-storage/async-storage";
import ImageFooter from "../components/ImageFooter";

type Review = {
  _id: string;
  locationId: string;
  userId: string;
  stars: number;
  comment: string;
  date: Date;
  heading: string;
  photos: Array<string>;
  upvotes: number;
  username: string;
};

export const IndividualReview = ({ review }: { review: Review }) => {
  //Load Theme
  const theme = ModeLoader();
  const styles = styling(theme);

  const [upvotes, setUpvotes] = useState(review.upvotes);
  const [upvoted, setUpvoted] = useState(false);

  const toggleUpvoted = () => {
    setUpvoted(!upvoted);
  };

  const changeUpvotes = () => {
    let tempReview = upvotes;
    upvoted ? tempReview-- : tempReview++;
    setUpvotes(tempReview);
  };

  const handleInitialState = (state: any) => {
    setUpvoted(state);
  };

  //API get-likedReviews : To-do: Change server
  const getLikedReviews = async () => {
    try {
      const server = "132.249.242.205";
      const port = "5050";
      const url = `http://${server}:${port}/get-likedReviews`;
      const response = await fetch(url, {
        method: "GET",
        headers: {
          "x-access-token": await AsyncStorage.getItem("token"),
        },
      });

      const data = await response.json();
      const data_likedReviews = data.likedReviews;

      handleInitialState(data_likedReviews.includes(review._id));
      return;
    } catch (error) {
      console.error("Error " + error);
    }
  };

  //API modify-likedReviews : To-do: Change server
  const modifyLikedReviews = async () => {
    try {
      const server = "132.249.242.205";
      const port = "5050";
      const url = `http://${server}:${port}/modify-likedReviews`;
      const response = await fetch(url, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          "x-access-token": await AsyncStorage.getItem("token"),
        },
        body: JSON.stringify({
          _id: review._id,
        }),
      });
      return;
    } catch (error) {
      console.error("Error " + error);
    }
  };

  const [image, setImage] = useState<{}[]>([]);
  const [imageUploaded, setImageUploaded] = useState(false);

  const handleInitialImageState = () => {
    setImageUploaded(review.photos.length != 0);
  };

  const handleSetImage = () => {
    setImage(
      review.photos.map((photo) => {
        return { uri: `http:132.249.242.205:5050/retrieve/${photo}` };
      })
    );
  };

  const images = [
    {
      uri: "https://images.unsplash.com/photo-1571501679680-de32f1e7aad4",
    },
  ];

  const [visible, setIsVisible] = useState(false);

  const toggleVisible = () => {
    setIsVisible(!visible);
  };

  useEffect(() => {
    getLikedReviews();
    handleInitialImageState();
    handleSetImage();
  }, []);

  return (
    <View style={styles.individualReview}>
      <Text style={styles.individualReviewTitle}>{review.heading}</Text>
      <Text style={styles.individualReviewUserDate}>
        {review.username} <Text>{"   "}</Text>
        {calculateTime(new Date(review.date))}
      </Text>
      <View style={styles.upvoteRatingContainer}>
        <Text
          style={[
            styles.upvotes,
            upvotes >= 10 && { width: 18 },
            upvotes >= 100 && { width: 25 },
          ]}
        >
          {upvotes}
        </Text>
        <UpvoteButton
          upvoted={upvoted}
          toggleUpvoted={toggleUpvoted}
          changeUpvotes={changeUpvotes}
          modifyLikedReviews={modifyLikedReviews}
        />
        <View style={styles.ratingContainer}>
          <Rating
            rated={review.stars}
            totalCount={7}
            size={18}
            readonly={true}
            ratingColor={Colors[theme]?.starColor}
            ratingBackgroundColor={Colors[theme]?.starBackgroundColor}
            icon="ios-star"
          />
        </View>
      </View>
      <View style={styles.individualReviewContentContainer}>
        <Text style={styles.individualReviewDescription}>{review.comment}</Text>
        <View style={styles.individualReviewImageContainer}>
          <TouchableOpacity
            onPress={() => {
              if (imageUploaded) {
                toggleVisible();
              }
            }}
            style={[
              { backgroundColor: "transparent" },
              imageUploaded && { backgroundColor: "black" },
            ]}
          >
            {image.length > 0 && (
              <Image
                source={image[0]}
                style={{ width: 50, height: 50 }}
                resizeMode="contain"
              />
            )}
          </TouchableOpacity>
          {image.length > 0 && (
            <ImageView
              images={image}
              imageIndex={0}
              visible={visible}
              onRequestClose={() => setIsVisible(false)}
              animationType="slide"
              presentationStyle="overFullScreen"
              backgroundColor="black"
              swipeToCloseEnabled={true}
              FooterComponent={({ imageIndex }) => (
                <ImageFooter
                  imageIndex={imageIndex}
                  imagesCount={image.length}
                />
              )}
            />
          )}
        </View>
      </View>
    </View>
  );
};

const styling = (theme) =>
  StyleSheet.create({
    individualReview: {
      flex: 1,
      width: "100%",
      backgroundColor: "transparent",
      marginBottom: 16,
    },
    individualReviewTitle: {
      fontFamily: "Poppins-Regular",
      fontSize: 14,
      fontStyle: "normal",
      fontWeight: "400",
      color: Colors[theme]?.primaryText,
      letterSpacing: 0.7,
    },
    individualReviewUserDate: {
      fontFamily: "Poppins-Regular",
      fontSize: 8,
      fontStyle: "normal",
      fontWeight: "400",
      color: Colors[theme]?.primaryText,
      letterSpacing: 0.4,
      marginTop: 3,
      marginBottom: 3,
    },
    upvoteRatingContainer: {
      height: 20,
      flexDirection: "row",
      alignItems: "center",
      justifyContent: "flex-start",
      backgroundColor: "transparent",
      marginBottom: 3,
    },
    upvotes: {
      width: 10,
      fontFamily: "Poppins-Regular",
      fontSize: 12,
      fontStyle: "normal",
      fontWeight: "400",
      color: Colors[theme]?.primaryText,
      letterSpacing: 0.6,
      paddingTop: 1,
      paddingRight: 1,
    },
    ratingContainer: {
      marginLeft: 5,
    },
    individualReviewContentContainer: {
      marginTop: 2,
      flexDirection: "row",
    },
    individualReviewDescription: {
      width: "75%",
      minHeight: 50,
      fontFamily: "Poppins-Regular",
      fontSize: 10,
      fontStyle: "normal",
      fontWeight: "300",
      lineHeight: 15,
      color: Colors[theme]?.primaryText,
      backgroundColor: "transparent",
    },
    individualReviewImageContainer: {
      width: "25%",
      alignItems: "flex-end",
      justifyContent: "flex-end",
      backgroundColor: "transparent",
      paddingRight: 5,
    },
  });
