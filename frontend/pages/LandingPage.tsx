import React, { Fragment, useEffect } from "react";
import {
  SafeAreaView,
  View,
  StyleSheet,
  Image,
  TouchableOpacity,
} from "react-native";
import { Text } from "react-native-paper";
import { Button } from "@rneui/themed";
import { Colors } from "../utils/theme";
import { ModeLoader } from "../utils/theme/mode-loader";
import AsyncStorage from "@react-native-async-storage/async-storage";

//useNavigation()
interface LandingPageProps {
  navigation: any;
}

function LandingPage(props: LandingPageProps) {
  //Page navigation
  const login = () => props.navigation.navigate("Login");
  const signup = () => props.navigation.navigate("Signup");
  const homePage = () => props.navigation.navigate("HomePage");

  //Load theme
  const theme = ModeLoader();
  const styles = styling(theme);

  useEffect(() => {
    const checkedLoggedIn = async () => {
      const checkToken = await AsyncStorage.getItem("token");
      if (checkToken) {
        homePage();
      }
    };
    checkedLoggedIn();
  }, []);

  return (
    <Fragment>
      <SafeAreaView style={styles.topSafeAreaView} />
      <SafeAreaView style={styles.safecontainer}>
        <View style={styles.topContainer}>
          <View style={styles.titleContainer}>
            <Text style={styles.title}>Roast and Review Any UCSD Spot</Text>
          </View>
        </View>
        <View style={styles.container}>
          <View style={styles.circleShape}>
            <Image
              source={require("../assets/AppLogo.png")}
              style={styles.logo}
            />
          </View>
          <View style={styles.buttonsContainer}>
            <Button
              onPress={signup}
              title="Get Started"
              buttonStyle={styles.button}
              titleStyle={styles.buttonText}
            />
            <Text style={styles.text}>Already Have An Account?</Text>
            <TouchableOpacity onPress={login}>
              <Text style={styles.loginText}>Login</Text>
            </TouchableOpacity>
          </View>
        </View>
      </SafeAreaView>
    </Fragment>
  );
}

const styling = (theme) =>
  StyleSheet.create({
    topSafeAreaView: {
      flex: 0,
      backgroundColor: Colors[theme]?.secondaryBackground,
    },
    safecontainer: {
      flex: 1,
      backgroundColor: Colors[theme]?.primaryBackground,
    },
    topContainer: {
      flex: 1,
      alignItems: "center",
      justifyContent: "flex-start",
      backgroundColor: Colors[theme]?.secondaryBackground,
      borderBottomLeftRadius: 50,
      borderBottomRightRadius: 50,
    },
    titleContainer: {
      width: 300,
      height: 174,
      margin: 65,
      marginLeft: 80,
    },
    title: {
      color: Colors[theme]?.secondaryText,
      fontFamily: "Poppins-Regular",
      fontSize: 36,
      fontStyle: "normal",
      fontWeight: "400",
      lineHeight: 50,
      letterSpacing: 1.8,
    },
    container: {
      flex: 1,
      alignItems: "center",
      justifyContent: "center",
      position: "relative",
    },
    circleShape: {
      width: 150,
      height: 150,
      borderRadius: 150 / 2,
      backgroundColor: Colors[theme]?.button,
      position: "absolute",
      top: -85,
    },
    logo: {
      width: 150,
      height: 150,
    },
    buttonsContainer: {
      alignItems: "center",
      justifyContent: "center",
      position: "absolute",
      bottom: 90,
    },
    button: {
      width: 315,
      height: 52,
      alignItems: "center",
      justifyContent: "center",
      borderRadius: 23,
      backgroundColor: Colors[theme]?.button,
    },
    buttonText: {
      fontFamily: "Poppins-Regular",
      color: Colors[theme]?.buttonText,
      fontSize: 16,
      fontStyle: "normal",
      fontWeight: "400",
      letterSpacing: 0.8,
    },
    text: {
      color: Colors[theme]?.primaryText,
      fontFamily: "Poppins-Regular",
      fontSize: 12,
      fontStyle: "normal",
      fontWeight: "400",
      letterSpacing: 0.6,
      margin: 20,
      marginBottom: 14,
    },
    loginText: {
      color: Colors[theme]?.button,
      fontFamily: "Poppins-Regular",
      fontSize: 16,
      fontStyle: "normal",
      fontWeight: "400",
      letterSpacing: 0.8,
    },
  });

export default LandingPage;
