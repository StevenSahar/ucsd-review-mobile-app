import express from "express";
import Location from "../../models/location.model";
import jwt from "jsonwebtoken";

const router = express.Router();

router.get("/:locationId", async (req, res) => {
  try {
    const token = req.headers["x-access-token"];
    const decoded = jwt.verify(token, process.env.JWT_SECRET);

    const { locationId } = req.params;
    const location = await Location.findById(locationId);

    console.log(location);
    res.status(200).send({ location: location });
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
});

export default router;
