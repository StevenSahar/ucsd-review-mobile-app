## Get User Information
Gets user information based on the authentication token provided in the request header.

- URL: `/settings`
- Method: `GET`
- Request Headers: `x-access-token` (string, required): The authentication token of the user.
### Responses
- Success Response:
    - Status Code: `200` OK
    - Response Body:
    ```json
    {
        "name": "John Doe",
        "email": "john@example.com",
        "profilePicture": "<base64-encoded-image>",
        "username": "johndoe"
    }
    ```
- Error Response:
    - Status Code: `401` Unauthorized
    - Response Body:
    ```json
    {
        "message": "Invalid token"
    }
    ```

## Update User Information
Updates user information based on the authentication token provided in the request header. 

- URL: `/settings`
- Method: `PUT`
- Request Headers: `x-access-token` (string, required): The authentication token of the user.
- Request Body (optional, provide one or more of the following fields to update):
    - `name` (string): The updated name of the user.
    - `username` (string): The updated username of the user.
    - `profilePicture` (string): The base64-encoded image representing the updated profile picture.
    - `password` (string): The updated password of the user (bcrypt-hashed).
    - `email` (string): The updated email address of the user.
### Responses
- Success Response:
    - Status Code: `200` OK
    - Response Body:
    ```json
    {
        "message": "User updated successfully"
    }
    ```
- Error Response:
    - Status Code: `400` 
    - Response Body:
    ```json
    {
        "message": "<errMsg>"
    }
    ```

## Get User's Favorite Locations
Gets the user's favorite locations based on the authentication token provided in the request header.

- URL: `/settings/favorites`
- Method: `GET`
- Request Headers: `x-access-token` (string, required): The authentication token of the user.

### Responses
- Success Response:
    - Status Code: `200` OK
    - Response Body:
    ```json
    {
        "favorites": [
            "name": "String",
            "tag": "String",
            "coordinates": "Array",
            "numReviews": "Number",
            "avgRating": "Number",
            "reviews": "Array",
            "image": "String",
            "upvotes": "Number",
            "isVerified": "Boolean"
        ]
    }
    ```
- Error Response:
    - Status Code: `401` Unauthorized
    - Response Body:
    ```json
    {
        "message": "Invalid token"
    }
    ```

### Get User's Reviews
Gets the user's reviews based on the authentication token provided in the request header.

- URL: `/settings/reviews`
- Method: `GET`
- Request Headers: `x-access-token` (string, required): The authentication token of the user.

### Responses
- Success Response:
    - Status Code: `200` OK
    - Response Body:
    ```json
    {
        "reviews": [
            "locationId": "String",
            "userId": "String",
            "stars": "Number",
            "comment": "String",
            "date": "Date",
            "heading": "String",
            "photos": "Array",
            "upvotes": "Number",
            "username": "String"
        ]
    }
- Error Response:
    - Status Code: `401` Unauthorized
    - Response Body:
    ```json
        {
            "message": "Invalid token"
        }
    ```