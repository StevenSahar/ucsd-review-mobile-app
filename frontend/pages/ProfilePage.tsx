import React, { Fragment } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  SafeAreaView,
  Button,
  Dimensions,
} from "react-native";
import { useEffect, useState } from "react";
import styling from "../styles";
import { useNavigation } from "@react-navigation/core";
import { TouchableOpacity } from "react-native-gesture-handler";
import { Colors } from "../utils/theme";
import { ModeLoader } from "../utils/theme/mode-loader";
import { Ionicons } from "@expo/vector-icons";
import { Entypo } from "@expo/vector-icons";
import { MaterialIcons } from "@expo/vector-icons";
import { useRoute } from "@react-navigation/native";
import { HomePageHeader } from "../components/page-headers";

export default function ProfilePage() {
  const theme = ModeLoader();
  const styles = styling(theme);
  const settings = setting(theme);

  const navigation = useNavigation();
  const goToSettingsPage = () => {
    navigation.navigate("SettingsPage");
  };
  const goToFavoritesPage = () => {
    navigation.navigate("FavoritesPage");
  };

  const goToReviewsPage = () => {
    navigation.navigate("ReviewsPage");
  };

  // const [userData, setUserData] = useState(null);

  return (
    <Fragment>
      <SafeAreaView
        style={{
          flex: 0,
          backgroundColor: Colors[theme]?.secondaryBackground,
          height: "3%",
        }}
      />
      <SafeAreaView style={styles.container}>
        <HomePageHeader title="Profile" theme={theme} />
        <View
          style={[
            {
              backgroundColor: "transparent",
              marginTop: 16,
              marginRight: (56 / 430) * Dimensions.get("window").width,
              marginLeft: (56 / 430) * Dimensions.get("window").width,
            },
          ]}
        >
          <TouchableOpacity onPress={goToSettingsPage} style={settings.buttons}>
            <Ionicons
              name="md-settings-sharp"
              size={24}
              color={Colors[theme]?.primaryText}
            />
            <Text style={settings.text}>Settings</Text>
            <Entypo
              name="chevron-right"
              size={24}
              color={Colors[theme]?.primaryText}
            />
          </TouchableOpacity>
          <TouchableOpacity
            onPress={goToFavoritesPage}
            style={settings.buttons}
          >
            <Entypo name="heart" size={24} color="#FB2B2B" />
            <Text style={settings.text}>Favorites</Text>
            <Entypo
              name="chevron-right"
              size={24}
              color={Colors[theme]?.primaryText}
            />
          </TouchableOpacity>
          <TouchableOpacity
            onPress={goToReviewsPage}
            style={[settings.buttons, { marginTop: 24 }]}
          >
            <View style={{ paddingTop: 4 }}>
              <MaterialIcons
                name="rate-review"
                size={24}
                color={Colors[theme]?.primaryText}
              />
            </View>
            <Text style={settings.text}>Reviews</Text>
            <Entypo
              name="chevron-right"
              size={24}
              color={Colors[theme]?.primaryText}
            />
          </TouchableOpacity>
        </View>
      </SafeAreaView>
    </Fragment>
  );
}

const setting = (theme) =>
  StyleSheet.create({
    name: {
      color: "#FFF3E2",
      fontFamily: "Poppins-Regular",
      fontSize: 28,
      fontStyle: "normal",
      fontWeight: "400",
      lineHeight: 30,
      letterSpacing: 1.5,
      textAlign: "center",
      left: -80,
    },
    username: {
      color: "#FFF3E2",
      fontFamily: "Poppins-Regular",
      fontSize: 20,
      fontStyle: "normal",
      fontWeight: "400",
      lineHeight: 30,
      letterSpacing: 1.5,
      textAlign: "center",
      left: -97,
      marginTop: 10,
    },
    buttons: {
      width: "100%",
      flexDirection: "row",
      alignItems: "center",
      justifyContent: "space-between",
      backgroundColor: "transparent",
      marginTop: 28,
    },
    text: {
      width: 90,
      color: Colors[theme]?.primaryText,
      fontFamily: "Poppins-Regular",
      fontSize: 18,
      fontStyle: "normal",
      fontWeight: "400",
      letterSpacing: 0.9,
      marginRight: (100 / 430) * Dimensions.get("window").width,
    },
  });
