import Images from "../models/images.model";
const express = require("express");
const multer = require("multer");

const router = express.Router();

const storage = multer.memoryStorage();
const upload = multer({ storage });

router.post("/", upload.array("images", 10), async (req, res) => {
  try {
    const newImages = req.files.map((file) => ({
      imageData: file.buffer,
      contentType: file.mimetype,
    }));

    const insertedImages = await Images.insertMany(newImages);
    const imageIds = insertedImages.map((image) => image._id);

    res.status(201).json(imageIds);
  } catch (error) {
    res.status(400).send(error.message);
  }
});

export default router;
