import React from "react";
import { TouchableOpacity } from "react-native";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import { ModeLoader } from "../utils/theme/mode-loader";

export const UpvoteButton = ({
  upvoted,
  toggleUpvoted,
  changeUpvotes,
  modifyLikedReviews,
}: {
  upvoted: boolean;
  toggleUpvoted: any;
  changeUpvotes: any;
  modifyLikedReviews: any;
}) => {
  const theme = ModeLoader();

  return (
    <TouchableOpacity
      onPress={() => {
        toggleUpvoted();
        changeUpvotes();
        modifyLikedReviews();
      }}
      style={{
        width: 20,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "transparent",
      }}
    >
      {upvoted ? (
        <MaterialCommunityIcons
          name="arrow-up-bold"
          size={20}
          color={theme === "light" ? "#E74B4B" : "#CBFF4D"}
        />
      ) : (
        <MaterialCommunityIcons
          name="arrow-up-bold"
          size={20}
          color={theme === "light" ? "#C0C0C0" : "#E8E9EB"}
        />
      )}
    </TouchableOpacity>
  );
};
