import React, { useEffect, useCallback, useState } from "react";
import { AppRegistry } from "react-native";
import { PaperProvider } from "react-native-paper";
import AppNavigator from "./app.navigator";
import { StatusBar } from "expo-status-bar";
import { get, save } from "./utils/theme/storage";
import { loadFontsAsync } from "./utils/font-loader";

const App = () => {
  //Font Loaded State
  const [fontLoaded, setFontLoaded] = useState(false);

  //Set default theme as light mode
  const setAppTheme = useCallback(async () => {
    const IS_FIRST = await get("IS_FIRST");
    if (IS_FIRST === null) {
      save("Theme", "light");
      save("IS_FIRST", true);
    }
  }, []);

  useEffect(() => {
    //Load the font
    const loadFonts = async () => {
      await loadFontsAsync();
      setFontLoaded(true);
    };

    loadFonts();
    setAppTheme();
  }, [setAppTheme]);

  //Ensure font is loaded
  if (!fontLoaded) {
    return null;
  }

  return (
    <PaperProvider>
      <StatusBar hidden />
      <AppNavigator />
    </PaperProvider>
  );
};

export default App;
AppRegistry.registerComponent("UCSD-ROAST-AND-REVIEW-APP", () => App);
