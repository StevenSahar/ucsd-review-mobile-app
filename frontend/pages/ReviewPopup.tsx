import React, {
  useState,
  useEffect,
  useCallback,
  useRef,
  useMemo,
} from "react";
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Image,
  Dimensions,
} from "react-native";
import { Text } from "react-native-paper";
import Modal from "react-native-modal";
import { Button, Icon } from "@rneui/themed";
import { Rating } from "react-native-rating-element";
import { FontAwesome5 } from "@expo/vector-icons";
import { Colors } from "../utils/theme";
import { ModeLoader } from "../utils/theme/mode-loader";
import AsyncStorage from "@react-native-async-storage/async-storage";
import BottomSheet, {
  BottomSheetModal,
  BottomSheetFlatList,
  BottomSheetScrollView,
  BottomSheetModalProvider,
} from "@gorhom/bottom-sheet";
import { IndividualReview } from "../components/IndividualReview";
import { useRoute, useNavigation } from "@react-navigation/native";
import { FavoriteButton } from "../components/FavoriteButton";
import { ReviewBreakdownDisplay } from "../components/ReviewBreakdownDisplay";

type Review = {
  _id: string;
  locationId: string;
  userId: string;
  stars: number;
  comment: string;
  date: Date;
  heading: string;
  photos: Array<string>;
  upvotes: number;
  username: string;
};

const ReviewsPopup = ({
  locationId,
  sheetRef,
  handleSheetChange,
}: {
  locationId: string;
  sheetRef: any;
  handleSheetChange: any;
}) => {
  //Load Theme
  const theme = ModeLoader();
  const styles = styling(theme);

  const route = useRoute();
  const navigation = useNavigation();

  // const locationId = route.params.locationId;

  const goToNewReview = () =>
    navigation.navigate("NewReview", { locationId: locationId });

  //State for bottom padding
  const [bottomPadding, setBottomPadding] = useState(false);

  //State for reviews
  const [review, setReview] = useState<Review[]>([]); //Store array of review objects
  const reviewCount = [0, 0, 0, 0, 0, 0, 0]; //Rating Counter: From Rating 1 to 7
  const [reviews, setReviews] = useState<number[]>([]); //Store array of number for Rating 1 to 7

  //State for location
  const [location, setLocation] = useState({
    name: "",
    tag: "",
    description: "",
    numReviews: 0,
    avgRating: 0,
    reviews: [] as number[],
    image: [] as string[],
  });

  //State for sorting
  const [sortingButton, setSortButton] = useState({
    highest: false,
    lowest: false,
    newest: true,
    mostUpvoted: false,
  });
  const [selectedSorting, setSelectedSorting] = useState("newest");

  // hooks
  // const sheetRef = useRef<BottomSheet>(null);

  const snapPoints = useMemo(() => ["66%", "88%"], []);

  const [hideHandleIndicator, setHideHandleIndicator] = useState(false);

  const handleAnimate = useCallback((fromIndex: number, toIndex: number) => {
    if (fromIndex === 0 && toIndex === 1) {
      setHideHandleIndicator(true);
    } else {
      setHideHandleIndicator(false);
    }
    console.log(fromIndex, toIndex);
  }, []);

  //API get-review : To-do: Change server before pushing
  const getReview = async () => {
    const server = "132.249.242.205";
    const port = "5050";
    const url = `http://${server}:${port}/get-reviews?sortOption=${selectedSorting}&locationId=${locationId}`;
    const response = await fetch(url, {
      method: "GET",
      headers: {
        "x-access-token": await AsyncStorage.getItem("token"),
      },
    });

    const data = await response.json();
    const data_reviews = data.reviews;
    if (data_reviews) {
      setReview(data_reviews);
      if (data_reviews.length >= 1) {
        setBottomPadding(true);
      }
      return;
    } else {
      console.log(data.error);
    }
  };

  //API get-location : To-do: Change server before pushing
  const getLocation = async () => {
    const server = "132.249.242.205";
    const port = "5050";
    const url = `http://${server}:${port}/get-location/${locationId}`;
    const response = await fetch(url, {
      method: "GET",
      headers: {
        "x-access-token": await AsyncStorage.getItem("token"),
      },
    });

    const data = await response.json();
    const data_location = data.location;
    if (data_location) {
      setLocation({
        name: data_location.name,
        tag: data_location.tag,
        description: data_location.description,
        numReviews: data_location.numReviews,
        avgRating: data_location.avgRating,
        reviews: data_location.reviews,
        image: data_location.image,
      });
      setReviews(data_location.reviews);
      // getReviewBreakdown(data_location.reviews);
      return;
    } else {
      console.log(data.error);
    }
  };

  //Get Review Breakdown
  const getReviewBreakdown = (reviews: any) => {
    reviews.map((review) => reviewCount[review - 1]++);
    setReviews(reviewCount);
  };

  const handleClosePress = useCallback(() => {
    sheetRef.current?.close();
  }, []);

  useEffect(() => {
    if (locationId !== "") {
      getLocation();
      getReview();
    }
  }, [sortingButton, locationId]);

  return (
    <BottomSheet
      ref={sheetRef}
      index={-1}
      snapPoints={snapPoints}
      onChange={handleSheetChange}
      onAnimate={handleAnimate}
      handleStyle={styles.bottomSheetHandle}
      handleIndicatorStyle={[
        styles.bottomSheetHandleIndicator,
        hideHandleIndicator && { backgroundColor: "transparent" },
      ]}
      enablePanDownToClose={true}
    >
      <BottomSheetScrollView
        style={styles.scrollView}
        stickyHeaderIndices={[2]}
        showsVerticalScrollIndicator={false}
      >
        <View style={styles.contentHeader}>
          <View style={[styles.locationContainer]}>
            <View
              style={{
                marginTop: "-6%",
                overflow: "hidden",
              }}
            >
              <Text
                adjustsFontSizeToFit={true}
                numberOfLines={2}
                style={styles.locationTitle}
              >
                {location.name}
              </Text>
              <Text style={styles.locationTag}>{location.tag}</Text>
              <Text
                adjustsFontSizeToFit={true}
                numberOfLines={4}
                style={styles.locationDescription}
              >
                {location.description}
              </Text>
            </View>
            <View style={{ top: "5%" }}>
              <FavoriteButton locationId={locationId} />
            </View>
          </View>
          <View style={styles.headerImage}>
            <Image
              source={{
                uri: `http:132.249.242.205:5050/retrieve/${location.image[0]}`,
              }}
              style={{ width: "100%", height: "100%", borderRadius: 16 }}
            />
          </View>
        </View>
        <View style={styles.contentReviews}>
          <View style={styles.reviewsAverage}>
            <Text style={styles.reviewsAverageText}>
              <Text style={[styles.reviewsAverageText, { fontSize: 30 }]}>
                {Math.round(location.avgRating * 10) / 10}
              </Text>
              /7
            </Text>
            <Text
              style={[
                styles.reviewsAverageText,
                { fontSize: 12, letterSpacing: 0.6, marginBottom: "10%" },
              ]}
            >
              based on {location.numReviews} reviews
            </Text>
            <Rating
              rated={location.avgRating}
              totalCount={7}
              size={18}
              readonly={true}
              ratingColor={Colors[theme]?.starColor}
              ratingBackgroundColor={Colors[theme]?.starBackgroundColor}
              icon="ios-star"
            />
          </View>
          <View style={styles.reviewsBreakdown}>
            {reviews.map((review, index) => {
              return (
                <ReviewBreakdownDisplay
                  starTally={reviews[reviews.length - 1 - index]}
                  starIndex={reviews.length - 1 - index}
                  totalReviews={location.numReviews}
                />
              );
            })}
          </View>
        </View>
        <View style={styles.sortContainerSticky}>
          <View style={styles.sortContainer}>
            <Text
              adjustsFontSizeToFit={true}
              numberOfLines={1}
              style={styles.sortTitle}
            >
              Sort By
            </Text>
            <View style={styles.sortButtonsContainer}>
              <Button
                title="highest"
                activeOpacity={1}
                onPress={() => {
                  setSortButton({
                    highest: true,
                    lowest: false,
                    newest: false,
                    mostUpvoted: false,
                  });
                  setSelectedSorting("highest");
                }}
                titleStyle={[
                  styles.sortButtonText,
                  sortingButton.highest && styles.sortButtonTextSelected,
                ]}
                buttonStyle={[
                  styles.sortButton,
                  sortingButton.highest && styles.sortButtonSelected,
                ]}
                containerStyle={styles.sortButtonContainer}
              />
              <Button
                title="lowest"
                activeOpacity={1}
                onPress={() => {
                  setSortButton({
                    highest: false,
                    lowest: true,
                    newest: false,
                    mostUpvoted: false,
                  });
                  setSelectedSorting("lowest");
                }}
                titleStyle={[
                  styles.sortButtonText,
                  sortingButton.lowest && styles.sortButtonTextSelected,
                ]}
                buttonStyle={[
                  styles.sortButton,
                  sortingButton.lowest && styles.sortButtonSelected,
                ]}
                containerStyle={styles.sortButtonContainer}
              />
              <Button
                title="newest"
                activeOpacity={1}
                onPress={() => {
                  setSortButton({
                    highest: false,
                    lowest: false,
                    newest: true,
                    mostUpvoted: false,
                  });
                  setSelectedSorting("newest");
                }}
                titleStyle={[
                  styles.sortButtonText,
                  sortingButton.newest && styles.sortButtonTextSelected,
                ]}
                buttonStyle={[
                  styles.sortButton,
                  sortingButton.newest && styles.sortButtonSelected,
                ]}
                containerStyle={styles.sortButtonContainer}
              />
              <Button
                title="most upvoted"
                activeOpacity={1}
                onPress={() => {
                  setSortButton({
                    highest: false,
                    lowest: false,
                    newest: false,
                    mostUpvoted: true,
                  });
                  setSelectedSorting("mostupvoted");
                }}
                titleStyle={[
                  styles.sortButtonText,
                  sortingButton.mostUpvoted && styles.sortButtonTextSelected,
                ]}
                buttonStyle={[
                  styles.sortButton,
                  sortingButton.mostUpvoted && styles.sortButtonSelected,
                ]}
                containerStyle={styles.sortButtonContainer}
              />
            </View>
            <TouchableOpacity
              style={styles.addNewReviewContainer}
              onPress={() => {
                goToNewReview();
                handleClosePress();
              }}
              activeOpacity={0.4}
            >
              <Icon
                name="pen"
                type="font-awesome-5"
                size={16}
                color={Colors[theme]?.starBackgroundColor}
              />
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.contentReviewList}>
          {review.map((review) => {
            return <IndividualReview key={review._id} review={review} />;
          })}
        </View>
        {bottomPadding && <View style={{ height: 100 }}></View>}
      </BottomSheetScrollView>
    </BottomSheet>
  );
};

export default ReviewsPopup;

const styling = (theme) =>
  StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: "transparent",
    },
    bottomSheetHandle: {
      backgroundColor: Colors[theme]?.primaryBackground,
      borderTopLeftRadius: 15,
      borderTopRightRadius: 15,
      height: Dimensions.get("window").width * 0.09,
      // marginBottom: "-3%",
    },
    bottomSheetHandleIndicator: {
      backgroundColor: Colors[theme]?.barIcon,
      width: "33%",
    },
    scrollView: {
      backgroundColor: Colors[theme]?.primaryBackground,
      flex: 1,
      // backgroundColor: "transparent",
      padding: "5%",
      paddingTop: "0%",
      paddingBottom: "0%",
    },
    contentHeader: {
      width: "100%",
      height: (Dimensions.get("window").width * 0.9) / 2,
      flexDirection: "row",
      alignItems: "center",
      backgroundColor: "transparent",
      marginBottom: Dimensions.get("window").width * 0.03,
    },
    locationContainer: {
      flex: 1,
      paddingTop: "2%",
      paddingLeft: 3,
    },
    locationTitle: {
      minHeight: Dimensions.get("window").width * 0.087,
      color: Colors[theme]?.primaryText,
      fontFamily: "Poppins-Regular",
      fontSize: 24,
      fontStyle: "normal",
      fontWeight: "400",
      letterSpacing: 1.2,
      marginRight: 15,
    },
    locationTag: {
      height: Dimensions.get("window").width * 0.058,
      color: Colors[theme]?.primaryText,
      fontFamily: "Poppins-Regular",
      fontSize: 16,
      fontStyle: "normal",
      fontWeight: "400",
      letterSpacing: 0.8,
      marginBottom: Dimensions.get("window").width * 0.023,
      marginTop: Dimensions.get("window").width * 0.023,
    },
    locationDescription: {
      flex: 0,
      // minHeight: Dimensions.get("window").width * 0.15,
      color: Colors[theme]?.primaryText,
      fontFamily: "Poppins-Regular",
      fontSize: 10,
      fontStyle: "normal",
      fontWeight: "400",
      letterSpacing: 0.5,
      paddingRight: 5,
    },
    headerImage: {
      opacity: 1,
      flex: 1,
      flexGrow: 1,
      justifyContent: "flex-start",
      alignItems: "flex-end",
    },
    contentReviews: {
      width: "100%",
      height: Dimensions.get("window").width * 0.9 * 0.45,
      flexDirection: "row",
      backgroundColor: Colors[theme]?.secondaryBackground,
      borderRadius: 30,
      marginBottom: Dimensions.get("window").width * 0.03,
    },
    reviewsAverage: {
      flex: 1,
      justifyContent: "center",
      alignItems: "flex-start",
      paddingLeft: 35,
    },
    reviewsAverageText: {
      color: Colors[theme]?.secondaryText,
      fontFamily: "Poppins-Regular",
      fontSize: 16,
      fontStyle: "normal",
      fontWeight: "400",
      letterSpacing: 0.8,
    },
    reviewsBreakdown: {
      flex: 1,
      justifyContent: "center",
      alignItems: "flex-end",
      paddingRight: "7%",
    },
    sortContainerSticky: {
      width: "100%",
      justifyContent: "flex-start",
      alignItems: "flex-start",
      backgroundColor: Colors[theme]?.primaryBackground,
      paddingLeft: 5,
      marginBottom: 7,
    },
    sortContainer: {
      width: "100%",
      height: 30,
      flexDirection: "row",
      alignItems: "center",
      backgroundColor: "transparent",
    },
    contentReviewList: {
      width: "100%",
      justifyContent: "flex-start",
      alignItems: "flex-start",
      backgroundColor: "transparent",
      paddingLeft: 5,
    },
    sortTitle: {
      flex: 2,
      color: Colors[theme]?.primaryText,
      fontFamily: "Poppins-Regular",
      fontSize: 16,
      letterSpacing: 0.8,
      marginRight: 3,
    },
    sortButtonsContainer: {
      flex: 10,
      flexDirection: "row",
      justifyContent: "space-between",
      backgroundColor: theme === "light" ? "#262322" : "#222222",
      borderRadius: 30,
      padding: 5,
    },
    sortButtonContainer: {
      justifyContent: "center",
      backgroundColor: "transparent",
    },
    sortButton: {
      backgroundColor: theme === "light" ? "#262322" : "#222222",
      justifyContent: "center",
      alignItems: "center",
      padding: 0,
      borderRadius: 30,
    },
    sortButtonText: {
      fontFamily: "Poppins-Regular",
      fontSize: 10,
      fontStyle: "normal",
      fontWeight: "400",
      color: Colors[theme]?.secondaryText,
    },
    sortButtonSelected: {
      backgroundColor: Colors[theme]?.mainButton,
    },
    sortButtonTextSelected: {
      color: Colors[theme]?.buttonText,
    },
    addNewReviewContainer: {
      flex: 1,
      backgroundColor: "transparent",
      marginLeft: 3,
    },
  });
