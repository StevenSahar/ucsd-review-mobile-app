import AsyncStorage from "@react-native-async-storage/async-storage";

export const rememberUser = async (email: string, password: string) => {
  try {
    await AsyncStorage.setItem("Email", email);
    await AsyncStorage.setItem("Password", password);
    await AsyncStorage.setItem("RememberMe", "true");
  } catch (error) {
    console.log(error);
  }
};

export const forgetUser = async () => {
  try {
    await AsyncStorage.removeItem("Email");
    await AsyncStorage.removeItem("Password");
    await AsyncStorage.removeItem("RememberMe");
  } catch (error) {
    console.log(error);
  }
};
