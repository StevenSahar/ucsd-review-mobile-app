import request from 'supertest';
import app from '../src/index'; 
import User from '../src/models/user.model';

describe('User Registration API', () => {
  
  beforeEach(async () => {
    await User.deleteMany();
  });

  it('should register a new user and return status 200', async () => {
    const userData = {
      username: 'John Doe',
      email: 'john.doe+signuptest@example.com',
      password: 'secretpassword',
    };

    const response = await request(app).post('/signup').send(userData);

    expect(response.status).toBe(200);
    expect(response.body).toEqual({ message: "Your account has been successfully created. A verification email was sent to your email. Please check your inbox to verify your account." });

    
    const user = await User.findOne({ email: userData.email });
    expect(user).toBeDefined();
    expect(user?.username).toBe(userData.username);
    
  });

  it('should return status 409 for duplicate email', async () => {
    
    const initialUserData = {
      username: 'Jane Smith',
      email: 'jane.smith@example.com',
      password: 'anothersecretpassword',
    };
    await User.create(initialUserData);

    const userData = {
      name: 'John Doe',
      email: 'jane.smith@example.com', 
      password: 'secretpassword',
    };

    const response = await request(app).post('/signup').send(userData);

    expect(response.status).toBe(409);
    expect(response.body).toEqual({error: "The email provided is already associated with an existing account. Please use a different email address." });
  });
});
