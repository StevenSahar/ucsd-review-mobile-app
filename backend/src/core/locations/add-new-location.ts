import express from "express";
import jwt from "jsonwebtoken";
import Location from "../../models/location.model";
import axios from "axios";

const router = express.Router();

router.post("/", async (req, res) => {
  try {
    const token = req.headers["x-access-token"];
    const decoded = await jwt.verify(token, process.env.JWT_SECRET);

    const { name, tag, description } = req.body;

    //Possible To-Do: Verify submitted location is not a duplicate

    //Geocode API Endpoint
    const GEOCODING_API = `https://geocode.maps.co/search?`;

    //Search API
    const response = await axios.get(GEOCODING_API, {
      params: {
        street: name,
        city: "San Diego",
        county: "San Diego County",
        state: "California",
        country: "United States",
        postalcode: 92093,
      },
    });

    if (response.data.length <= 0) {
      return res.status(404).send({ error: "Location not found." });
    }

    //Extract latitude and longitude
    const { lat, lon } = response.data[0];
    const coordinates = [lat, lon];

    //Add the new unverified location to location model
    const newLocation = await new Location({
      name: name,
      tag: tag,
      coordinates: coordinates,
      numReviews: 0,
      avgRating: 0,
      reviews: [],
      image: "",
      upvotes: 0,
      isVerified: false,
      description: description,
    }).save();

    res.status(200).send({ message: "New location successfully added." });
  } catch (error) {
    res.status(400).send({ error: "Error in adding new location." });
  }
});

export default router;
