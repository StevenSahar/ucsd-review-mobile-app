//Component to display the star breakdown in the reviews-popup.tsx

import React from "react";
import { View, StyleSheet } from "react-native";
import { Text } from "react-native-paper";
import { Colors } from "../utils/theme";
import { ModeLoader } from "../utils/theme/mode-loader";

export const ReviewBreakdownDisplay = ({
  starTally,
  starIndex,
  totalReviews,
}: {
  starTally: any;
  starIndex: number;
  totalReviews: number;
}) => {
  //Load Theme
  const theme = ModeLoader();
  const styles = styling(theme);

  return (
    <View style={styles.breakdownRow}>
      <Text style={styles.breakdownText}>
        {starIndex + 1} star{starIndex + 1 !== 1 ? "s" : ""}
      </Text>
      <View style={styles.breakdownTotal}>
        <View
          style={[
            styles.breakdownPercentage,
            {
              width: `${(starTally * 100) / totalReviews}%`,
            },
          ]}
        ></View>
      </View>
    </View>
  );
};

const styling = (theme) =>
  StyleSheet.create({
    breakdownRow: {
      flexDirection: "row",
      alignItems: "center",
      marginTop: "3%",
      marginBottom: "3%",
    },
    breakdownText: {
      color: Colors[theme]?.secondaryText,
      fontFamily: "Poppins-Regular",
      fontSize: 8,
      fontStyle: "normal",
      fontWeight: "400",
      lineHeight: 10,
      letterSpacing: 0.4,
    },
    breakdownTotal: {
      width: "40%",
      height: 4,
      backgroundColor: Colors[theme]?.starBackgroundColor,
      borderRadius: 2,
      marginLeft: "12%",
    },
    breakdownPercentage: {
      width: "0%",
      height: 4,
      borderRadius: 2,
      backgroundColor: Colors[theme]?.starColor,
    },
  });
