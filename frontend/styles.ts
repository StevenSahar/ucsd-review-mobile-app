import { StyleSheet } from "react-native";
import { Colors } from "./utils/theme";
import { ModeLoader } from "./utils/theme/mode-loader";

const styling = (theme) =>
  StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: Colors[theme]?.primaryBackground,
    },

    box: {
      backgroundColor: Colors[theme]?.secondaryBackground,
      height: 290,
      borderRadius: 10,
      textAlign: "center",
      paddingTop: 120,
    },

    smallerBox: {
      backgroundColor: Colors[theme]?.secondaryBackground,
      height: 150,
      borderRadius: 10,
      marginBottom: -50,
    },

    buttonGroup: {
      top: -200,
      flex: 1,
      flexDirection: "column",
      justifyContent: "center",
      alignItems: "center",
      right: 30,
      left: 50,
      width: 300,
    },

    buttons: {
      overflow: "hidden",
      textAlign: "center",
      flexDirection: "row",
      paddingTop: 20,
      justifyContent: "space-evenly",
      alignItems: "center",
    },

    arrow: {
      marginLeft: 150,
      marginRight: 20,
    },

    pageTitle: {
      color: Colors[theme]?.secondaryText,
      fontFamily: "Poppins-Regular",
      fontSize: 30,
      fontStyle: "normal",
      fontWeight: "400",
      lineHeight: 40,
      letterSpacing: 1.5,
      textAlign: "center",
      flex: 1,
    },
    header: {
      backgroundColor: Colors[theme]?.secondaryBackground,
      borderBottomLeftRadius: 10,
      borderBottomRightRadius: 10,
      height: "13%",
      justifyContent: "center",
      alignItems: "center",
    },
    headerContents: {
      justifyContent: "center",
      alignItems: "center",
      flexDirection: "row",
    },
    headerBackButton: {
      left: 40,
      flex: 0,
      padding: 0,
    },

    buttonText: {
      width: 90,
      marginLeft: 15,
      fontSize: 18,
      fontStyle: "normal",
      fontWeight: "400",
      lineHeight: 24,
      letterSpacing: 0.9,
    },

    icon: {
      display: "flex",
      margin: "auto",
      padding: 1,
      justifyContent: "center",
      alignItems: "center",
    },
  });

export default styling;

export const LightMapStyle = [
  {
    elementType: "labels",
    stylers: [
      {
        visibility: "off",
      },
    ],
  },
  {
    featureType: "administrative.land_parcel",
    stylers: [
      {
        visibility: "off",
      },
    ],
  },
  {
    featureType: "administrative.neighborhood",
    stylers: [
      {
        visibility: "off",
      },
    ],
  },
];

export const DarkMapStyle = [
  {
    elementType: "geometry",
    stylers: [
      {
        color: "#1d2c4d",
      },
    ],
  },
  {
    elementType: "labels",
    stylers: [
      {
        visibility: "off",
      },
    ],
  },
  {
    elementType: "labels.text.fill",
    stylers: [
      {
        color: "#8ec3b9",
      },
    ],
  },
  {
    elementType: "labels.text.stroke",
    stylers: [
      {
        color: "#1a3646",
      },
    ],
  },
  {
    featureType: "administrative.country",
    elementType: "geometry.stroke",
    stylers: [
      {
        color: "#4b6878",
      },
    ],
  },
  {
    featureType: "administrative.land_parcel",
    elementType: "labels.text.fill",
    stylers: [
      {
        color: "#64779e",
      },
    ],
  },
  {
    featureType: "administrative.neighborhood",
    stylers: [
      {
        visibility: "off",
      },
    ],
  },
  {
    featureType: "administrative.province",
    elementType: "geometry.stroke",
    stylers: [
      {
        color: "#4b6878",
      },
    ],
  },
  {
    featureType: "landscape.man_made",
    elementType: "geometry.stroke",
    stylers: [
      {
        color: "#334e87",
      },
    ],
  },
  {
    featureType: "landscape.natural",
    elementType: "geometry",
    stylers: [
      {
        color: "#023e58",
      },
    ],
  },
  {
    featureType: "poi",
    elementType: "geometry",
    stylers: [
      {
        color: "#283d6a",
      },
    ],
  },
  {
    featureType: "poi",
    elementType: "labels.text.fill",
    stylers: [
      {
        color: "#6f9ba5",
      },
    ],
  },
  {
    featureType: "poi",
    elementType: "labels.text.stroke",
    stylers: [
      {
        color: "#1d2c4d",
      },
    ],
  },
  {
    featureType: "poi.park",
    elementType: "geometry.fill",
    stylers: [
      {
        color: "#023e58",
      },
    ],
  },
  {
    featureType: "poi.park",
    elementType: "labels.text.fill",
    stylers: [
      {
        color: "#3C7680",
      },
    ],
  },
  {
    featureType: "road",
    elementType: "geometry",
    stylers: [
      {
        color: "#304a7d",
      },
    ],
  },
  {
    featureType: "road",
    elementType: "labels.text.fill",
    stylers: [
      {
        color: "#98a5be",
      },
    ],
  },
  {
    featureType: "road",
    elementType: "labels.text.stroke",
    stylers: [
      {
        color: "#1d2c4d",
      },
    ],
  },
  {
    featureType: "road.highway",
    elementType: "geometry",
    stylers: [
      {
        color: "#2c6675",
      },
    ],
  },
  {
    featureType: "road.highway",
    elementType: "geometry.stroke",
    stylers: [
      {
        color: "#255763",
      },
    ],
  },
  {
    featureType: "road.highway",
    elementType: "labels.text.fill",
    stylers: [
      {
        color: "#b0d5ce",
      },
    ],
  },
  {
    featureType: "road.highway",
    elementType: "labels.text.stroke",
    stylers: [
      {
        color: "#023e58",
      },
    ],
  },
  {
    featureType: "transit",
    elementType: "labels.text.fill",
    stylers: [
      {
        color: "#98a5be",
      },
    ],
  },
  {
    featureType: "transit",
    elementType: "labels.text.stroke",
    stylers: [
      {
        color: "#1d2c4d",
      },
    ],
  },
  {
    featureType: "transit.line",
    elementType: "geometry.fill",
    stylers: [
      {
        color: "#283d6a",
      },
    ],
  },
  {
    featureType: "transit.station",
    elementType: "geometry",
    stylers: [
      {
        color: "#3a4762",
      },
    ],
  },
  {
    featureType: "water",
    elementType: "geometry",
    stylers: [
      {
        color: "#0e1626",
      },
    ],
  },
  {
    featureType: "water",
    elementType: "labels.text.fill",
    stylers: [
      {
        color: "#4e6d70",
      },
    ],
  },
];
