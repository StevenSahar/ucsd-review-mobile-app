import React from "react";
import {View, Text, StyleSheet} from "react-native";
import { FontAwesome } from "@expo/vector-icons"



export default class Star extends React.Component {
    render() {
        return <FontAwesome name="star"  color="yellow" size={28} style={{ marginHorizontal: 6 }} />;
    }
}