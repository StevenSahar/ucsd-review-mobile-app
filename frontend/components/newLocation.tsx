import React, { useState, useEffect, Fragment } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  ScrollView,
  SafeAreaView,
} from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";
import { useNavigation } from "@react-navigation/native";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { Colors } from "../utils/theme";
import { ModeLoader } from "../utils/theme/mode-loader";
import styling from "../styles";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import { HomePageHeader } from "../components/page-headers";
import ImageView from "react-native-image-viewing";
import ImageFooter from "../components/ImageFooter";




type newLocations = {
    name: { type: String; required: true };
    _id: { type: String; required: true };
    tag: { type: String; required: true };
    coordinates: { type: Array<number>; required: true };
    numReviews: { type: Number };
    avgRating: { type: Number };
    reviews: { type: Array<string> };
    image: { type: String };
    upvotes: { type: Number };
    isVerified: { type: Boolean };
    description: { type: String };
    upvoted: { type: boolean };
  };

export default function NewLocation({ item, onUpvote }, getLocations){
    const theme = ModeLoader();
    const location = locations(theme);
    const styles = styling(theme);

    const [imageUploaded, setImageUploaded] = useState(false);
    const [image, setImage] = useState<{}[]>([]);
    const [visible, setIsVisible] = useState(false);


    const handleInitialImageState = () => {
        console.log(item.image.length)
        setImageUploaded(item.image?.length != 0);
      };


    const lightOrDark = () => {
        return theme == "light";
    };

    const toggleVisible = () => {
        setIsVisible(!visible);
      };
    

    const handleSetImage = () => {
        setImage(
          item.image?.map((photo: any) => {
            return { uri: `http:132.249.242.205:5050/retrieve/${photo}` };
          })
        );
      };
    async function pressUpvote(item: newLocations) {
        try {
          const response = await fetch("http://132.249.242.205:5050/vote", {
            method: "POST",
            headers: {
              "Content-Type": "application/json",
              "x-access-token": await AsyncStorage.getItem("token"),
            },
            body: JSON.stringify({
              locationId: item._id,
            }),
          });
          onUpvote();
        } catch (e) {
          console.error("Error" + e);
        }
        console.log(item.name, item._id, item.upvoted);
      }


      useEffect(() => {
        handleInitialImageState();
        handleSetImage();
      }, []);
    
    return(
        <View key={item._id} style={location.item}>
        <View style={location.row}>
          <View
            style={{
              flexDirection: "row",
              alignItems: "center",
              justifyContent: "space-between",
              flex: 1,
            }}
          >
            <Text style={location.title}>{item.name}</Text>
            <View
              style={{ flexDirection: "row", alignItems: "center" }}
            >
              <Text
                style={{ marginLeft: 4, fontSize: 19, color: "white" }}
              >
                {item.upvotes}
              </Text>
              <TouchableOpacity
                style={{ paddingLeft: 5 }}
                onPress={() => {
                  pressUpvote(item);
                }}
              >
                {lightOrDark() ? (
                  item.upvoted ? (
                    <Image
                      style={{ marginBottom: 1 }}
                      source={require("../assets/redUpvote.png")}
                    />
                  ) : (
                    <Image
                      style={{ marginBottom: 1 }}
                      source={require("../assets/whiteUpvote.png")}
                    />
                  )
                ) : item.upvoted ? (
                  <Image
                    style={{ marginBottom: 1 }}
                    source={require("../assets/greenUpvote.png")}
                  />
                ) : (
                  <Image
                    style={{ marginBottom: 1 }}
                    source={require("../assets/whiteUpvote.png")}
                  />
                )}
              </TouchableOpacity>
            </View>
          </View>
        </View>
        <View style={location.row}>
          <View style={{ flex: 1 }}>
            <Text style={location.tag}>{item.tag}</Text>
          </View>
        </View>
        <View style={location.thirdRow}> 
        <Text style={{ color: "white", alignItems: "center", width:"50%", justifyContent:'center', marginTop:"12%" }}>
          {" "}
          {item.description}
        </Text>
        <TouchableOpacity
          onPress={() => {
            if (imageUploaded) {
              toggleVisible();
            }
          }}
          style={[
            { backgroundColor: "transparent", height:100, width:100, marginLeft:"26%", marginTop:'0%' },
          ]}
        >
          <Image
            source={image[0]}
            style={{ width: 110, height: 110, borderRadius:15 }}
          />
        </TouchableOpacity>
        <ImageView
          images={image}
          imageIndex={0}
          visible={visible}
          onRequestClose={() => setIsVisible(false)}
          animationType="slide"
          presentationStyle="overFullScreen"
          backgroundColor="black"
          swipeToCloseEnabled={true}
          FooterComponent={({ imageIndex }) => (
            <ImageFooter imageIndex={imageIndex} imagesCount={image.length} />
          )}
        />
        </View>
      </View>
    )
}


const locations = (theme) =>
  StyleSheet.create({
    item: {
      marginLeft: 20,
      marginRight: 20,
      flexDirection: "column",
      borderRadius: 20,
      marginTop: 24,
      padding: 30,
      backgroundColor: Colors[theme]?.card,
      fontSize: 24,
    },

    row: {
      flexDirection: "row",
      justifyContent: "space-between",
      alignItems: "center",
      paddingBottom: 8,
      top: -10,
    },
    title: {
      paddingRight: 70,
      color: "white",
      fontSize: 16,
      lineHeight: 16,
      letterSpacing: 1,
    },
    tag: {
      color: "white",
      fontSize: 14,
      lineHeight: 14,
      letterSpacing: 1,
    },

    addButton: {
      borderRadius: 20,
    },

    addButtonContainer: {
      position: "absolute",
      marginLeft: 320,
      zIndex: 1,
      bottom: "15%",
    },

    thirdRow: {
        flexDirection: 'row',    
    }
  });
