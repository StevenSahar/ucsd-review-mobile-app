import { View, Text, StyleSheet, TouchableOpacity } from "react-native";
import Category from "../components/category";
import React, { useState } from "react";
import { Icon } from "@rneui/themed";
import { ModeLoader } from "../utils/theme/mode-loader";
import { Colors } from "../utils/theme";

function Filter({
  checkboxes,
  initialCheckboxes,
  handleSelectAll,
  handleCheckboxClick,
  handleResetAll,
  onSubmit,
}) {
  const initialDarkCheckboxes = [
    { name: "Dining Hall", color: "#4BFF00", checked: false },
    { name: "Dorms", color: "#00FFFF", checked: false },
    { name: "Lecture Hall", color: "#FF006E", checked: false },
    { name: "Study Spot", color: "#F0FF1C", checked: false },
    { name: "Favorites", color: "#FB2B2B", checked: false },
  ];

  const theme = ModeLoader();
  const styles = styling(theme);

  const lightorDark = () => {
    return theme == "light";
  };

  return (
    <View style={styles.main}>
      <View style={styles.header}>
        <Text
          style={{
            flex: 5,
            textAlign: "center",
            alignContent: "center",
            justifyContent: "center",
            fontSize: 20,
            marginTop: 12,
            paddingLeft: 50,
            color: Colors[theme]?.primaryText,
          }}
        >
          {" "}
          Filters{" "}
        </Text>
        <TouchableOpacity style={styles.reset} onPress={handleResetAll}>
          <Text style={styles.resetText}>Reset</Text>
        </TouchableOpacity>
      </View>
      <View style={styles.options}>
        {checkboxes.map((checkbox, index) => (
          <Category
            key={index}
            color={
              lightorDark()
                ? checkbox.color
                : initialDarkCheckboxes[index].color
            }
            name={checkbox.name}
            checked={checkbox.checked}
            onCheckboxClick={() => handleCheckboxClick(index)}
          />
        ))}
      </View>
      <View style={styles.footer}>
        <TouchableOpacity style={styles.selectAll} onPress={handleSelectAll}>
          <Text style={styles.selectText}>Select all</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.showResults} onPress={onSubmit}>
          <Text style={styles.submitText}>Submit</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styling = (theme: string) =>
  StyleSheet.create({
    main: {
      width: "80%",
      height: "50%",
      display: "flex",
      backgroundColor: Colors[theme]?.primaryBackground,
      borderRadius: 25,
      position: "absolute",
      top: "25%", // Adjust this value to center vertically
      left: "10%", // Adjust this value to center horizontally
    },
    header: {
      flex: 1,
      backgroundColor: Colors[theme]?.primaryBackground,
      width: "100%",
      height: "10%",
      display: "flex",
      flexDirection: "row",
      borderRadius: 25,
      marginTop: 3,
    },
    reset: {
      flex: 1,
      alignItems: "center",
      justifyContent: "center",
      paddingRight: 20,
    },
    back: {
      flex: 1,
      alignItems: "flex-start",
      justifyContent: "center",
      paddingLeft: 15,
      paddingTop: 2,
    },
    resetText: {
      fontSize: 16,
      lineHeight: 21,
      letterSpacing: 0.25,
      color: Colors[theme]?.button,
      // marginRight: 5,
      marginTop: 12,
    },
    options: {
      marginTop: 10,
      flex: 8,
    },
    footer: {
      flex: 2,
      justifyContent: "center",
      gap: 10,
      alignItems: "center",
      flexDirection: "row",
    },
    selectAll: {
      alignItems: "center",
      justifyContent: "center",
      paddingVertical: 9,
      paddingHorizontal: 32,
      borderRadius: 4,
      width: "45%",
    },
    showResults: {
      alignItems: "center",
      justifyContent: "center",
      paddingVertical: 9,
      paddingHorizontal: 32,
      borderRadius: 11,
      backgroundColor: Colors[theme]?.button,
      width: "45%",
    },
    selectText: {
      fontSize: 16,
      lineHeight: 21,
      letterSpacing: 0.25,
      color: Colors[theme]?.button,
    },
    submitText: {
      fontSize: 16,
      lineHeight: 21,
      letterSpacing: 0.25,
      color: Colors[theme]?.buttonText,
    },
  });

export default Filter;
