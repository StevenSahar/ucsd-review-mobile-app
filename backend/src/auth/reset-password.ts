import express from "express";
import jwt from "jsonwebtoken";
import bcrypt from "bcrypt";
import User from "../models/user.model";

const router = express.Router();

router.post("/:id/:token", async (req, res) => {
  const { id, token } = req.params;
  const { password } = req.body;

  const user = await User.findById(id);
  if (!user) {
    res.status(400).send({ error: "User not found." });
  }

  const tempSecret = process.env.JWT_SECRET + user.email + user.password;
  try {
    const payload = jwt.verify(token, tempSecret);
    if (payload.id !== id) {
      res.status(400).send({ error: "Invalid token." });
    }

    const salt = await bcrypt.genSalt(Number(process.env.SALT));
    const hashPassword = await bcrypt.hash(req.body.password, salt);
    user.password = hashPassword;
    await user.save();

    res.status(200).send({ message: "Password reset successfully." });
  } catch (err) {
    res.status(400).send({ error: "Invalid token." });
  }
});

export default router;
