import express from "express";
import Location from "../../models/location.model";
import User from "../../models/user.model";
import jwt from "jsonwebtoken";

const router = express.Router();

router.get("/", async (req, res) => {
    try {
        const token = req.headers["x-access-token"];
        const decoded = jwt.verify(token, process.env.JWT_SECRET);
        const user = await User.findById(decoded.id);

        const isVerified = req.query.isVerified === "true" ? true : false;
        let locations : any = await Location.find({ isVerified: isVerified }).lean();

        if (!isVerified) {
            for (const location of locations) {
                location.upvoted = user.upvotedLocations.includes(location._id.toString());
            }
        }

        res.status(200).json(locations);
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
});

export default router;
