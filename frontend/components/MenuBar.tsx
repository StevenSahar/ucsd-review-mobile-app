import {
  StyleSheet,
  Text,
  View,
  Image,
  SafeAreaView,
  TouchableOpacity,
} from "react-native";
import React, { useEffect } from "react";
import { useNavigation } from "@react-navigation/native";
import { useState } from "react";
import { Colors } from "../utils/theme";
import { ModeLoader } from "../utils/theme/mode-loader";
import { FontAwesome5, Ionicons } from "@expo/vector-icons";

//Screens
let newLocationPressed: boolean = false;
let profilePressed: boolean = false;
let homePressed: boolean = false;

export default function MenuBar() {
  const theme = ModeLoader();
  const styles = menu(theme);

  const [newLocationPressed, setNewLocationPressed] = useState(false);
  const [profilePressed, setProfilePressed] = useState(false);
  const [homePressed, setHomePressed] = useState(true);

  const navigation = useNavigation();
  const currentPage =
    navigation.getState().routes[navigation.getState().index].name;

  const goToProfilePage = () => {
    navigation.navigate("ProfilePage");
    setProfilePressed(true);
    setNewLocationPressed(false);
    setHomePressed(false);
  };

  const goToNewLocationPage = () => {
    navigation.navigate("NewLocationsHome");
    setProfilePressed(false);
    setNewLocationPressed(true);
    setHomePressed(false);
  };

  const goToHomePage = () => {
    navigation.navigate("HomePage");
    setProfilePressed(false);
    setNewLocationPressed(false);
    setHomePressed(true);
  };

  const onProfilePage = () => {
    setProfilePressed(true);
    setNewLocationPressed(false);
    setHomePressed(false);
  };

  const onNewLocationPage = () => {
    setProfilePressed(false);
    setNewLocationPressed(true);
    setHomePressed(false);
  };

  const onHomePage = () => {
    setProfilePressed(false);
    setNewLocationPressed(false);
    setHomePressed(true);
  };

  const determineHighlight = () => {
    if (currentPage === "ProfilePage") {
      onProfilePage();
    } else if (currentPage === "NewLocationsHome") {
      onNewLocationPage();
    } else {
      onHomePage();
    }
  };

  useEffect(() => {
    determineHighlight();
  }, [currentPage]);

  return (
    <View style={styles.container}>
      <TouchableOpacity onPress={goToNewLocationPage} style={styles.add}>
        <FontAwesome5
          name="fire"
          size={27}
          color={
            newLocationPressed
              ? theme === "light"
                ? "#FFF3E2"
                : "#181927"
              : theme === "light"
              ? "#262322"
              : "#18192780"
          }
        />
      </TouchableOpacity>

      <TouchableOpacity onPress={goToHomePage} style={styles.home}>
        <FontAwesome5
          name="map-marked-alt"
          size={27}
          color={
            homePressed
              ? theme === "light"
                ? "#FFF3E2"
                : "#181927"
              : theme === "light"
              ? "#262322"
              : "#18192780"
          }
        />
      </TouchableOpacity>

      <TouchableOpacity onPress={goToProfilePage} style={styles.profile}>
        <Ionicons
          name="person"
          size={30}
          color={
            profilePressed
              ? theme === "light"
                ? "#FFF3E2"
                : "#181927"
              : theme === "light"
              ? "#262322"
              : "#18192780"
          }
        />
      </TouchableOpacity>
    </View>
  );
}

const menu = (theme) =>
  StyleSheet.create({
    container: {
      position: "absolute",
      bottom: 30,
      left: 15,
      right: 15,
      height: 70,
      flexDirection: "row",
      justifyContent: "space-around",
      alignItems: "center",
      backgroundColor: Colors[theme]?.button,
      borderRadius: 15,
      paddingVertical: 10,
      // shadowColor: "#262322",
      // shadowOffset: { width: -10, height: 0 },
      // shadowOpacity: 0.3,
      // shadowRadius: 8,
      elevation: 8,
    },

    add: {
      flex: 1,
      alignItems: "center",
      justifyContent: "center",
      height: 70,
    },

    profile: {
      flex: 1,
      alignItems: "center",
      justifyContent: "center",
      height: 70,
    },

    home: {
      flex: 1,
      alignItems: "center",
      justifyContent: "center",
      height: 70,
    },
  });
