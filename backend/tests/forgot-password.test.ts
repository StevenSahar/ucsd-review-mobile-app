import request from 'supertest';
import app from '../src/index';
import User from '../src/models/user.model';
import nodemailer from 'nodemailer';

jest.mock('nodemailer', () => ({
  createTransport: jest.fn().mockReturnValue({
    sendMail: jest.fn().mockImplementation((options, callback) => {
      callback(null, { response: 'Email sent' });
    }),
  }),
}));

describe('Forgot Password API', () => {
  const testUser = {
    username: 'Test User',
    email: 'test@example.com',
    password: 'testpassword',
  };

  beforeAll(async () => {
    await User.create(testUser);
  });

  afterAll(async () => {
    await User.deleteOne({ email: testUser.email });
  });

  it('should send an email with a reset password link for a valid user', async () => {
    const response = await request(app).post('/forgot-password').send({ email: testUser.email });

    expect(response.status).toBe(200);
    expect(response.body).toEqual({ message: "Email sent to user's email." });

    expect(nodemailer.createTransport).toHaveBeenCalledWith({
      service: 'gmail',
      auth: {
        user: process.env.EMAIL,
        pass: process.env.EMAIL_PASSWORD,
      },
    });

    expect(nodemailer.createTransport().sendMail).toHaveBeenCalled();
  });

  it('should return status 400 if the user email is invalid', async () => {
    const response = await request(app).post('/forgot-password').send({ "email": 'invalid@example.com' });

    expect(response.status).toBe(400);
    expect(response.body).toEqual({ error: 'User not found.' });
  });
});