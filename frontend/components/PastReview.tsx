import React, { useState, useEffect } from "react";
import { StyleSheet, Text, View, Image, ScrollView } from "react-native";
import styles from "../styles";
import { TouchableOpacity } from "react-native-gesture-handler";
// import { Rating } from "react-native-stock-star-rating"; //$ npm install react-native-stock-star-rating
import { Colors } from "../utils/theme";
import { ModeLoader } from "../utils/theme/mode-loader";
import { Rating } from "react-native-rating-element";
import { calculateTime } from "../utils/calculate-time";
import ImageView from "react-native-image-viewing";
import ImageFooter from "../components/ImageFooter";

export default function PastReview({ item }: { item: any }) {
  //Load theme
  const theme = ModeLoader();
  const review = styling(theme);

  const [image, setImage] = useState<{}[]>([]);
  const [imageUploaded, setImageUploaded] = useState(false);

  const handleInitialImageState = () => {
    setImageUploaded(item.photos.length != 0);
  };

  const handleSetImage = () => {
    setImage(
      item.photos.map((photo: any) => {
        return { uri: `http:132.249.242.205:5050/retrieve/${photo}` };
      })
    );
  };

  const [visible, setIsVisible] = useState(false);

  const toggleVisible = () => {
    setIsVisible(!visible);
  };

  useEffect(() => {
    handleInitialImageState();
    handleSetImage();
  }, []);

  return (
    <View key={item._id} style={review.item}>
      <View style={review.row1}>
        <View style={{ flex: 1 }}>
          <Text style={review.title}>{item.heading}</Text>
        </View>
        <Text style={review.date}>{calculateTime(new Date(item.date))}</Text>
      </View>
      <View style={review.row1}>
        <View style={{ flex: 1 }}>
          <Rating
            rated={item.stars}
            totalCount={7}
            size={14}
            readonly={true}
            ratingColor={Colors[theme]?.starColor}
            ratingBackgroundColor={Colors[theme]?.starBackgroundColor}
            icon="ios-star"
          />
        </View>
        <Text style={review.date}>{item.username}</Text>
      </View>
      <View style={review.row1}>
        <View style={{ flex: 1 }}>
          <Text style={review.description}>{item.comment}</Text>
        </View>
        <TouchableOpacity
          onPress={() => {
            if (imageUploaded) {
              toggleVisible();
            }
          }}
          style={[
            { backgroundColor: "transparent" },
            imageUploaded && { backgroundColor: "black" },
          ]}
        >
          {image.length > 0 && (
            <Image
              source={image[0]}
              style={{ width: 50, height: 50 }}
              resizeMode="contain"
            />
          )}
        </TouchableOpacity>
        {image.length > 0 && (
          <ImageView
            images={image}
            imageIndex={0}
            visible={visible}
            onRequestClose={() => setIsVisible(false)}
            animationType="slide"
            presentationStyle="overFullScreen"
            backgroundColor="black"
            swipeToCloseEnabled={true}
            FooterComponent={({ imageIndex }) => (
              <ImageFooter imageIndex={imageIndex} imagesCount={image.length} />
            )}
          />
        )}
      </View>
    </View>
  );
}

const styling = (theme) =>
  StyleSheet.create({
    item: {
      flexDirection: "column",
      alignItems: "flex-start",
      marginLeft: 40,
      marginRight: 40,
      borderRadius: 20,
      marginTop: 24,
      padding: 30,
      backgroundColor: Colors[theme]?.card,
      fontSize: 24,
    },
    row1: {
      flexDirection: "row",
      justifyContent: "space-between",
      alignItems: "center",
      paddingBottom: 8,
      top: -10,
    },
    title: {
      color: "white",
      fontSize: 16,
      fontStyle: "normal",
      fontWeight: "400",
      lineHeight: 16,
      letterSpacing: 0.8,
    },
    date: {
      color: "white",
      fontSize: 10,
      fontStyle: "normal",
      fontWeight: "400",
      lineHeight: 16,
      letterSpacing: 0.8,
    },

    description: {
      color: "white",
      fontSize: 10,
      fontStyle: "normal",
      fontWeight: "normal",
      letterSpacing: 0.5,
      marginRight: 10,
    },
  });
