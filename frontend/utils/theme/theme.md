## Theme (Dark & Light Mode)

This guide offers instructions on how to access colors for both dark and light mode.

### Module Imports

- `import { Colors } from "../utils/theme";`
- `import { ModeLoader } from "../utils/theme/mode-loader";`

### Put Inside Main Function

`ModeLoader()` gets the current state of the mode (either dark or light)
`const styles = styling(theme);` is not required if `StyleSheet` is not being used\*

- `const theme = ModeLoader();`
- `const styles = styling(theme);`

### Defining StyleSheet

Instead of defining `const styles = StyleSheet.create({...})`,
we use the code above to pass in the current theme as argument

- `const styling = (theme) => StyleSheet.create({...})`
