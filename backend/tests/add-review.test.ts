import request from 'supertest';
import jwt from 'jsonwebtoken';
import bcrypt from 'bcrypt';
import app from '../src/index'; 
import User from '../src/models/user.model'; 
import Location from '../src/models/location.model'; 
import Review from '../src/models/review.model'; 


const createTestUser = async () => {
    const salt = await bcrypt.genSalt(Number(process.env.SALT));
    const hashPassword = await bcrypt.hash('secretpassword', salt);

    return User.create({
        username: 'Test User',
        email: `testuser_${Date.now()}@example.com`,
        password: hashPassword,
    });
};


const generateAuthToken = (user) => {
  return jwt.sign(
    {
      name: user.name,
      email: user.email,
      id: user._id
    },
    process.env.JWT_SECRET
  );
};


const createTestLocation = async () => {
  return Location.create({
    name: 'Test Location',
    tag: 'test',
    coordinates: [0, 0],
    reviews : [[], [], [], [], [], [], []],
    avgRating: 0,
    numReviews: 0,
  });
};

describe('Add Review API', () => {
  let testUser;
  let authToken;
  let testLocation;

  beforeAll(async() => {
    testUser = await createTestUser();
    authToken = generateAuthToken(testUser);
    testLocation = await createTestLocation();
  });

  afterAll(async () => {
    await User.deleteOne({ _id: testUser._id });
    await Location.deleteOne({ _id: testLocation._id });
  });

  it('should add a review for a valid location and user', async () => {
    const reviewData = {
      locationId: testLocation._id,
      comment: 'Great location!',
      stars: 5,
      date: new Date(),
      heading: 'Excellent'
    };

    const response = await request(app)
      .post('/add-review')
      .set('x-access-token', authToken)
      .send(reviewData)
      .expect(200);

    expect(response.body.message).toBe('Review added successfully');

    const updatedUser = await User.findById(testUser._id);
    expect(updatedUser.ownReviews).toHaveLength(1);

    const updatedLocation = await Location.findById(testLocation._id);

    expect(updatedLocation.reviews[4]).toBe(1);
    expect(updatedLocation.avgRating).toBe(5);
    expect(updatedLocation.numReviews).toBe(1);

    // const newReview = await Review.findById(updatedUser.ownReviews[0].toString());

    // expect(newReview.locationId).toBe(testLocation._id.toString());
    // expect(newReview.userId).toBe(testUser._id.toString());
    // expect(newReview.comment).toBe(reviewData.comment);
    // expect(newReview.stars).toBe(reviewData.stars);
    // expect(newReview.heading).toBe(reviewData.heading);
  });

  it('should return 400 for invalid review data', async () => {
    const invalidReviewData = {
        locationId: testLocation._id,
        comment: 'Great location!',
    };

    const response = await request(app)
      .post('/add-review')
      .set('x-access-token', authToken)
      .send(invalidReviewData)
      .expect(400);

    expect(response.body.message).toBeTruthy();
  });

  it('should return 400 for invalid locationId', async () => {
    const reviewDataWithInvalidLocation = {
      locationId: 'invalid-location-id',
      comment: 'Great location!',
      stars: 5,
      date: new Date(),
      heading: 'Excellent',
    };

    const response = await request(app)
      .post('/add-review')
      .set('x-access-token', authToken)
      .send(reviewDataWithInvalidLocation)
      .expect(400);

    expect(response.body.message).toBeTruthy();
  });

  it('should return 400 for invalid x-access-token', async () => {
    const reviewData = {
      locationId: testLocation._id,
      comment: 'Great location!',
      stars: 5,
      date: new Date(),
      heading: 'Excellent',
    };

    const response = await request(app)
      .post('/add-review')
      .set('x-access-token', 'invalid-token')
      .send(reviewData)
      .expect(400);

    expect(response.body.message).toBeTruthy();
  });
});
