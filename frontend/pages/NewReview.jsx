import React, { useState, Fragment } from "react";
import {
  SafeAreaView,
  View,
  StyleSheet,
  ScrollView,
  Image,
} from "react-native";
import { Text, TextInput } from "react-native-paper";
import { Button, AirbnbRating } from "@rneui/themed";
import Modal from "react-native-modal";
import { Feather, Entypo } from "@expo/vector-icons";
import { MainPageHeader } from "../components/page-headers";
import { Colors } from "../utils/theme";
import { ModeLoader } from "../utils/theme/mode-loader";
import * as ImagePicker from "expo-image-picker";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { useRoute, useNavigation } from "@react-navigation/native";
import axios from "axios";
import ImageModal from "react-native-image-modal";
import { Ionicons } from "@expo/vector-icons";

const NewReview = () => {
  //Load theme
  const theme = ModeLoader();
  const styles = styling(theme);

  const route = useRoute();
  const navigation = useNavigation();

  const locationId = route.params.locationId;

  //Navigation
  const HomePage = () => navigation.navigate("HomePage");

  //State declaration
  const [reviewTitle, setReviewTitle] = useState("");
  const [rating, setRating] = useState(0);
  const [reviewDescription, setReviewDescription] = useState("");
  const [visible, setVisible] = useState(false);
  const [selectedImages, setSelectedImages] = useState([]);
  const [titleError, setTitleError] = useState(false);
  const [ratingError, setRatingError] = useState(false);
  const [descriptionError, setDescriptionError] = useState(false);
  const [message, setMessage] = useState("");

  let tempImages = [];

  //API
  const submitNewReview = async () => {
    const formData = new FormData();

    //Insert the image information into form data
    selectedImages.forEach((image) => {
      formData.append("images", {
        uri: image.uri,
        type: "image/jpeg", // You can customize the type as needed
        name: "image.jpg", // You can customize the name as needed
      });
    });

    try {
      //Define route
      const server = "132.249.242.205";
      const port = "5050";
      const reviewRoute = `http://${server}:${port}/add-review`;
      const imgRoute = `http://${server}:${port}/upload`;

      //Post image to database
      let imgResponse = { data: [] };
      if (selectedImages.length > 0) {
        imgResponse = await axios.post(imgRoute, formData);
      }

      //Post review to database
      const response = await fetch(reviewRoute, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          "x-access-token": await AsyncStorage.getItem("token"),
        },
        body: JSON.stringify({
          locationId: locationId,
          heading: reviewTitle,
          stars: rating,
          comment: reviewDescription,
          date: Date(),
          photos: imgResponse.data,
        }),
      });

      const data = await response.json();
      if (data.message === "Review added successfully") {
        HomePage();
      }
    } catch (error) {
      console.error("Error: " + error);
    }
  };

  //Open user's image library
  const pickImage = async () => {
    //Request user permission
    const { status } = await ImagePicker.requestMediaLibraryPermissionsAsync();

    if (status !== "granted") {
      alert("Permission to access media library was denied");
      return;
    }

    //Launch image library
    const result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Images,
      allowsMultipleSelection: true,
      orderedSelection: true,
      quality: 1,
      selectionLimit: 7 - selectedImages.length,
    });

    if (!result.canceled) {
      toggleOverlay();
      tempImages = [...selectedImages, ...result.assets];
      setSelectedImages(tempImages);
      setTitleError(false);
      setRatingError(false);
      setDescriptionError(false);
      setMessage("");
    }
  };

  //Open camera
  const openCamera = async () => {
    try {
      const result = await ImagePicker.launchCameraAsync({
        mediaTypes: ImagePicker.MediaTypeOptions.Images,
        quality: 1,
        allowsEditing: true,
        aspect: [4, 3],
      });

      if (!result.canceled) {
        toggleOverlay();
        tempImages = [...selectedImages, ...result.assets];
        setSelectedImages(tempImages);
        setTitleError(false);
        setRatingError(false);
        setDescriptionError(false);
        setMessage("");
      }
    } catch (error) {
      console.log("Error accessing the camera:", error);
    }
  };

  //Open camera permission
  const takePicture = async () => {
    const { status } = await ImagePicker.requestCameraPermissionsAsync();
    if (status === "granted") {
      openCamera();
    }
  };

  //Toggle Camera/Photos option
  const toggleOverlay = () => {
    setVisible(!visible);
  };

  //Error validation
  const formValidator = () => {
    setTitleError(false);
    setRatingError(false);
    setDescriptionError(false);
    setMessage("");

    //Ensure title is not empty
    if (!reviewTitle) {
      setTitleError(true);
      setMessage("*Please provide a title");
      return false;
    }

    //Ensure rating is not empty
    if (!rating) {
      setRatingError(true);
      setMessage("*Please rate by clicking the stars.");
      return false;
    }

    //Ensure description is not empty
    if (!reviewDescription) {
      setDescriptionError(true);
      setMessage("*Please provide a description");
      return false;
    }

    return true;
  };

  //Remove image from upload
  const removeImage = (index) => {
    let tempArr = [...selectedImages];
    tempArr.splice(index, 1);
    setSelectedImages(tempArr);
  };

  return (
    <Fragment>
      <SafeAreaView style={styles.topSafeAreaView} />
      <SafeAreaView style={styles.safeContainer}>
        <ScrollView
          contentContainerStyle={{ flexGrow: 1 }}
          keyboardShouldPersistTaps="handled"
          scrollEnabled={false}
        >
          <MainPageHeader title="Add New Review" />
          <View style={styles.container}>
            <View
              style={[
                styles.inputContainer,
                { marginTop: 35 },
                titleError && styles.inputContainerError,
              ]}
            >
              <TextInput
                style={[styles.input, { justifyContent: "center", height: 20 }]}
                value={reviewTitle}
                onChangeText={setReviewTitle}
                placeholder="review title"
                placeholderTextColor={Colors[theme]?.placeholderText}
                textColor={Colors[theme]?.primaryText}
                contentStyle={{ fontFamily: "Poppins-Regular" }}
                selectionColor={Colors[theme]?.placeholderText}
                underlineColor="transparent"
                activeUnderlineColor="transparent"
                textContentType="none"
                keyboardType="default"
                maxLength={60}
              />
            </View>
            <AirbnbRating
              onFinishRating={(number) => {
                setRating(number);
              }}
              defaultRating={0}
              count={7}
              size={20}
              showRating={false}
              selectedColor={Colors[theme]?.starColor}
              starContainerStyle={styles.starRating}
              ratingContainerStyle={styles.ratingContainer}
            />
            <View
              style={[
                styles.inputContainer,
                styles.inputContainerDescription,
                descriptionError && styles.inputContainerError,
              ]}
            >
              <TextInput
                multiline={true}
                scrollEnabled={true}
                style={[styles.input, styles.inputDescription]}
                value={reviewDescription}
                onChangeText={setReviewDescription}
                placeholder="description"
                placeholderTextColor={Colors[theme]?.placeholderText}
                textColor={Colors[theme]?.primaryText}
                contentStyle={{ fontFamily: "Poppins-Regular" }}
                selectionColor={Colors[theme]?.placeholderText}
                underlineColor="transparent"
                activeUnderlineColor="transparent"
                textContentType="none"
                keyboardType="default"
              />
            </View>
            <View style={styles.buttonContainer}>
              <ScrollView
                horizontal
                style={{ height: 142, minWidth: "100%" }}
                contentContainerStyle={{ alignItems: "center" }}
                scrollEnabled={selectedImages.length !== 0}
                showsHorizontalScrollIndicator={false}
              >
                {selectedImages.map((img, index) => {
                  return (
                    <View key={img.uri}>
                      <ImageModal
                        source={{ uri: img.uri }}
                        style={{
                          width: (img.width / img.height) * 138,
                          height: 138,
                          backgroundColor: "transparent",
                          marginRight: 10,
                          borderRadius: 5,
                          marginTop: 1,
                          flex: 1,
                        }}
                        resizeMode="contain"
                      />
                      <Ionicons
                        name="ios-close-circle-sharp"
                        size={24}
                        color="rgba(255,255,255, 0.7)"
                        style={{
                          position: "absolute",
                          top: 2,
                          left: 2,
                        }}
                        onPress={() => {
                          removeImage(index);
                        }}
                      />
                    </View>
                  );
                })}
                {selectedImages.length < 7 && (
                  <Button
                    onPress={toggleOverlay}
                    icon={
                      <Feather
                        name="camera"
                        size={45}
                        color={Colors[theme]?.button}
                      />
                    }
                    containerStyle={styles.imageUploadContainer}
                    buttonStyle={styles.imageUploadButton}
                    activeOpacity={0.2}
                  />
                )}
              </ScrollView>
              <Text style={styles.imageCounter}>
                Photos: {selectedImages.length}/7
              </Text>
              <Modal
                isVisible={visible}
                onBackdropPress={toggleOverlay}
                onBackButtonPress={toggleOverlay}
                style={styles.modal}
                animationIn="fadeIn"
                animationOut="fadeOut"
                animationInTiming={300}
                animationOutTiming={100}
                avoidKeyboard={false}
                backdropTransitionInTiming={0}
                backdropTransitionOutTiming={0}
                hideModalContentWhileAnimating={true}
              >
                <View style={styles.modalContent}>
                  <Button
                    onPress={takePicture}
                    title="Camera"
                    buttonStyle={[
                      styles.modalButton,
                      {
                        borderTopLeftRadius: 20,
                        borderTopRightRadius: 20,
                        borderBottomLeftRadius: 0,
                        borderBottomRightRadius: 0,
                        borderBottomWidth: 0.3,
                        borderColor: Colors[theme]?.border,
                      },
                    ]}
                    titleStyle={styles.modalButtonText}
                    containerStyle={styles.modalButtonContainer}
                    activeOpacity={0.8}
                  />
                  <Button
                    onPress={pickImage}
                    title="Photo Library"
                    buttonStyle={[
                      styles.modalButton,
                      {
                        borderBottomLeftRadius: 20,
                        borderBottomRightRadius: 20,
                        borderTopLeftRadius: 0,
                        borderTopRightRadius: 0,
                      },
                    ]}
                    titleStyle={styles.modalButtonText}
                    containerStyle={[
                      styles.modalButtonContainer,
                      { marginBottom: 10 },
                    ]}
                    activeOpacity={0.8}
                  />
                  <Button
                    onPress={toggleOverlay}
                    title="Cancel"
                    buttonStyle={[styles.modalButton, { borderRadius: 20 }]}
                    titleStyle={[
                      styles.modalButtonText,
                      { fontFamily: "Poppins-SemiBold" },
                    ]}
                    containerStyle={styles.modalButtonContainer}
                    activeOpacity={0.8}
                  />
                </View>
              </Modal>
            </View>
            <View style={styles.messageContainer}>
              <Text
                style={[
                  styles.message,
                  (titleError || ratingError || descriptionError) &&
                    styles.messageError,
                ]}
              >
                {message}
              </Text>
            </View>
            <Button
              onPress={() => {
                if (formValidator()) {
                  submitNewReview();
                }
              }}
              title="Submit"
              icon={
                <Entypo
                  name="chevron-right"
                  size={25}
                  color={Colors[theme]?.mainButtonText}
                />
              }
              iconRight
              buttonStyle={styles.submitButton}
              titleStyle={styles.submitButtonText}
            />
          </View>
        </ScrollView>
      </SafeAreaView>
    </Fragment>
  );
};

export default NewReview;

const styling = (theme) =>
  StyleSheet.create({
    topSafeAreaView: {
      flex: 0,
      backgroundColor: Colors[theme]?.secondaryBackground,
    },
    safeContainer: {
      flex: 1,
      backgroundColor: Colors[theme]?.primaryBackground,
      paddingBottom: 5,
    },
    container: {
      alignItems: "center",
      justifyContent: "flex-start",
    },
    inputContainer: {
      width: 350,
      height: 50,
      alignItems: "center",
      justifyContent: "center",
      borderTopLeftRadius: 23,
      borderBottomLeftRadius: 23,
      borderBottomRightRadius: 23,
      borderTopRightRadius: 23,
      borderWidth: 0.5,
      borderColor: Colors[theme]?.border,
      backgroundColor: Colors[theme]?.inputContainer,
      opacity: 1,
      margin: 10,
    },
    input: {
      width: "100%",
      flex: 1,
      borderTopLeftRadius: 23,
      borderBottomLeftRadius: 23,
      borderBottomRightRadius: 23,
      borderTopRightRadius: 23,
      backgroundColor: "transparent",
      color: Colors[theme]?.placeholderText,
      fontFamily: "Poppins-Regular",
      fontSize: 12,
      fontStyle: "normal",
      fontWeight: "400",
      letterSpacing: 0.6,
      paddingLeft: 5,
      paddingRight: 5,
    },
    inputContainerDescription: {
      height: 210,
    },
    inputDescription: {
      textAlignVertical: "top",
    },
    ratingContainer: {
      width: 350,
      alignItems: "flex-start",
      margin: 10,
    },
    starRating: {
      backgroundColor: "transparent",
    },
    buttonContainer: {
      width: 350,
      alignItems: "flex-start",
      marginTop: 10,
      marginBottom: 50,
    },
    imageUploadContainer: {
      alignItems: "center",
      justifyContent: "center",
      borderTopLeftRadius: 23,
      borderBottomLeftRadius: 23,
      borderBottomRightRadius: 23,
      borderTopRightRadius: 23,
      borderWidth: 0.5,
      borderColor: Colors[theme]?.border,
      backgroundColor: Colors[theme]?.inputContainer,
    },
    imageUploadButton: {
      width: 150,
      height: 140,
      backgroundColor: Colors[theme]?.inputContainer,
    },
    imageCounter: {
      marginTop: 10,
      fontFamily: "Poppins-Regular",
      fontStyle: "normal",
      fontWeight: "400",
      letterSpacing: 0.2,
      fontSize: 12,
      color: Colors[theme]?.primaryText,
    },
    submitButton: {
      width: 340,
      height: 50,
      alignItems: "center",
      justifyContent: "flex-end",
      borderRadius: 23,
      backgroundColor: Colors[theme]?.mainButton,
    },
    submitButtonText: {
      color: Colors[theme]?.mainButtonText,
      fontFamily: "Poppins-Regular",
      fontSize: 12,
      fontStyle: "normal",
      fontWeight: "400",
      letterSpacing: 0.6,
      textAlign: "left",
      paddingLeft: 14,
      marginRight: 220,
    },
    modal: {
      justifyContent: "flex-end",
      alignItems: "center",
      marginBottom: 20,
    },
    modalContent: {
      width: "100%",
      backgroundColor: "transparent",
    },
    modalButtonContainer: {
      width: "100%",
    },
    modalButton: {
      height: 65,
      alignItems: "center",
      justifyContent: "center",
      backgroundColor: Colors[theme]?.inputContainer,
    },
    modalButtonText: {
      color: "rgb(10, 132, 255)",
      fontFamily: "Poppins-Medium",
      fontSize: 18,
      fontStyle: "normal",
      fontWeight: "400",
      letterSpacing: 0.6,
    },
    inputContainerError: {
      backgroundColor: "rgba(231, 70, 70, 0.24)",
      borderWidth: 1,
      borderColor: "#E74646",
    },
    messageContainer: {
      width: 300,
      height: 25,
      justifyContent: "center",
      backgroundColor: "transparent",
      marginBottom: 20,
    },
    message: {
      width: "100%",
      color: Colors[theme]?.primaryText,
      fontFamily: "Poppins-Regular",
      fontSize: 13,
      fontStyle: "normal",
      fontWeight: "400",
      letterSpacing: 0.6,
      textAlign: "left",
      padding: 5,
    },
    messageError: {
      color: "#E74646",
    },
  });
