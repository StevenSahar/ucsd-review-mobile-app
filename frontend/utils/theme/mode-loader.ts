import { useCallback, useEffect, useState } from "react";
import { get, save } from "./storage";

export const ModeLoader = () => {
  const [themeValue, setThemeValue] = useState("");

  const themeOperations = (theme: string) => {
    switch (theme) {
      case "dark":
        setThemeValue(theme);
        return;
      case "light":
        setThemeValue(theme);
        return;
    }
  };

  const getAppTheme = useCallback(async () => {
    const theme = await get("Theme");
    themeOperations(theme);
  }, []);

  const setTheme = useCallback(async (theme: string) => {
    save("Theme", theme);
    setThemeValue(theme);
  }, []);

  useEffect(() => {
    getAppTheme();
  }, [getAppTheme]);

  return themeValue;
};
