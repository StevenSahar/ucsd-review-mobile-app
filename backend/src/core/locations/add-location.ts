import express from "express";
import Location from "../../models/location.model";
import jwt from "jsonwebtoken";

const router = express.Router();

router.post("/", async (req, res) => {
    try {
        const token = req.headers["x-access-token"];
        const decoded = jwt.verify(token, process.env.JWT_SECRET);
        
        const location = new Location({
            name: req.body.name,
            tag: req.body.tag,
            coordinates: req.body.coordinates,
            numReviews: 0,
            avgRating: 0,
            image: req.body.images ? req.body.images : "",
            upvotes: 0,
            reviews: [0,0,0,0,0,0,0],
            description: req.body.description,
            isVerified: false
        });
        await location.save();
        res.status(200).send({ message: "Location added successfully"});
    } catch (error) {
        res.status(400).send({ message: error.message });
    }
});

export default router;