import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  Image,
  Dimensions,
} from "react-native";
import { Icon } from "@rneui/themed";
import React, { useState, useEffect, useRef, useCallback } from "react";
import { RootStackParamList } from "../type";
import { StackNavigationProp } from "@react-navigation/stack";
// import { createNativeStackNavigator } from "@react-navigation/native-stack";
import MapView, { Callout, Marker, PROVIDER_GOOGLE } from "react-native-maps";
import AsyncStorage from "@react-native-async-storage/async-storage";
import ReviewsPopup from "./ReviewPopup";
import BottomSheet from "@gorhom/bottom-sheet";
import Filter from "../components/Filter";
import AINavigationButton from "../components/AINavigationButton";
import { LightMapStyle, DarkMapStyle } from "../styles";
import { useNavigation } from "@react-navigation/native";
import { ModeLoader } from "../utils/theme/mode-loader";
import { Colors } from "../utils/theme";
import { useRoute } from "@react-navigation/native";

const LATITUDE = 32.880201;
const LONGITUDE = -117.235796;
const LATITUDE_DELTA = 0.017;
const LONGITUDE_DELTA = 0.017;

const UCSDRegion = {
  latitude: LATITUDE,
  longitude: LONGITUDE,
  latitudeDelta: LATITUDE_DELTA,
  longitudeDelta: LONGITUDE_DELTA,
};

type HomePageProps = {
  navigation: StackNavigationProp<RootStackParamList, "Home">;
};

type Location = {
  _id: string;
  name: { type: String; required: true };
  tag: { type: String; required: true };
  coordinates: { type: Array<number>; required: true };
  numReviews: { type: Number };
  avgRating: { type: Number };
  reviews: { type: Array<string> };
  image: { type: String };
  upvotes: { type: Number };
  isVerified: { type: Boolean };
};

export default function HomePage({ route }: any) {
  const theme = ModeLoader();
  const styles = styling(theme);
  const navigation = useNavigation();

  const lightOrDark = () => {
    return theme == "light";
  };

  const initialCheckboxes = [
    { name: "Dining Hall", color: "#568259", checked: false },
    { name: "Dorms", color: "#2C4251", checked: false },
    { name: "Lecture Hall", color: "#B26E63", checked: false },
    { name: "Study Spot", color: "#E9B44C", checked: false },
    { name: "Favorites", color: "#FB2B2B", checked: false },
  ];

  const [verifiedLocations, setVerifiedLocations] = useState<Location[]>([]);
  const [unverifiedLocations, setUnverifiedLocations] = useState<Location[]>(
    []
  );
  const [isFilterVisible, setIsFilterVisible] = useState(false);
  const [searchQuery, setSearchQuery] = useState("");
  const [searchResults, setSearchResults] = useState([]);
  const [isDropdownVisible, setDropdownVisible] = useState(false);
  const [checkboxes, setCheckboxes] = useState(initialCheckboxes);
  const [filteredVerifiedLocations, setFilteredVerifiedLocations] = useState<
    Location[]
  >([]);
  const [fromFavorites, setFromFavorites] = useState(Boolean);

  const filterLocations = () => {
    const selectedCategories = checkboxes
      .filter((chk) => chk.checked)
      .map((chk) => chk.name);
    console.log("Selected categories:", selectedCategories);
    const filteredLocations = verifiedLocations.filter((location) => {
      return selectedCategories.includes(location.tag);
    });

    setFilteredVerifiedLocations(filteredLocations); // Set the filtered locations
    setIsFilterVisible(false);
  };

  const handleSelectAll = () => {
    const allChecked = checkboxes.map((checkbox) => ({
      ...checkbox,
      checked: true,
    }));
    setCheckboxes(allChecked);
  };

  const handleCheckboxClick = (index: number) => {
    setCheckboxes((prevCheckboxes) => {
      const newCheckboxes = [...prevCheckboxes];
      newCheckboxes[index].checked = !newCheckboxes[index].checked;
      return newCheckboxes;
    });
  };

  const handleResetAll = () => {
    setCheckboxes(initialCheckboxes);
  };

  // search query not logging the data
  async function handleSearch() {
    try {
      const response = await fetch(
        "http://132.249.242.205:5050/?" + // Assuming local server, update the URL if needed
          new URLSearchParams({
            name: searchQuery, // Sending the name parameter as a query string
          }),
        {
          method: "GET",
          headers: {
            "Content-Type": "application/json",
            "x-access-token": await AsyncStorage.getItem("token"),
          },
        }
      );
      const data = await response.json();
      setSearchResults(data);
      setDropdownVisible(true);
    } catch (e) {
      console.error("Error fetching search results: ", e);
      setSearchResults([]);
    }
  }

  const handleSelect = (result) => {
    console.log("Selected:", result);
    togglePopup(result._id, result.coordinates);
    setDropdownVisible(false); // Hide the dropdown after selection
  };

  function getIconColor(categoryName) {
    const categories = {
      "Dining Hall": theme === "light" ? "#568259" : "#FF006E",
      Dorms: theme === "light" ? "#2C4251" : "#0FF",
      "Lecture Hall": theme === "light" ? "#B26E63" : "#F0F",
      "Study Spot": theme === "light" ? "#E9B44C" : "#1EFFBC",
      Favorites: "#FB2B2B",
    };

    return categories[categoryName];
  }

  const SearchResultDropdown = ({ results, onSelect }) => {
    if (results.length === 0) {
      return (
        <View style={styles.resultsContainer}>
          <Text style={styles.noResultsText}>No results found</Text>
        </View>
      );
    }

    return (
      <View style={styles.resultsContainer}>
        {results.map((result) => (
          <TouchableOpacity
            key={result._id}
            onPress={() => onSelect(result)}
            style={styles.resultItem}
          >
            {/* <Icon
              color={getIconColor(result.tag)}
              name="map-pin"
              size={16}
              containerStyle={{ width: 40 }}
              type="font-awesome"
            /> */}
            <View style={{ width: 22, height: 22, marginRight: 10 }}>
              <Image
                source={getMarker(result.tag)}
                style={{ width: "100%", height: "100%", resizeMode: "contain" }}
              />
            </View>
            <Text style={styles.resultText}>{result.name}</Text>
          </TouchableOpacity>
        ))}
      </View>
    );
  };

  async function getLocations() {
    try {
      const response = await fetch(
        "http://132.249.242.205:5050/locations?" +
          new URLSearchParams({
            isVerified: "true",
          }),
        {
          method: "GET",
          headers: {
            "Content-Type": "application/json",
            "x-access-token": await AsyncStorage.getItem("token"),
          },
        }
      );
      const data = await response.json();
      setVerifiedLocations(data);
      setFilteredVerifiedLocations(data);
    } catch (e) {
      console.error("Error fetching locations: ", e);
    }
  }

  useEffect(() => {
    getLocations();
  }, []);

  const getMarker = (tag: string) => {
    if (theme === "light") {
      if (tag === "Dining Hall") {
        return require("../assets/greenPin.png");
      } else if (tag === "Lecture Hall") {
        return require("../assets/redPin.png");
      } else if (tag === "Dorms") {
        return require("../assets/bluePin.png");
      } else if (tag === "Study Spot") {
        return require("../assets/yellowPin.png");
      }
    } else {
      if (tag === "Dining Hall") {
        return require("../assets/newNeonGreen.png");
      } else if (tag === "Lecture Hall") {
        return require("../assets/newNeonRed.png");
      } else if (tag === "Dorms") {
        return require("../assets/newNeonBlue.png");
      } else if (tag === "Study Spot") {
        return require("../assets/newNeonYellow.png");
      }
    }
  };

  //Review Popup Slider
  const sheetRef = useRef<BottomSheet>(null);

  // MapView component
  const mapRef = useRef<MapView>();

  // Search bar text input
  const searchRef = useRef<TextInput>();

  //Close the Popup Slider
  const handleClosePress = useCallback(() => {
    setIsFilterVisible(false);
    sheetRef.current?.close();
    searchRef.current?.blur();
  }, []);

  //Open Popup Slider
  const handleSnapPress = useCallback((index) => {
    sheetRef.current?.snapToIndex(index);
  }, []);

  const [locationId, setLocationId] = useState("");

  const handleSheetChange = useCallback((index: number) => {
    console.log("handleSheetChange", index);
    if (index === -1) {
      setLocationId("");
    }
  }, []);

  const togglePopup = async (
    _id: string,
    coordinates: { type: Array<number>; required: true }
  ) => {
    await handleClosePress();
    await setLocationId(_id);
    await handleSnapPress(0);
    console.log(_id);

    mapRef.current?.animateToRegion(
      {
        latitude: coordinates[0] - 0.005,
        longitude: coordinates[1],
        latitudeDelta: 0.01,
        longitudeDelta: 0.01,
      },
      350
    );
  };

  const toggleFavPopup = async (
    _id: string,
    coordinates: { type: Array<number>; required: true }
  ) => {
    route.params.favID = undefined;
    route.params.favID = undefined;
    await setLocationId(_id);
    await handleSnapPress(0);
    console.log(coordinates);
    mapRef.current?.animateToRegion(
      {
        latitude: coordinates[0] - 0.005,
        longitude: coordinates[1],
        latitudeDelta: 0.01,
        longitudeDelta: 0.01,
      },
      1000
    );
  };

  if (route.params?.favID !== undefined) {
    const { favLocation, favID } = route.params;
    toggleFavPopup(favID, favLocation);
  }

  //Pin Clustering
  const [zoomLevel, setZoomLevel] = useState(0.015);

  const handleRegionChange = (region) => {
    setZoomLevel(region.longitudeDelta);
  };

  const zoomOutTier1 = [
    "64c092c47215cb11b43ff624",
    "64c092b47215cb11b43ff622",
    "64becdbb43626f3c61839fd3",
    "64c081d97215cb11b43ff5ea",
    "64c080457215cb11b43ff5cc",
    "64becf3e43626f3c61839fd9",
    "64bededb43626f3c61839fdf",
    "64c072db7215cb11b43ff5b2",
    "64c083b07215cb11b43ff604",
    "64c013e97215cb11b43ff590",
    "64c080f97215cb11b43ff5da",
    "64c072ba7215cb11b43ff5b0",
    "64c0121543626f3c61839fed",
    "64becf9743626f3c61839fdb",
    "64c0922c7215cb11b43ff618",
    "64c083fb7215cb11b43ff60a",
    "64c014b27215cb11b43ff59c",
    "64c082a37215cb11b43ff5fc",
    "64becc3743626f3c61839fcd",
    "64c082457215cb11b43ff5f0",
  ];

  const zoomOutTier2 = [
    ...zoomOutTier1,
    "64c0143e7215cb11b43ff594",
    "64c081c77215cb11b43ff5e8",
    "64c0811b7215cb11b43ff5de",
    "64bece8043626f3c61839fd7",
    "64c080ba7215cb11b43ff5d4",
    "64c013507215cb11b43ff582",
    "64c080137215cb11b43ff5ca",
    "64c0805f7215cb11b43ff5ce",
    "64c0737b7215cb11b43ff5be",
    "64c0733e7215cb11b43ff5ba",
    "64c072f17215cb11b43ff5b4",
    "64c072a37215cb11b43ff5ae",
    "64c0114543626f3c61839fe3",
    "64c0725e7215cb11b43ff5aa",
    "64c091e87215cb11b43ff610",
    "64becd5143626f3c61839fd1",
    "64c014937215cb11b43ff59a",
    "64c0826e7215cb11b43ff5f6",
    "64c0837f7215cb11b43ff600",
    "64becc9943626f3c61839fcf",
    "64c0840a7215cb11b43ff60c",
    "64c082517215cb11b43ff5f2",
    "64c0155a7215cb11b43ff5a0",
  ];

  const showLocations = () => {
    return filteredVerifiedLocations.map((item, id) => {
      if (zoomLevel > 0.025) {
        if (zoomOutTier1.includes(item._id)) {
          return (
            <Marker
              key={id}
              coordinate={{
                latitude: item.coordinates[0],
                longitude: item.coordinates[1],
              }}
              title={item.name}
              image={getMarker(item.tag)}
              onPress={() => togglePopup(item._id, item.coordinates)}
            >
              <Callout tooltip={true} />
            </Marker>
          );
        }
      } else if (zoomLevel >= 0.015) {
        if (zoomOutTier2.includes(item._id)) {
          return (
            <Marker
              key={id}
              coordinate={{
                latitude: item.coordinates[0],
                longitude: item.coordinates[1],
              }}
              title={item.name}
              image={getMarker(item.tag)}
              onPress={() => togglePopup(item._id, item.coordinates)}
            >
              <Callout tooltip={true} />
            </Marker>
          );
        }
      } else {
        return (
          <Marker
            key={id}
            coordinate={{
              latitude: item.coordinates[0],
              longitude: item.coordinates[1],
            }}
            title={item.name}
            image={getMarker(item.tag)}
            onPress={() => togglePopup(item._id, item.coordinates)}
          >
            <Callout tooltip={true}>
              <View
                style={{ width: 1, height: 1, backgroundColor: "transparent" }}
              />
            </Callout>
          </Marker>
        );
      }
    });
  };

  return (
    <View style={styles.container}>
      <View style={styles.searchBarContainer}>
        <View style={styles.searchBar}>
          <Icon
            name="magnifying-glass"
            size={21}
            color={Colors[theme]?.primaryText}
            type="entypo"
          />
          <TextInput
            value={searchQuery}
            onChangeText={(text) => {
              setSearchQuery(text);
              if (text.length === 0) setDropdownVisible(false); // Hide dropdown if search input is cleared
            }}
            onSubmitEditing={handleSearch}
            ref={searchRef}
            inputMode="search"
            autoComplete="off"
            autoCorrect={false}
            placeholderTextColor={Colors[theme]?.placeholderText}
            style={{
              color: Colors[theme]?.primaryText,
              fontFamily: "Poppins-Regular",
              width: "90%",
              paddingLeft: 10,
            }}
            contentStyle={{ fontFamily: "Poppins-Regular" }}
            selectionColor={Colors[theme]?.placeholderText}
          />
          <TouchableOpacity
            onPress={() => setIsFilterVisible(!isFilterVisible)}
          >
            <Icon
              name="bars"
              size={18}
              color={Colors[theme]?.primaryText}
              type="font-awesome"
            />
          </TouchableOpacity>
          <AINavigationButton />
        </View>
        {isDropdownVisible && (
          <SearchResultDropdown
            results={searchResults}
            onSelect={handleSelect}
          />
        )}
      </View>

      <MapView
        style={styles.map}
        customMapStyle={lightOrDark() ? LightMapStyle : DarkMapStyle}
        initialRegion={UCSDRegion}
        provider={PROVIDER_GOOGLE}
        showsUserLocation={false}
        onPress={() => {
          handleClosePress();
        }}
        ref={mapRef}
        onRegionChangeComplete={handleRegionChange}
      >
        {showLocations()}
      </MapView>

      {isFilterVisible && (
        <Filter
          checkboxes={checkboxes}
          initialCheckboxes={initialCheckboxes}
          handleSelectAll={handleSelectAll}
          handleCheckboxClick={handleCheckboxClick}
          handleResetAll={handleResetAll}
          onSubmit={filterLocations}
        />
      )}

      <ReviewsPopup
        locationId={locationId}
        sheetRef={sheetRef}
        handleSheetChange={handleSheetChange}
      />
    </View>
  );
}

// Get device window dimensions
const { height, width } = Dimensions.get("window");

// Calculate aspect ratio
const aspectRatio = height / width;

// Conditionally set paddingTop based on aspect ratio
const searchBarPadding = aspectRatio > 1.8 ? 50 : 15; // You can adjust these numbers based on testing on various devices

const styling = (theme: string) =>
  StyleSheet.create({
    container: {
      textAlign: "center",
      justifyContent: "center",
      // backgroundColor: "E74646",
      flex: 1,
      backgroundColor: "black",
      alignItems: "center",
    },

    title: {
      textAlign: "center",
      justifyContent: "center",
      top: 100,
    },
    map: {
      width: "100%",
      height: "100%",
    },
    filterButtonContainer: {
      position: "absolute",
      top: 40,
      right: 20,
    },
    searchBarContainer: {
      position: "absolute",
      top: 2,
      left: 0,
      right: 0,
      zIndex: 2,
      paddingHorizontal: 20,
      paddingTop: searchBarPadding,
    },
    searchBar: {
      flexDirection: "row",
      alignItems: "center",
      justifyContent: "space-between",
      height: 45,
      backgroundColor: theme === "light" ? "#FFF3E2" : "rgba(24, 25, 39, 0.80)",
      borderRadius: 10,
      paddingHorizontal: 16,
      // shadowColor: "#262322",
      // shadowOffset: { width: -5, height: 0 },
      // shadowOpacity: 0.2,
      // shadowRadius: 7,
      elevation: 5,
    },

    addLocation: {
      position: "absolute",
      bottom: 20,
      right: 30,
    },
    smallButton: {
      // flex: 1,
      zIndex: 1000,
      width: 82,
      height: 46,
      // flexShrink: 0,
      // flexDirection: 'row',
      padding: 10,
      borderRadius: 20,
      alignItems: "center",
      justifyContent: "center",
      position: "absolute",
      right: 22,
      top: 54,
      backgroundColor: "#ED7878",
      paddingHorizontal: 12, // Adjust for the size you want
      elevation: 1, // Add shadow on Android
      // shadowColor: "black",
      // shadowOffset: { width: 0, height: 2 }, // Shadow position
      // shadowOpacity: 0.25,
      // shadowRadius: 3.84, // Shadow blur
    },
    buttonText: {
      fontFamily: "Poppins-Regular",
      color: "white",
      fontSize: 17,
    },
    resultsContainer: {
      width: "100%",
      backgroundColor: theme === "light" ? "#FFF3E2" : "rgba(24, 25, 39, 0.85)",
      zIndex: 3,
      marginTop: 2, // Added this line to create space between search bar and dropdown
      borderRadius: 10,
    },
    resultItem: {
      padding: 10,
      borderBottomWidth: 1,
      borderBottomColor: "#D1D1D1",
      display: "flex",
      flexDirection: "row",
    },
    noResultsText: {
      padding: 10,
      textAlign: "center",
      fontFamily: "Poppins-Regular",
      color: Colors[theme]?.primaryText,
    },
    resultText: {
      color: Colors[theme]?.primaryText,
    },
    resultDropdownText: {
      color: Colors[theme]?.primaryText,
      fontFamily: "Poppins-Regular",
    },
  });
