// Valid Username Constraints:
// 1. Start with an alphabet
// 2. All other characters can be alphabets, numbers or an underscore
// 3. At least 3 characters long
export const validUsername = (username: string) => {
  const regexValidator = /^[A-Za-z][A-Za-z0-9_]{2,}$/;

  if (!regexValidator.test(username)) {
    return "Username must be at least 3 characters long and can only contain letters (a-z), numbers, and underscores (_).";
  }
  return "";
};

export const validEmailAddress = (email: string) => {
  const regexValidator = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

  if (!regexValidator.test(email)) {
    return "Please enter a valid email address.";
  }
  return "";
};

// Valid Password Constraints:
// 1. At least 6 characters long
// 2. Contains at least 1 uppercase letter (A-Z)
// 3. Contains at least 1 lowercase letter (a-z)
// 4. Contains at least 1 number (0-9)
// 5. Contains at least 1 special character (e.g., @, #, $, etc.)
export const validPassword = (password: string) => {
  const regexValidator =
    /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[-().&@?'#$!%*])[A-Za-z\d().&@?'#$!%*-]{6,}$/;

  if (!regexValidator.test(password)) {
    return "Password must be 6+ characters with 1 uppercase, 1 lowercase, 1 number, and 1 special character.";
  }
  return "";
};

export const confirmSamePassword = (
  password: string,
  confirmPassword: string
) => {
  if (password !== confirmPassword) {
    return "Passwords do not match. Please double-check and try again.";
  }
  return "";
};
