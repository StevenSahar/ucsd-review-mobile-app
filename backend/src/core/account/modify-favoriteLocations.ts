import express from "express";
import User from "../../models/user.model";
import jwt from "jsonwebtoken";

const router = express.Router();

router.post("/", async (req, res) => {
  try {
    const token = req.headers["x-access-token"];
    const decoded = jwt.verify(token, process.env.JWT_SECRET);

    const { locationId } = req.body;

    const user = await User.findOne({ email: decoded.email });

    //Unfavorites Location
    if (user.favoriteLocations.includes(locationId)) {
      const newArray = user.favoriteLocations.filter(function (value) {
        return value !== locationId;
      });
      user.favoriteLocations = newArray;
      await user.save();
      return res
        .status(200)
        .send({ message: "Location successfully unfavorited." });
    }

    //Favorites Location
    const newArray = [...user.favoriteLocations, locationId];
    user.favoriteLocations = newArray;
    await user.save();
    return res
      .status(200)
      .send({ message: "Location successfully favorited." });
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
});

export default router;
