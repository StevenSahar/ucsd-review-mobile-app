import React, { Fragment, useEffect, useState } from "react";
import {
  SafeAreaView,
  View,
  StyleSheet,
  TouchableOpacity,
  Image,
} from "react-native";
import { Text, TextInput } from "react-native-paper";
import { CheckBox, Button } from "@rneui/themed";
import { Formik } from "formik";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { Entypo } from "@expo/vector-icons";
import { toggleButton } from "../components/toggle-password";
import { rememberUser, forgetUser } from "../utils/remember-me";
import { orSeparator } from "../components/or-separator";
import { Colors } from "../utils/theme";
import { ModeLoader } from "../utils/theme/mode-loader";
import { blankPageHeader } from "../components/page-headers";
import GoogleAccount from "../components/google-account";

//useNavigation()
interface LoginProps {
  navigation: any;
}

function Login(props: LoginProps) {
  //Page navigation
  const ForgetPassword = () => props.navigation.navigate("ForgotPassword");
  const HomePage = () => props.navigation.navigate("HomePage");

  //Load theme
  const theme = ModeLoader();
  const styles = styling(theme);

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [showPassword, setShowPassword] = useState(true);
  const [rememberMe, setRememberMe] = useState(false);

  const [emailError, setEmailError] = useState(false);
  const [passwordError, setPasswordError] = useState(false);
  const [message, setMessage] = useState("");

  async function login() {
    const server = "132.249.242.205";
    const port = "5050";
    const route = `http://${server}:${port}/login`;
    const response = await fetch(route, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        email: email,
        password: password,
      }),
    });

    const data = await response.json();

    //Resets the error
    setEmailError(false);
    setPasswordError(false);
    setMessage("");

    if (data.token) {
      //Remember me button
      if (rememberMe) {
        rememberUser(email, password);
      } else {
        forgetUser();
      }

      //Sets token
      await AsyncStorage.setItem("token", data.token);

      //Navigate to homepage
      HomePage();
    } else {
      //Email Error
      if (data.message) {
        setEmailError(true);
        setMessage("Please verify account before logging in.");
      }
      //Password Error
      else {
        setPasswordError(true);
        setMessage("Invalid Email or Password.");
      }
    }
  }

  const togglePassword = () => {
    setShowPassword(!showPassword);
  };

  const toggleRememberMe = async () => {
    setRememberMe(!rememberMe);
  };

  useEffect(() => {
    //Fill the Textinput with email & password if remember me is toggled
    const checkRememberMe = async () => {
      try {
        const rememberMeStatus = await AsyncStorage.getItem("RememberMe");
        if (rememberMeStatus === "true") {
          setRememberMe(true);
          const savedEmail = await AsyncStorage.getItem("Email");
          const savedPassword = await AsyncStorage.getItem("Password");
          setEmail(savedEmail || "");
          setPassword(savedPassword || "");
        }
      } catch (error) {
        console.log(error);
      }
    };
    checkRememberMe();
  }, []);

  return (
    <Fragment>
      <SafeAreaView style={styles.topSafeAreaView} />
      <SafeAreaView style={styles.safeContainer}>
        {blankPageHeader("LandingPage")}
        <View style={styles.outerContainer}>
          <Formik
            initialValues={{}}
            onSubmit={() => {
              login();
            }}
          >
            {({ handleSubmit }) => (
              <View style={styles.container}>
                <Text style={styles.title}>Log In</Text>
                <View
                  style={[
                    styles.inputContainer,
                    emailError && styles.inputContainerError,
                  ]}
                >
                  <TextInput
                    style={styles.input}
                    value={email}
                    onChangeText={setEmail}
                    placeholder="email"
                    placeholderTextColor={Colors[theme]?.placeholderText}
                    textColor={Colors[theme]?.primaryText}
                    contentStyle={{ fontFamily: "Poppins-Regular" }}
                    selectionColor={Colors[theme]?.placeholderText}
                    underlineColor="transparent"
                    activeUnderlineColor="transparent"
                    textContentType="emailAddress"
                    keyboardType="email-address"
                    autoCapitalize="none"
                  />
                </View>
                <View
                  style={[
                    styles.inputContainer,
                    passwordError && styles.inputContainerError,
                  ]}
                >
                  <TextInput
                    style={styles.input}
                    value={password}
                    onChangeText={setPassword}
                    placeholder="password"
                    placeholderTextColor={Colors[theme]?.placeholderText}
                    textColor={Colors[theme]?.primaryText}
                    contentStyle={{ fontFamily: "Poppins-Regular" }}
                    selectionColor={Colors[theme]?.placeholderText}
                    underlineColor="transparent"
                    activeUnderlineColor="transparent"
                    textContentType="password"
                    keyboardType="default"
                    secureTextEntry={showPassword}
                  />
                  {toggleButton(showPassword, togglePassword)}
                </View>
                <View style={styles.forgotRememberContainer}>
                  <CheckBox
                    title="remember me"
                    checked={rememberMe}
                    onIconPress={() => toggleRememberMe()}
                    onPress={() => toggleRememberMe()}
                    size={20}
                    fontFamily="Poppins-Regular"
                    containerStyle={styles.checkbox}
                    textStyle={styles.rememberMeText}
                    uncheckedColor={Colors[theme]?.border}
                    checkedColor={Colors[theme]?.border}
                    iconType="material-community"
                    checkedIcon="checkbox-outline"
                    uncheckedIcon={"checkbox-blank-outline"}
                  />
                  <TouchableOpacity
                    activeOpacity={0.3}
                    onPress={ForgetPassword}
                    style={styles.forgotPasswordContainer}
                  >
                    <Text style={styles.forgotPassword}>forget password?</Text>
                  </TouchableOpacity>
                </View>
                <View style={styles.messageContainer}>
                  <Text
                    style={[
                      styles.message,
                      (emailError || passwordError) && styles.messageError,
                    ]}
                  >
                    {message}
                  </Text>
                </View>
                <Button
                  onPress={() => handleSubmit()}
                  title="Continue"
                  icon={
                    <Entypo
                      name="chevron-right"
                      size={25}
                      color={Colors[theme]?.buttonText}
                    />
                  }
                  iconRight
                  buttonStyle={styles.continueButton}
                  titleStyle={styles.continueButtonText}
                />
              </View>
            )}
          </Formik>
          {orSeparator()}
          {/* {GoogleAccount(true)} */}
        </View>
      </SafeAreaView>
    </Fragment>
  );
}

const styling = (theme) =>
  StyleSheet.create({
    topSafeAreaView: {
      flex: 0,
      backgroundColor: Colors[theme]?.primaryBackground,
    },
    safeContainer: {
      flex: 1,
      backgroundColor: Colors[theme]?.primaryBackground,
    },
    outerContainer: {
      flex: 1,
      alignItems: "center",
    },
    container: {
      alignItems: "center",
      justifyContent: "flex-start",
    },
    title: {
      color: Colors[theme]?.primaryText,
      fontFamily: "Poppins-Regular",
      fontSize: 20,
      fontStyle: "normal",
      fontWeight: "400",
      letterSpacing: 1,
      marginTop: 15,
      marginBottom: 47,
    },
    inputContainer: {
      width: 290,
      height: 50,
      flexDirection: "row",
      alignItems: "center",
      borderTopLeftRadius: 23,
      borderBottomLeftRadius: 23,
      borderBottomRightRadius: 23,
      borderTopRightRadius: 23,
      borderWidth: 0.5,
      borderColor: Colors[theme]?.border,
      backgroundColor: Colors[theme]?.inputContainer,
      opacity: 1,
      margin: 10,
      padding: 0,
    },
    input: {
      width: "87%",
      height: 50,
      borderTopLeftRadius: 23,
      borderBottomLeftRadius: 23,
      borderBottomRightRadius: 0,
      borderTopRightRadius: 0,
      backgroundColor: "transparent",
      color: Colors[theme]?.placeholderText,
      fontFamily: "Poppins-Regular",
      fontSize: 12,
      fontStyle: "normal",
      fontWeight: "400",
      letterSpacing: 0.6,
      paddingLeft: 5,
    },
    forgotRememberContainer: {
      width: 290,
      height: 30,
      flexDirection: "row",
      alignItems: "center",
    },
    checkbox: {
      flex: 1,
      alignItems: "flex-start",
      justifyContent: "center",
      backgroundColor: "transparent",
      borderWidth: 0,
      padding: 0,
      paddingLeft: 6,
    },
    rememberMeText: {
      color: Colors[theme]?.border,
      fontFamily: "Poppins-Regular",
      fontSize: 10,
      fontWeight: "400",
      letterSpacing: 0.5,
      marginLeft: 7,
    },
    forgotPasswordContainer: {
      flex: 1,
      alignItems: "center",
      justifyContent: "center",
    },
    forgotPassword: {
      color: Colors[theme]?.border,
      fontFamily: "Poppins-Regular",
      fontSize: 10,
      fontStyle: "normal",
      fontWeight: "400",
      letterSpacing: 0.5,
      textAlign: "center",
    },
    continueButton: {
      width: 290,
      height: 50,
      alignItems: "center",
      justifyContent: "flex-end",
      borderTopLeftRadius: 23,
      borderBottomLeftRadius: 23,
      borderBottomRightRadius: 23,
      borderTopRightRadius: 23,
      backgroundColor: Colors[theme]?.button,
      marginTop: 12,
    },
    continueButtonText: {
      color: Colors[theme]?.buttonText,
      fontFamily: "Poppins-Regular",
      fontSize: 12,
      fontStyle: "normal",
      fontWeight: "400",
      letterSpacing: 0.6,
      textAlign: "left",
      paddingLeft: 14,
      marginRight: 168,
    },
    inputContainerError: {
      backgroundColor: "rgba(231, 70, 70, 0.24)",
      borderWidth: 1,
      borderColor: "#E74646",
    },
    messageContainer: {
      width: 270,
      height: 20,
      justifyContent: "center",
    },
    message: {
      width: "100%",
      color: Colors[theme]?.primaryText,
      fontFamily: "Poppins-Regular",
      fontSize: 11,
      fontStyle: "normal",
      fontWeight: "400",
      letterSpacing: 0.6,
      textAlign: "left",
      padding: 5,
    },
    messageError: {
      color: "#E74646",
    },
  });

export default Login;
