import express from "express";
import User from "../../models/user.model";
import Location from "../../models/location.model";
import Review from "../../models/review.model";
import jwt from "jsonwebtoken";
import bcrypt from "bcrypt";

const router = express.Router();

router.get("/", async (req, res) => {
    try {
        const token = req.headers["x-access-token"];
        const decoded = jwt.verify(token, process.env.JWT_SECRET);
        const user = await User.findOne({ email: decoded.email });

        res.status(200).send({ 
            username: user.username,
            email: user.email,
        });
    } catch (error) {
        res.status(401).send({ message: "Invalid token" });
    }
});

router.put("/", async (req, res) => {
    let errMsg = "User not found";
    try {
        const token = req.headers["x-access-token"];
        const decoded = jwt.verify(token, process.env.JWT_SECRET);
        const user = await User.findOne({ email: decoded.email });

        if(req.body.name) {
            user.username = req.body.name;
        }
        if(req.body.username) {
            user.username = req.body.username;
        }
        if(req.body.profilePicture) {
            user.profilePicture = req.body.profilePicture;
        }
        if (req.body.password) {
            if(!(await bcrypt.compare(req.body.oldPassword ? req.body.oldPassword : "", user.password))){
                res.status(400).send({ message: "Incorrect password" });
                return;
            } 
            const salt = await bcrypt.genSalt(Number(process.env.SALT));
            const hashPassword = await bcrypt.hash(req.body.password, salt);
            user.password = hashPassword;
        }
        if (req.body.email) {
            errMsg = "Email already exists";
            user.email = req.body.email;
        }

        await user.save();
        res.status(200).send({ message: "User updated successfully" });
    } catch (error) {
        res.status(400).send({ message: errMsg });
    }
});

router.get("/favorites", async (req, res) => {
    try {
        const token = req.headers["x-access-token"];
        const decoded = jwt.verify(token, process.env.JWT_SECRET);
        const user = await User.findOne({ email: decoded.email });
        const favoriteLocations = user.favoriteLocations;
        
        let result = [];
        for (const element of favoriteLocations) {
            const location = await Location.findById(element);
            result.push(location);
        }
        res.status(200).send({ message: "Favorites retrieved successfully", favorites: result });
    } catch (error) {
        res.status(401).send({ message: "Invalid token" });
    }
});

router.get("/reviews", async (req, res) => {
    try {
        const token = req.headers["x-access-token"];
        const decoded = jwt.verify(token, process.env.JWT_SECRET);
        const user = await User.findOne({ email: decoded.email });
        const reviews = await Review.find({ userId: user._id });
        res.status(200).send({ message: "Reviews retrieved successfully", reviews: reviews });
    } catch (error) {
        res.status(401).send({ message: "Invalid token" });
    }
});

export default router;