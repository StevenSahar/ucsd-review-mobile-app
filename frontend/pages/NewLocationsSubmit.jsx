import {
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  ScrollView,
  SafeAreaView,
} from "react-native";
import { Fragment, useEffect, useState } from "react";
import React from "react";
import { TouchableOpacity } from "react-native-gesture-handler";
import { useNavigation } from "@react-navigation/core";
import CategorySelect from "../components/CategorySelect";
import Modal from "react-native-modal";
import { Feather, Entypo } from "@expo/vector-icons";
import { Colors } from "../utils/theme";
import * as ImagePicker from "expo-image-picker";
import { Button, AirbnbRating } from "@rneui/themed";
import { ModeLoader } from "../utils/theme/mode-loader";
import AsyncStorage from "@react-native-async-storage/async-storage";
import styling from "../styles";
import ImageModal from "react-native-image-modal";
import { Ionicons } from "@expo/vector-icons";
import { AccountPageHeader } from "../components/page-headers";
import axios from "axios";

export default function NewLocationsSubmit({ route }) {
  const theme = ModeLoader();
  const styles = styling(theme);
  const style = locations(theme);

  const lightorDark = () => {
    return theme == "light";
  };
  const navigation = useNavigation();
  const marker = route.params.markerLocation;
  const goBack = () => {
    navigation.goBack();
  };

  const initialCheckboxes = [
    { name: "Dining Hall", color: "#568259", checked: false },
    { name: "Dorms", color: "#B26E63", checked: false },
    { name: "Lecture Hall", color: "#2C4251", checked: false },
    { name: "Study Spot", color: "#E9B44C", checked: false },
  ];

  const initialDarkCheckboxes = [
    { name: "Dining Hall", color: "#4BFF00", checked: false },
    { name: "Dorms", color: "#00FFFF", checked: false },
    { name: "Lecture Hall", color: "#FF006E", checked: false },
    { name: "Study Spot", color: "#F0FF1C", checked: false },
    { name: "Favorites", color: "#FB2B2B", checked: false },
  ];

  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");
  const [checkboxes, setCheckboxes] = useState(initialCheckboxes);
  const [tag, setTag] = useState();
  const [selectedImages, setSelectedImages] = useState([]);
  const [visible, setVisible] = useState(false);
  const [message, setMessage] = useState("");
  const [uploaded, setUploaded] = useState(false);

  let tempImages = [];

  const handleTitleText = (inputText) => {
    setTitle(inputText);
  };
  const handleDescriptionText = (inputText) => {
    setDescription(inputText);
  };

  async function submitLocation() {
    const formData = new FormData();

    //Insert the image information into form data
    selectedImages.forEach((image) => {
      formData.append("images", {
        uri: image.uri,
        type: "image/jpeg", // You can customize the type as needed
        name: "image.jpg", // You can customize the name as needed
      });
    });

    try {
      //Post image to database
      let imgResponse = { data: [] };
      if (selectedImages.length > 0) {
        imgResponse = await axios.post(
          "http://132.249.242.205:5050/upload",
          formData
        );
      }

      const response = await fetch("http://132.249.242.205:5050/add-location", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          "x-access-token": await AsyncStorage.getItem("token"),
        },
        body: JSON.stringify({
          name: title,
          description: description,
          tag: tag,
          coordinates: [marker.latitude, marker.longitude],
          isVerified: false,
          images: imgResponse.data,
        }),
      });
      console.log(imgResponse.data)
    } catch (e) {
      console.error("Error " + e);
    }
  }

  //Open user's image library
  const pickImage = async () => {
    //Request user permission
    const { status } = await ImagePicker.requestMediaLibraryPermissionsAsync();

    if (status !== "granted") {
      alert("Permission to access media library was denied");
      return;
    }

    //Launch image library
    const result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Images,
      allowsMultipleSelection: true,
      orderedSelection: true,
      quality: 1,
      selectionLimit: 5 - selectedImages.length,
    });

    if (!result.canceled) {
      toggleOverlay();
      tempImages = [...selectedImages, ...result.assets];
      setSelectedImages(tempImages);
      setUploaded(true);
    }
  };

  //Open camera
  const openCamera = async () => {
    try {
      const result = await ImagePicker.launchCameraAsync({
        mediaTypes: ImagePicker.MediaTypeOptions.Images,
        quality: 1,
        allowsEditing: true,
        aspect: [4, 3],
      });

      if (!result.canceled) {
        toggleOverlay();
        tempImages = [...selectedImages, ...result.assets];
        setSelectedImages(tempImages);
      }
    } catch (error) {
      console.log("Error accessing the camera:", error);
    }
  };

  //Open camera permission
  const takePicture = async () => {
    const { status } = await ImagePicker.requestCameraPermissionsAsync();
    if (status === "granted") {
      openCamera();
    }
  };

  //Toggle Camera/Photos option
  const toggleOverlay = () => {
    setVisible(!visible);
  };

  const handleSubmit = () => {
    handleDescriptionText(description);
    handleTitleText(title);
    submitLocation();
    navigation.navigate("NewLocationsHome");
  };

  // const renderUploadedImages = () => {
  //   return (
  //     <ScrollView horizontal style={style.imageDisplay}>
  //       {imageUpload.map((imageBase64Array, index) => (
  //         <View style={style.imageContainer} key={index}>
  //           <ImageModal
  //             resizeMode="center"
  //             source={{ uri: `data:image/png;base64,${imageBase64Array}` }}
  //             style={style.uploadedImage}
  //           />
  //         </View>
  //       ))}

  //       <TouchableOpacity
  //         onPress={toggleOverlay}
  //         style={style.smallimageUploadButton}
  //         activeOpacity={0.2}
  //       >
  //         <Icon
  //           name="camera"
  //           type="feather"
  //           size={45}
  //           color={Colors[theme]?.button}
  //         />
  //       </TouchableOpacity>
  //     </ScrollView>
  //   );
  // };

  useEffect(() => {
    checkboxes.map((item) => {
      if (item.checked === true) {
        setTag(item.name);
      }
    });
  }, [checkboxes]);

  const handleCheckboxClick = (index) => {
    setCheckboxes((prevCheckboxes) => {
      const newCheckboxes = [...prevCheckboxes];
      // Uncheck all checkboxes in the same radio group
      newCheckboxes.forEach((checkbox) => (checkbox.checked = false));

      // Toggle the clicked checkbox
      newCheckboxes[index].checked = !newCheckboxes[index].checked;

      return newCheckboxes;
    });
  };

  //Remove image from upload
  const removeImage = (index) => {
    let tempArr = [...selectedImages];
    tempArr.splice(index, 1);
    setSelectedImages(tempArr);
  };

  // need to make post request once filter system is done
  return (
    <Fragment>
      <SafeAreaView
        style={{
          flex: 0,
          backgroundColor: Colors[theme]?.secondaryBackground,
          height: "3%",
        }}
      />
      <SafeAreaView style={styles.container}>
        <AccountPageHeader
          title="New Location"
          page="NewLocationsMap"
          theme={theme}
        />
        <ScrollView
          style={{ backgroundColor: "transparent" }}
          scrollEnabled={true}
          showsVerticalScrollIndicator={false}
        >
          <View style={{ alignItems: "center" }}>
            <ScrollView style={style.name} keyboardShouldPersistTaps="handled">
              <TextInput
                style={style.textInput}
                onChangeText={handleTitleText}
                value={title}
                placeholder="location name"
                placeholderTextColor={Colors[theme]?.placeholderText}
                maxLength={200}
                textAlign="left" // Align the text to the left
                textAlignVertical="center" // Center the text vertically
              />
            </ScrollView>

            <View>
              {checkboxes.map((checkbox, index) => (
                <CategorySelect
                  key={index}
                  color={
                    lightorDark()
                      ? checkbox.color
                      : initialDarkCheckboxes[index].color
                  }
                  name={checkbox.name}
                  checked={checkbox.checked}
                  onCheckboxClick={() => handleCheckboxClick(index)}
                />
              ))}
            </View>

            <ScrollView
              style={style.description}
              keyboardShouldPersistTaps="handled"
            >
              <TextInput
                style={[style.textInput, { height: 120, borderRadius: 40 }]}
                onChangeText={handleDescriptionText}
                value={description}
                placeholder="description"
                multiline={true}
                placeholderTextColor={Colors[theme]?.placeholderText}
                maxLength={200}
              />
            </ScrollView>

            <View style={style.buttonContainer}>
              {/* {imageUpload.length > 0 ? (
                renderUploadedImages()
              ) : (
                <TouchableOpacity
                  onPress={toggleOverlay}
                  style={style.imageUploadButton}
                  activeOpacity={0.2}
                >
                  <Icon
                    name="camera"
                    type="feather"
                    size={45}
                    color={Colors[theme]?.button}
                  />
                </TouchableOpacity>
              )} */}
              <ScrollView
                horizontal
                style={{ height: 142, minWidth: "100%" }}
                contentContainerStyle={{ alignItems: "center" }}
                scrollEnabled={selectedImages.length !== 0}
                showsHorizontalScrollIndicator={false}
              >
                {selectedImages.map((img, index) => {
                  return (
                    <View key={img.uri}>
                      <ImageModal
                        source={{ uri: img.uri }}
                        style={{
                          width: (img.width / img.height) * 138,
                          height: 138,
                          backgroundColor: "transparent",
                          marginRight: 10,
                          borderRadius: 5,
                          marginTop: 1,
                          flex: 1,
                        }}
                        resizeMode="contain"
                      />
                      <Ionicons
                        name="ios-close-circle-sharp"
                        size={24}
                        color="rgba(255,255,255, 0.7)"
                        style={{
                          position: "absolute",
                          top: 2,
                          left: 2,
                        }}
                        onPress={() => {
                          removeImage(index);
                        }}
                      />
                    </View>
                  );
                })}
                {selectedImages.length < 5 && (
                  <Button
                    onPress={toggleOverlay}
                    icon={
                      <Feather
                        name="camera"
                        size={45}
                        color={Colors[theme]?.button}
                      />
                    }
                    containerStyle={style.imageUploadContainer}
                    buttonStyle={style.imageUploadButton}
                    activeOpacity={0.2}
                  />
                )}
              </ScrollView>
              <Text style={style.imageCounter}>
                Photos: {selectedImages.length}/5
              </Text>
              <Modal
                isVisible={visible}
                onBackdropPress={toggleOverlay}
                onBackButtonPress={toggleOverlay}
                style={style.modal}
                animationIn="fadeIn"
                animationOut="fadeOut"
                animationInTiming={300}
                animationOutTiming={100}
                avoidKeyboard={false}
                backdropTransitionInTiming={0}
                backdropTransitionOutTiming={0}
                hideModalContentWhileAnimating={true}
              >
                <View style={style.modalContent}>
                  <Button
                    onPress={takePicture}
                    title="Camera"
                    buttonStyle={[
                      style.modalButton,
                      {
                        borderTopLeftRadius: 20,
                        borderTopRightRadius: 20,
                        borderBottomLeftRadius: 0,
                        borderBottomRightRadius: 0,
                        borderBottomWidth: 0.3,
                        borderColor: Colors[theme]?.border,
                      },
                    ]}
                    titleStyle={style.modalButtonText}
                    containerStyle={style.modalButtonContainer}
                    activeOpacity={0.8}
                  />
                  <Button
                    onPress={pickImage}
                    title="Photo Library"
                    buttonStyle={[
                      style.modalButton,
                      {
                        borderBottomLeftRadius: 20,
                        borderBottomRightRadius: 20,
                        borderTopLeftRadius: 0,
                        borderTopRightRadius: 0,
                      },
                    ]}
                    titleStyle={style.modalButtonText}
                    containerStyle={[
                      style.modalButtonContainer,
                      { marginBottom: 10 },
                    ]}
                    activeOpacity={0.8}
                  />
                  <Button
                    onPress={toggleOverlay}
                    title="Cancel"
                    buttonStyle={[style.modalButton, { borderRadius: 20 }]}
                    titleStyle={[
                      style.modalButtonText,
                      { fontFamily: "Poppins-SemiBold" },
                    ]}
                    containerStyle={style.modalButtonContainer}
                    activeOpacity={0.8}
                  />
                </View>
              </Modal>
            </View>

            <View style={style.submit}>
              <TouchableOpacity style={style.buttons} onPress={handleSubmit}>
                <Text
                  style={{
                    color: Colors[theme]?.mainButtonText,
                    marginLeft: 30,
                    marginRight: 225,
                    fontFamily: "Poppins-Regular",
                  }}
                >
                  Submit
                </Text>
                <Entypo
                  name="chevron-right"
                  size={24}
                  color={Colors[theme]?.mainButtonText}
                />
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>
      </SafeAreaView>
    </Fragment>
  );
}

const locations = (theme) =>
  StyleSheet.create({
    name: {
      paddingTop: 20,
      paddingBottom: 25,
      backgroundColor: "transparent",
    },
    textInput: {
      backgroundColor: Colors[theme]?.inputContainer,
      width: 340,
      borderRadius: 80,
      height: 50,
      borderWidth: 0.5,
      borderColor: Colors[theme]?.border,
      padding: 10,
      paddingTop: 13,
      paddingLeft: 35,
      fontSize: 15,
      textAlignVertical: "center",
      fontFamily: "Poppins-Regular",
      color: Colors[theme]?.text,
    },

    description: {
      backgroundColor: "transparent",
      paddingTop: 30,
      paddingBottom: 10,
    },

    submit: {
      marginTop: 10,
      alignItems: "center",
      alignContent: "center",
      backgroundColor: "transparent",
      marginBottom: 150,
    },

    buttonContainer: {
      width: 340,
      alignItems: "flex-start",
      paddingTop: 10,
      marginBottom: 10,
    },

    imageUploadContainer: {
      alignItems: "center",
      justifyContent: "center",
      borderTopLeftRadius: 23,
      borderBottomLeftRadius: 23,
      borderBottomRightRadius: 23,
      borderTopRightRadius: 23,
      borderWidth: 0.5,
      borderColor: Colors[theme]?.border,
      backgroundColor: Colors[theme]?.inputContainer,
    },
    imageUploadButton: {
      width: 150,
      height: 140,
      backgroundColor: Colors[theme]?.inputContainer,
    },
    imageCounter: {
      marginTop: 10,
      fontFamily: "Poppins-Regular",
      fontStyle: "normal",
      fontWeight: "400",
      letterSpacing: 0.2,
      fontSize: 12,
      color: Colors[theme]?.primaryText,
    },

    buttons: {
      backgroundColor: Colors[theme]?.mainButton,
      flexDirection: "row",
      width: 350,
      borderRadius: 80,
      height: 50,
      fontSize: 15,
      alignItems: "center",
    },

    modal: {
      justifyContent: "flex-end",
      alignItems: "center",
      marginBottom: 20,
    },
    modalContent: {
      width: "100%",
      backgroundColor: "transparent",
    },
    modalButtonContainer: {
      width: "100%",
    },
    modalButton: {
      height: 65,
      alignItems: "center",
      justifyContent: "center",
      backgroundColor: Colors[theme]?.inputContainer,
    },

    modalButtonText: {
      color: "rgb(10, 132, 255)",
      fontFamily: "Poppins-Medium",
      fontSize: 18,
      fontStyle: "normal",
      fontWeight: "400",
      letterSpacing: 0.6,
    },

    imageDisplay: {
      height: 150,
    },

    imageContainer: {
      marginRight: 15,
    },

    uploadedImage: {
      width: 150,
      height: 150,
      borderTopLeftRadius: 23,
      borderBottomLeftRadius: 23,
      borderBottomRightRadius: 23,
      borderTopRightRadius: 23,
      borderWidth: 0.5,
    },

    smallimageUploadButton: {
      alignItems: "center",
      justifyContent: "center",
      borderTopLeftRadius: 23,
      borderBottomLeftRadius: 23,
      borderBottomRightRadius: 23,
      borderTopRightRadius: 23,
      borderWidth: 0.5,
      width: 150,
      height: "100%",
      backgroundColor: Colors[theme]?.inputContainer,
    },
  });
